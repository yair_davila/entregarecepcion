<?php 
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  include 'conexion.php';
?>
<div class="container-fluid">
  <h4>Registros del anexo 1</h4>
	<table class="table table-hover">
		<thead>
			<tr>
        <th>Objetivo</th>
        <th>Creación</th>
        <th>Legal</th>
        <th>Fecha captura</th>
        <th>Acción</th>
      </tr>
		</thead>
		<tbody>
			<?php 
			$sql_a1 = " SELECT FOLIOA1, OBJETIVO,CREACION,LEGAL,F_CAPTURA FROM ANEXO1  WHERE ID_ENTREGA = :id";

      		$res=$conexion->prepare($sql_a1);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA1'].",".$fila['OBJETIVO'].",".$fila['CREACION'].",".$fila['LEGAL'];//datos a pasar al servidor
				
				$borrar = $fila['FOLIOA1'].",". 1;
        

         
				?>

			<tr>
				<td><?php echo $fila['OBJETIVO']; ?></td>
				<td><?php echo $fila['CREACION']; ?></td>
				<td><?php echo $fila['LEGAL']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A1" id="actualizar" onclick="datos_a1('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				</td>
			</tr>
			<?php endwhile;
       $res->closeCursor();  $conexion=null; ?>

		</tbody>
	</table>
</div>

<form id="a1">
<div class="modal fade" id="Edicion_A1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label >Obljetivo:</label>
        <textarea class="form-control " name="objA1" id="objA1"  placeholder ="Describa el objetivo"></textarea>
        <br>
        <label>Fecha de creación</label>
        <input type="date" class="form-control" name="feA1" id="feA1">
        <br>
        <label for="objetivo">Ley, Decreto, Acuerdo u Otro:</label>
        <textarea class="form-control" name="decA1" id="decA1" placeholder ="Describa el objetivo"></textarea> 
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>
<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a1').validate({
        rules: {
            objA1: { required: true, texto:true},
            feA1:{required: true},
            decA1:{required: true, texto:true}   
        },
        messages: {
            objA1: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            feA1:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
            decA1:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a1();
        }
    });
  });

  $('#Edicion_A1').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>







