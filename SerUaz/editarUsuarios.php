<?php

include 'conexion.php';

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }else if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

?>
<div class="container-fluid">
	<h5>Editar usuario</h5>
			<input type="hidden" id="accion" value="usuario_id" />
			<div class="from-group" id="ver">
				<label>Selecciona al usuario para editar</label>
				<?php 
				$sql_u="SELECT ID_USUARIO,NOMBRE FROM USUARIOS";
				$res_u= $conexion -> prepare($sql_u);
				$res_u->execute();
				?>
				<select class="form-control"  id="id_usr">
					<?php while($fila = $res_u -> fetch(PDO::FETCH_ASSOC)):?>
					<option value="<?php echo $fila['ID_USUARIO']; ?>"><?php echo $fila['NOMBRE'];?></option>
					<?php endwhile; $res_u -> closeCursor(); $conexion=null; ?> 
				</select>
				<br>
              	<button type="button" class="btn btn-primary" onclick="set_id()"><span class="glyphicon glyphicon-ok"></span> Seleccionar</button>
	
			</div>

			<div class="form-group" id="oculto" style="display:none;">
				<label>Nombre:</label>
				<input type="text"  name="0" id="nombreU" class="form-control" required placeholder="Nombre completo"  onChange="getValidaTu(this.value,name,id)">
				<p id="aviso0" style="display:none; color: red;">Rellena el campo por favor<br></p>
          		<p id="aviso_0" style="display:none; color: red;"><br></p>
				<label>Usario:</label>
				<input type="text"  name="1" id="usrU" class="form-control" placeholder="Usuario asignado" required  onChange="getValidaTu(this.value,name,id)"  >
				<p id="aviso1" style="display:none; color: red;">Rellena el campo por favor<br></p>
          		<p id="aviso_1" style="display:none; color: red;"><br></p>
				<label>Password:</label>
				<input type="password"  name="2"  id="passU" class="form-control" placeholder="Password" onChange="getValidaP(this.value,name,id)">
				<p id="aviso2" style="display:none; color: red;">Rellena el campo por favor<br></p>
          		<p id="aviso_2" style="display:none; color: red;"><br></p>
				<label>Tipo de usuario</label>
				<select class="form-control" id="tipousrU">
					<option value="">Selecciona una opcion...</option>
					<option value="1">Capturista</option>
					<option value="2">Responsable de unidad</option>
				</select>
				<p id="aviso3" style="display:none; color: red;">Debes seleccionar una opción<br></p>
				<br>
				<button type="button" class="btn btn-primary" onclick="upd_usr();"><span class="glyphicon glyphicon-send"></span> Actualizar</button>				
			</div>
			</form>
	</div>