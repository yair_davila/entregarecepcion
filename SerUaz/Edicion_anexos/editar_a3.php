<?php 
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require '../conexion.php';
?>


<div class="table-responsive">
	<h4>Registros del anexo 3.- Recursos Humanos<span class="glyphicon glyphicon-list-alt"></h4>
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="100px">Nomina</th>
        		<th width="125px">Fecha de corte</th>
        		<th width="800px">Observaciones</th>
        		<th width="150px">Fecha de captura</th>
        		<th width="100px">Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a1 = " SELECT FOLIOA3,NOMINA,CORTE,OBSERVACIONES,F_CAPTURA FROM ANEXO3 WHERE ID_ENTREGA = :id";

      		$res=$conexion->prepare($sql_a1);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA3']. "|" .$fila['NOMINA']. "|" .$fila['CORTE']."|".$fila['OBSERVACIONES'] ."|". $fila['F_CAPTURA'];
				$borrar = $fila['FOLIOA3']."|" . 3;
				?>
			<tr>
				<td><?php echo $fila['NOMINA']; ?></td>
				<td><?php echo $fila['CORTE']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
				<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A3" id="actualizar" onclick="datos_a3('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
				<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor();  $conexion= null; ?> 	
		</tbody>
	</table>
</div>

<form id="a3">
<div class="modal fade" id="Edicion_A3"role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      <label> Tipo de Nominaaa:</label>
      <select class="form-control" name="t_nominau" id="t_nominau">
        <option value="Ordinaria">Ordinaria</option>   
      </select>
      <br>
      <label>Fecha de Corte:</label>
      <input type="date" class="form-control" name="f_corteu" id="f_corteu" >
      <br>
      <label>Observaciones:</label>
      <textarea class="form-control" name="obs_a3u" id="obs_a3u" placeholder="Describa las observaciones"></textarea>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>




<script>
      $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a3').validate({
        rules: {
            t_nominau: { required: true, texto:true},
            f_corteu:{required: true},
            obs_a3u:{required: true, texto:true}   
        },
        messages: {
            t_nominau: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            f_corteu:{
              required: "Completa el campo por favor",
            },
            obs_a3u:{
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a3();
        }
    });
  });

  $('#Edicion_A3').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>




