<?php 
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require '../conexion.php';
?>
<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="100px">Tipo de cuenta</th>
        		<th width="100px">Número de cuenta</th>
        		<th width="100px">Institución</th>
        		<th width="50px">Saldo</th>
        		<th width="100px">Conciliación</th>
        		<th width="600px">Observaciones</th>
        		<th>Fecha captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a1 = " SELECT FOLIOA8,T_CUENTA,NO_CUENTA,INSTITUCION,SALDO,CONCILIACION,OBSERVACIONES,F_CAPTURA FROM ANEXO8  WHERE ID_ENTREGA = :id";


      		$res=$conexion->prepare($sql_a1);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA8']."|".$fila['T_CUENTA']."|".$fila['NO_CUENTA']."|".$fila['INSTITUCION']."|".$fila['SALDO']."|".$fila['CONCILIACION']."|".$fila['OBSERVACIONES'];
				
				$borrar = $fila['FOLIOA8']."|". 8;
        

				?>
			<tr>
				<td><?php echo $fila['T_CUENTA']; ?></td>
				<td><?php echo $fila['NO_CUENTA']; ?></td>
				<td><?php echo $fila['INSTITUCION']; ?></td>
				<td><?php echo $fila['SALDO']; ?></td>
				<td><?php echo $fila['CONCILIACION']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A8" id="actualizar" onclick="datos_a8('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); ?> 	
		</tbody>
	</table>
</div>


<form id="a8">
<div class="modal fade" id="Edicion_A8" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Tipo de Cuenta:</label>
		<select class="form-control" name="tcu_a8u" id="tcu_a8u">
         	<option value="">Selecciona una opción...</option>
			<option value="Cheques">Cheques</option>
			<option value="Inversiones">Inversiones</option>
		</select>
		<br>
		<label>Número de Cuenta:</label>
		<input type="text"  name="ncu_a8u" id="ncu_a8u" class="form-control" placeholder="Número de cuenta bancaria">
		<br>	
		<label for="">Institución Bancaria:</label>
		<input type="text"  name="iba_a8u" id="iba_a8u" class="form-control" placeholder="Institución Bancaria con la cual se celebra la cuenta">
		<br>
		<label>Saldo</label>
		<input type="text" name="sal_a8u" id="sal_a8u" class="form-control" placeholder="Saldo actual">
		<br>	
		<label for="p_inf">Conciliación Bancaria</label>
		<label class="radio-inline"><input type="radio" name="cban_a8u" id="si_a8u" value="Si"> Si</label>
		<label class="radio-inline"><input type="radio" name="cban_a8u" id="no_a8u" value="No">   No</label>
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a8u" id="obs_a8u" placeholder="Observaciones respecto a lo concerniente al informe de recursos financieros"></textarea>		
      	</div>
      	<div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      	</div>
    </div>
  </div>
</div>
</form>


<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a8').validate({
        rules: {
            tcu_a8u: {required: true, texto:true},
            ncu_a8u:{required: true, number:true},
            iba_a8u:{required: true, texto:true},
            sal_a8u:{required: true, number:true},
            obs_a8u:{required: true, texto:true}
        },
        messages: {
            tcu_a8u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            ncu_a8u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",

            },
            iba_a8u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            sal_a8u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            obs_a8u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a8();
        }
    });
  });

  $('#Edicion_A8').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>


