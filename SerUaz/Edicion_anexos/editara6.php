<?php 
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
      header("Location: https://localhost/SerUaz/");
  
  }
  require '../conexion.php';
?>
<div class="table-responsive">
  <h4>Registros del anexo 6.- Principales Archivos de Documentos<span class="glyphicon glyphicon-list-alt"></h4>
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="160px">Área que resguarda </th>
        		<th width="160px">Tipo de documento</th>
        		<th width="115px">Tipo archivo</th>
        		<th width="600px">Contenido</th>
        		<th width="150px">Fecha de captura</th>
        		<th width="100px">Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a6 = "SELECT FOLIOA6,AREA_RESGUARDA,TIPO_DOC,TIPO_ARCHIVO,CONTENIDO,F_CAPTURA FROM ANEXO6 WHERE ID_ENTREGA = :id";

      		$res=$conexion->prepare($sql_a6);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
			$datos = $fila['FOLIOA6']."|".$fila['AREA_RESGUARDA']."|".$fila['TIPO_DOC']."|".$fila['TIPO_ARCHIVO']."|".$fila['CONTENIDO'];

				$borrar = $fila['FOLIOA6']."|". 6;
				

				?>
			<tr>
				<td><?php echo $fila['AREA_RESGUARDA']; ?></td>
				<td><?php echo $fila['TIPO_DOC']; ?></td>
				<td><?php echo $fila['TIPO_ARCHIVO']; ?></td>
				<td><?php echo $fila['CONTENIDO']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A6" id="actualizar" onclick="datos_a6('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
					
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion = null; ?> 	
		</tbody>
	</table>
</div>

<form id="a6">
<div class="modal fade" id="Edicion_A6" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Área que resguarda</label>
		<input type="text" class="form-control" name="area_a6u" id="area_a6u" placeholder="Área de unidad académica o administrativa">
		<br>
		<label>Tipo de Documento</label>
		<select class="form-control" name="tdoc_a6u" id="tdoc_a6u">
			<option value="">Selecciona una opción...</option>
			<option value="1">Archivos de documentos</option>
			<option value="2">Archivos electrónicos</option>
		</select>
		<br>
		<label>Tipo de Archivo</label>
		<select class="form-control" id="tarc_a6u" name="tarc_a6u">
			<option value="">Selecciona una opción...</option>
			<option value="Vigente">Vigente</option>
			<option value="Muerto">Muerto</option>
		</select>
		<br>
		<label>Documentos y/o Archivos que Contiene</label>
		<textarea class="form-control" name="doc_a6u" id="doc_a6u" placeholder="Contenido de documento y/o archivo electónico"></textarea>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>


<script>
      $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a6').validate({
        rules: {
            area_a6u: { required: true, texto:true},
            tdoc_a6u:{required: true, texto:true},
            tarc_a6u:{required: true, texto:true},
            doc_a6u:{required: true, texto:true}   
        },
        messages: {
            area_a6u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            tdoc_a6u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
            tarc_a6u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            doc_a6u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a6();
        }
    });
  });

  $('#Edicion_A6').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>
