<?php 
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  include '../conexion.php';
?>
<div class="table-responsive">
	<h4>Registros del anexo 4.- Recursos Materiales<span class="glyphicon glyphicon-list-alt"></h4>
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="200px">Inventario</th>
        		<th width="125px" >Fecha de corte</th>
        		<th width="720px" >Observaciones</th>
        		<th width="150px">Fecha de captura</th>
        		<th >Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a4 = " SELECT FOLIOA4,INVENTARIO,FECHA,OBSERVACIONES,F_CAPTURA FROM ANEXO4 WHERE ID_ENTREGA = :id";

      		$res=$conexion->prepare($sql_a4);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA4']."|".$fila['INVENTARIO']."|".$fila['FECHA']."|".$fila['OBSERVACIONES'];
				$borrar = $fila['FOLIOA4']."|" . 4;
				?>
			<tr>
				<td><?php echo $fila['INVENTARIO']; ?></td>
				<td><?php echo $fila['FECHA']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A4" id="actualizar" onclick="datos_a4('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion = null; ?> 	
		</tbody>
	</table>
</div>

<form id="a4">
<div class="modal fade" id="Edicion_A4" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
	  	<label>Tipo de Inventario</label>
	  	<select class="form-control" name="inv_a4u" id="inv_a4u">
			<option value="">Selecciona una opción...</option>
			<option value="1">Bienes, muebles, informáticos</option>
			<option value="2">Vehiculos y transporte</option>
			<option value="3">Bienes e inmuebles</option>
			<option value="4">Existencia en almacen</option>
			<option value="5">Relación de bajas</option>
		</select>
		<br>
		<label>Fecha de Corte</label>
		<input type="date" class="form-control" name="f_cortea4u" id="f_cortea4u">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a4u" id="obs_a4u" placeholder="Describa las observaciones"></textarea>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>


<script>
      $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a4').validate({
        rules: {
            inv_a4u: {required: true, texto:true},
            f_cortea4u:{required: true},
            obs_a4u:{required: true, texto:true}   
        },
        messages: {
            inv_a4u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            f_cortea4u:{
              required: "Completa el campo por favor",
              
            },
            obs_a4u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a4();
        }
    });
  });

  $('#Edicion_A4').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>




