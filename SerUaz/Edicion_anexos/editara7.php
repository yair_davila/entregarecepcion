<?php 
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("Location: https://localhost/SerUaz/");
  
  }
  require '../conexion.php';
?>
<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="180px">Tipo de informe</th>
        		<th width="100px">Reporte financiero</th>
        		<th width="150px">Fecha de informe</th>
        		<th width="650px">Observaciones</th>
        		<th>Fecha captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a1 = " SELECT FOLIOA7,TIPO_INFORME,REPORTE_FINANCIERO,FECHA_INFORME,OBSERVACIONES,F_CAPTURA FROM ANEXO7  WHERE ID_ENTREGA = :id";

      		$res=$conexion->prepare($sql_a1);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA7'] ."|". $fila['TIPO_INFORME'] ."|". $fila['REPORTE_FINANCIERO'] ."|". $fila['FECHA_INFORME'] ."|". $fila['OBSERVACIONES'];

				$borrar = $fila['FOLIOA7']."|". 7;

				?>
			<tr>
				<td><?php echo $fila['TIPO_INFORME']; ?></td>
				<td><?php echo $fila['REPORTE_FINANCIERO']; ?></td>
				<td><?php echo $fila['FECHA_INFORME']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A7" id="actualizar" onclick="datos_a7('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); ?> 	
		</tbody>
	</table>
</div>


<form id="a7">
<div class="modal fade" id="Edicion_A7" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      <label>Tipo de Informe</label>
		<select class="form-control" id="tipi_a7u" name="tipi_a7u">
			<option value="" >Slecciona un aopción</option>
			<option value="1">Presupuesto</option>
			<option value="2">Estado de Situación Financiera</option>
			<option value="3">Estado de actividades</option>
			<option value="4">Balanza de comprobación</option>
			<option value="5">Deudores diversos</option>
			<option value="6">Acredores diversos</option>
			<option value="7">Proveedores</option>
			<option value="8">Otros</option>
		</select>
		<br>
		<label>¿Presenta Informe?</label>
		<label class="radio-inline"><input type="radio" name="repi_a7u" id="si_a7u" value="Si"> Si</label>
		<label class="radio-inline"><input type="radio" name="repi_a7u" id="no_a7u" value="No">   No</label>
		<br>
		<label>Fecha del Documento</label>
		<input type="date" name="fdoc_a7u" id="fdoc_a7u" class="form-control">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a7u" id="obs_a7u" placeholder="Observaciones referentes al documento de informe financiero"></textarea>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>

<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a7').validate({
        rules: {
            tipi_a7u: { required: true, texto:true},
            repi_a7u:{required: true},
            fdoc_a7u:{required: true},
            obs_a7u:{required: true, texto:true},   
        },
        messages: {
            tipi_a7u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            repi_a7u:{
              required: "Completa el campo por favor",
            },
            fdoc_a7u:{
              required: "Completa el campo por favor",
                
            },
            obs_a7u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a7();
        }
    });
  });

  $('#Edicion_A7').on('hidden.bs.modal', function (e) {
      $("label.error").remove();
  });
</script>
