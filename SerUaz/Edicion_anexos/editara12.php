<?php 
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("Location: https://localhost/SerUaz/");
  
  }
  require '../conexion.php';
?>
<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="90px">Fecha de apertura </th>
        		<th width="90px">Fecha de fallo</th>
        		<th width="150px">Nombre del proyecto</th>
        		<th width="150px">Unidad y/o programa</th>
        		<th>Tipo de concurso</th>
        		<th>Origen del recurso</th>
        		<th>Empresa ganadora</th>
        		<th>Observaciones</th>
        		<th>Fecha de captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a12 = "SELECT FOLIOA12,F_APERTURA,F_FALLO,PROYECTO,UNIDAD,TIPO_CONCURSO,ORIGEN_RECURSO,EMPRESA_GANADORA,OBSERVACIONES,F_CAPTURA FROM ANEXO12  WHERE ID_ENTREGA = :id";
      		$res=$conexion->prepare($sql_a12);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA12']."|".$fila['F_APERTURA']."|".$fila['F_FALLO']."|".$fila['PROYECTO']."|".$fila['UNIDAD']."|".$fila['TIPO_CONCURSO']."|".$fila['ORIGEN_RECURSO']."|".$fila['EMPRESA_GANADORA']."|".$fila['OBSERVACIONES'];
				
				$borrar = $fila['FOLIOA12']."|". 12;
        

				?>
			<tr>
				<td><?php echo $fila['F_APERTURA']; ?></td>
				<td><?php echo $fila['F_FALLO']; ?></td>
				<td><?php echo $fila['PROYECTO']; ?></td>
				<td><?php echo $fila['UNIDAD']; ?></td>
				<td><?php echo $fila['TIPO_CONCURSO']; ?></td>
				<td><?php echo $fila['ORIGEN_RECURSO']; ?></td>
				<td><?php echo $fila['EMPRESA_GANADORA']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A12" onclick="datos_a12('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion=null; ?> 	
		</tbody>
	</table>
</div>


<form id="a12">
<div class="modal fade" id="Edicion_A12" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Fecha de aperuta técnica</label>
		<input type="date" class="form-control" name="fape_a12u" id="fape_a12u">
		<br>
		<label>Fecha de fallo</label>
		<input type="date" class="form-control" name="ffa_a12u" id="ffa_a12u">
		<br>
		<label>Nombre del proyecto</label>
		<input type="text"  name="npro_a12u" id="npro_a12u" class="form-control" placeholder="Nombre del proyecto con el cual se realiza el consurso">	
		<br>
		<label>Unidad y/o programa</label>
		<input type="text"  name="uni_a12u" id="uni_a12u" placeholder="Nombre de la unidad que invita al conscurso" class="form-control">
		<br>
		<label>Tipo de concurso</label>
		<select class="form-control" name="tcon_a12u" id="tcon_a12u">
			<option value="" >Selecciona una opción...</option>
			<option value="1">Adjudicación directa</option>
			<option value="2">Cuadro coparativo</option>
			<option value="3">Invitación restringida a 3 proveedores</option>
			<option value="4">Licitación</option>
		</select>
		<br>
		<label>Origen del recurso</label>
		<textarea class="form-control" name="oricon_a12u" id="oricon_a12u" placeholder="Origen del concurso con el cual se convoca el concurso"></textarea>
		<br>
		<label>Empresa ganadora</label>
		<textarea class="form-control" name="empg_a12u" id="empg_a12u" placeholder="Empresa ganadora del concurso"></textarea>
		<br>
		<label for="conte">Observaciones</label>
		<textarea class="form-control" name="obs_a12u" id="obs_a12u" placeholder="Observaciones referentes del concurso aclaraciones u otros"></textarea>		
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>

<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a12').validate({
        rules: {
            fape_a12u: {required: true },
            ffa_a12u: {required: true},
            npro_a12u: {required: true, texto:true},
            uni_a12u: {required: true,texto:true},
            tcon_a12u: {required: true,texto:true},
            oricon_a12u: {required: true, texto:true},
            empg_a12u: {required: true, texto:true},
            obs_a12u:{required: true, texto:true}
        },
        messages: {
            fape_a12u: {
                required: "Completa el campo por favor",
            },
            ffa_a12u:{
              required: "Completa el campo por favor",
            },
            npro_a12u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor", 
            },
            uni_a12u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            tcon_a12u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",              
            },
            oricon_a12u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            empg_a12u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            obs_a12u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            
        },
        submitHandler: function () {
           actualiza_a12();
        }
    });
  });

  $('#Edicion_A12').on('hidden.bs.modal', function (e) {
      $("label.error").remove();

  });
</script>