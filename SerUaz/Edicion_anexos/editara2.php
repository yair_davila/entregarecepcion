<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: https://localhost/SerUaz/");
}
require '../conexion.php';
?>

<div class="table-responsive">
  <h4>Registros del anexo 2.- Asuntos en Trámite<span class="glyphicon glyphicon-list-alt"></h4>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th width="310px">Asunto</th>
        <th width="150px">Unidad de trámite</th>
        <th width="100px">Avance</th>
        <th width="550px">Observaciones</th>
        <th width="85px">Captura</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $sql_a1 = " SELECT FOLIOA2,ASUNTO,UNIDAD_TRAMITE,AVANCE,OBSERVACIONES,F_CAPTURA FROM ANEXO2 WHERE ID_ENTREGA = :id AND FOLIO_PERIODO =:fp";
      $res = $conexion->prepare($sql_a1);
      $res->bindValue(":id", $_SESSION['id_entrega']);
      $res->bindValue(":fp", $_SESSION['periodo']);
      $res->execute();
      while ($fila = $res->fetch(PDO::FETCH_ASSOC)) :
        $datos = $fila['FOLIOA2']."|".$fila['ASUNTO']."|".$fila['UNIDAD_TRAMITE']."|".$fila['AVANCE']."|".$fila['OBSERVACIONES'];
        $borrar = $fila['FOLIOA2'] . "|" . 2;
      ?>
        <tr>
          <td><?php echo $fila['ASUNTO']; ?></td>
          <td><?php echo $fila['UNIDAD_TRAMITE']; ?></td>
          <td><?php echo $fila['AVANCE']; ?></td>
          <td><?php echo $fila['OBSERVACIONES']; ?></td>
          <td><?php echo $fila['F_CAPTURA']; ?></td>
          <td>
            <button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A2" id="actualizar" onclick="datos_a2('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
            <button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash"></span></button>

          </td>
        </tr>
      <?php endwhile;
      $res->closeCursor();
      $conexion = null;?>
    </tbody>
  </table>
</div>



<form id="a2">
  <div class="modal fade" id="Edicion_A2" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
        </div>
        <div class="modal-body">
          <h5>Anexo 2.- Asuntos en Trámite</h5>
          <label>Asunto:</label>
          <textarea class="form-control" name="asunto_a2u" id="asunto_a2u" placeholder="Describa el asunto"></textarea>
          <br>
          <label>Unidad:</label>
          <textarea class="form-control" name="unidad_a2u" id="unidad_a2u" placeholder="Unidad con quien se realiza el trámite"></textarea>
          <br>
          <label>Avance:</label>
          <textarea class="form-control" name="avance_a2u" id="avance_a2u" placeholder="Describa el avance"></textarea>
          <br>
          <label>Observaciones:</label>
          <textarea class="form-control" name="obs_a2u" id="obs_a2u" placeholder="Describa las observaciones"></textarea>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Actualizar</button>
        </div>
      </div>
    </div>
  </div>
</form>



<script>
  $(document).ready(function() {

    $.validator.addMethod('texto', function(value, element) {
      return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
    });

    $('#a2').validate({
      rules: {
        asunto_a2u: {
          required: true,
          texto: true
        },
        unidad_a2u: {
          required: true
        },
        avance_a2u: {
          required: true,
          texto: true
        },
        obs_a2u: {
          required: true,
          texto: true
        }
      },
      messages: {
        asunto_a2u: {
          required: "Completa el campo por favor",
          texto: "No se aceptan caracteres especiales verificalo por favor",
        },
        unidad_a2u: {
          required: "Completa el campo por favor",
          texto: "No se aceptan caracteres especiales verificalo por favor",

        },
        avance_a2u: {
          required: "Completa el campo por favor",
          texto: "No se aceptan caracteres especiales verificalo por favor",
        },
        obs_a2u: {
          required: "Completa el campo por favor",
          texto: "No se aceptan caracteres especiales verificalo por favor",

        }

      },
      submitHandler: function() {
        actualiza_a2();
      }
    });
  });
  $('#Edicion_A2').on('hidden.bs.modal', function(e) {
    $("label.error").remove();
  });
</script>