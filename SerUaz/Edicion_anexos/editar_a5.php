<?php 
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  include '../conexion.php';
?>
<div class="table-responsive">
  <h4>Registros del anexo 5.- Libros, Boletines y Libretos de Consulta<span class="glyphicon glyphicon-list-alt"></h4>
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th width="100px">Cantidad</th>
        		<th width="430px">Descripción</th>
        		<th width="100px">Ubicación</th>
        		<th width="430px">Observaciones</th>
        		<th>Fecha de captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a5 = "SELECT FOLIOA5,CANTIDAD,DESCRIPCION,UBICACION,OBSERVACIONES,F_CAPTURA FROM ANEXO5 WHERE ID_ENTREGA = :id";

      		$res=$conexion->prepare($sql_a5);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA5'] . "|" . $fila['CANTIDAD'] . "|" .$fila['DESCRIPCION'] ."|". $fila['UBICACION'] . "|" . $fila['OBSERVACIONES'];
				$borrar = $fila['FOLIOA5'] . "|" . 5;
				?>
			<tr>
				<td><?php echo $fila['CANTIDAD']; ?></td>
				<td><?php echo $fila['DESCRIPCION']; ?></td>
				<td><?php echo $fila['UBICACION']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A5" id="actualizar" onclick="datos_a5('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion = null; ?> 	
		</tbody>
	</table>
</div>

<form id="a5">
<div class="modal fade" id="Edicion_A5" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Cantidad</label>
	  	<input type="text" class="form-control" id="can_a5u" name="can_a5u"  placeholder="Cantidad de libros, boletines u otros documentos en existencia">
		<br>
		<label>Descripción</label>
		<textarea class="form-control" name="des_a5u" id="des_a5u" placeholder="Descripción de libros, boletines u otros documentos en existencia"></textarea>
		<br>
		<label>Ubicación</label>
		<input type="text" class="form-control" id="ubi_a5u" name="ubi_a5u"placeholder="Ubicación de libros, boletines u otros documentos en existencia">	
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a5u" id="obs_a5u" placeholder="Observaciones referentes a los mismos"></textarea>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>


<script>
      $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a5').validate({
        rules: {
            can_a5u:{required: true, number:true},
            des_a5u:{required: true, texto:true},
            ubi_a5u:{required: true, texto:true},
            obs_a5u:{required: true, texto:true}   
        },
        messages: {
            can_a5u: {
                required: "Completa el campo por favor",
                number:"Campo solo numérico",
            },
            des_a5u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
            ubi_a5u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            obs_a5u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
            actualiza_a5();
        }
    });
  });

  $('#Edicion_A5').on('hidden.bs.modal', function (e) {
      $("label.error").remove();
  });
</script>
