<?php 
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require '../conexion.php';
?>
<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
        		<th>Tipo de documento</th>
        		<th>Número de cuenta</th>
        		<th>Institución</th>
        		<th>Folio del último <br>documento</th>
        		<th>Folio del documento <br> a iniciar</th>
        		<th width="400px">Observaciones</th>
        		<th>Captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a9 = " SELECT FOLIOA9,TIPO_DOCUMENTO,NUMERO_CUENTA,INSTITUCION,N_FINAL,N_INICIAL,OBSERVACIONES,F_CAPTURA FROM ANEXO9  WHERE ID_ENTREGA = :id";


      		$res=$conexion->prepare($sql_a9);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA9']."|".$fila['TIPO_DOCUMENTO']."|".$fila['NUMERO_CUENTA']."|".$fila['INSTITUCION']."|".$fila['N_FINAL']."|".$fila['N_INICIAL']."|".$fila['OBSERVACIONES'];
				
				$borrar = $fila['FOLIOA9']."|". 9;
				?>
			<tr>
				<td><?php echo $fila['TIPO_DOCUMENTO']; ?></td>
				<td><?php echo $fila['NUMERO_CUENTA']; ?></td>
				<td><?php echo $fila['INSTITUCION']; ?></td>
				<td><?php echo $fila['N_FINAL']; ?></td>
				<td><?php echo $fila['N_INICIAL']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A9" id="actualizar" onclick="datos_a9('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion=null;?> 	
		</tbody>
	</table>
</div>


<form id="a9">
<div class="modal fade" id="Edicion_A9" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Tipo de Documento</label>
		<select class="form-control" name="tdoc_a9u" id="tdoc_a9u">
			<option value="">Selecciona una opcion...</option>
        	<option value="1">Chequera</option>
			<option value="2">Recibos oficiales internos</option>
			<option value="3">Póliza de diario</option>
			<option value="4">Póliza de ingresos</option>
			<option value="5">Póliza de egresos</option>
			<option value="6">Otros</option>
		</select>
		<br>
      	<label>Número de Cuenta</label>
		<input type="text"  name="ncu_a9u" id="ncu_a9u" class="form-control" placeholder="Número de cuenta del documento" >
		<br>
		<label>Institución Bancaria:</label>
		<input type="text"  name="iba_a9u" id="iba_a9u" class="form-control" placeholder="Institución que emite la documentación" >
		<br>
		<label>Folio del Último Documento</label>
		<input type="text" class="form-control" id="foud_a9u"  name="foud_a9u" placeholder="Último número de folio de documento emitido" >
		<br>
		<label>Folio del Documento a Iniciar</label>
		<input type="text" class="form-control" id="foid_a9u"  name="foid_a9u" placeholder="Primer número de folio a emitir del documento" >
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a9u" id="obs_a9u" placeholder="Observaciones referentes a al corte documental"></textarea>
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>


<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a9').validate({
        rules: {
            tdoc_a9u: {required: true, texto:true},
            ncu_a9u: {required: true, number:true},
            iba_a9u: {required: true, texto:true},
            foud_a9u: {required: true, number:true},
            foid_a9u: {required: true, number:true},
            obs_a9u: {required: true, texto:true},
        },
        messages: {
            tdoc_a9u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            ncu_a9u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",

            },
            iba_a9u:{
              required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            foud_a9u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            foid_a9u:{
              required: "Completa el campo por favor",
			  number:"Campo solo numérico",              
            },
            obs_a9u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            
        },
        submitHandler: function () {
            actualiza_a9();
        }
    });
  });

  $('#Edicion_A9').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>