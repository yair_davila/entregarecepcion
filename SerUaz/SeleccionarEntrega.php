<?php  
session_start(); 
if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  		header("location:index.php");  
  }
 include 'conexion.php';

?>

<div class="container-fluid">
			 <input type="hidden" id="accion" value="seleccion"/>
			 	 <h5>Seleccionar Entrega</h5>

			 <div class="form-group">
			 	 
					<?php

			 	 	$sql_entregas=" SELECT C.nombre, EU.id_entrega, E.id_unidad, E.espacio
			 	 					FROM CAT_UNIDADES C join ENTREGA E on C.id_unidad = E.id_unidad
			 	 					join ENTREGA_USUARIOS EU on E.id_entrega = EU.id_entrega
			 	 					WHERE EU.id_usuario = :id_usr";

			 	 	$res_entregas  = $conexion-> prepare($sql_entregas);
			 	 	$res_entregas-> bindValue(":id_usr",$_SESSION['id_usuario'],PDO::PARAM_STR);

			 	 	$res_entregas ->execute();
			 	 		
			 	 	?>
			 	 	<?php if($res_entregas->rowCount() == 0):?>
			 	 		<select class="form-control" disabled>
			 	 			<option value="0">No hay entregas asignadas</option>
			 	 		</select>
			 	 	<?php endif;?>
			 	 		
			 	 	<?php if ($res_entregas->rowCount() != 0):?>
			 	 	<label>Entrgas disponibles</label>

			 	 <select id="sel_entrega" class="form-control">

              			<?php while($ren = $res_entregas -> fetch(PDO::FETCH_ASSOC)):?>
            
            			<option value="<?php echo $ren['id_entrega']?>"><?php echo $ren['nombre'] . " - " . strtoupper($ren['espacio']);?></option>
            
            			<?php endwhile;  $res_entregas ->closeCursor(); $conexion  =  null; ?>
            	</select>
            	<br>
            	<button type="button" class="btn btn-primary" onclick="sel()"><span class="glyphicon glyphicon-send"></span> Aceptar</button>		
		
            		<?php endif;?>
            </div>
        </form>
</div>