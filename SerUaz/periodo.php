<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  }

  include 'conexion.php'; 
     
    

?>
<div class="container-fluid">
  <div class="form-group">
    <h5>Crear nuevo periodo</h5>
      <input type="hidden" id="accion" value="nuevo_periodo">
        <label>Entregas disponibles</label>
        <?php
                  
                  $sql_entregas=" SELECT E.id_entrega,C.nombre, E.espacio
                  FROM CAT_UNIDADES C join ENTREGA E on C.id_unidad = E.id_unidad";// and E.id_entrega NOT IN (SELECT P.id_entrega FROM PERIODO P)";

                  $entregas_dis= $conexion-> prepare($sql_entregas);

                  $entregas_dis->execute();
        ?>

        <?php if ($entregas_dis -> rowCount() == 0): ?>

        <select class="form-control" disabled>
          
          <option value="0">No hay entregas disponibles</option>
        
        </select>
      <?php endif;?>


      <?php if($entregas_dis -> rowCount() != 0): ?>      
        <select id="dis_entrega" class="form-control">
          <?php while($fila = $entregas_dis ->fetch(PDO::FETCH_ASSOC)):?>
          <option value= "<?php echo $fila['id_entrega']?>" > <?php echo $fila['nombre'] . "- " . strtoupper($fila['espacio']) ;?> </option>
          <?php endwhile; $entregas_dis -> closeCursor();?>      
        </select>

        <label>Responsable que entrega</label>
        <input type="text" class="form-control" name="0" id="res_entrega" placeholder="Responsable que entrega" onChange="getValidaT(this.value,name,id)">
        <p id="aviso0" style="display:none; color: red;">Rellena el campo por favor<br></p>
        <p id="aviso_0" style="display:none; color: red;"><br></p>  
        <label>Responsable que recibe</label>
        <input type="text" class="form-control" name="1" id="res_recibe" placeholder="Responsable que recibe" onChange="getValidaT(this.value,name,id)">
        <p id="aviso1" style="display:none; color: red;">Rellena el campo por favor<br></p>
        <p id="aviso_1" style="display:none; color: red;"><br></p>
        <label>Fecha inicio</label>
        <input type="date" class="form-control" id="fe_inicio">
        <p id="aviso2" style="display:none; color: red;">Rellena el campo por favor<br></p>
        <label>Fecha termino</label>
        <input type="date" class="form-control" id="fe_fin">
        <p id="aviso3" style="display:none; color: red;">Rellena el campo por favor<br></p>
        <label>Anexos aplicables</label><br>
        <div class="form-group">
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="1">Anexo 1 
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="2">Anexo 2
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="3">Anexo 3
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="4">Anexo 4
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="5">Anexo 5
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="6">Anexo 6
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="7">Anexo 7
          </label>
        </div>
        <div class="form-group">
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="8">Anexo 8 
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="9">Anexo 9
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="10">Anexo 10
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="11">Anexo 11 
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="12">Anexo 12
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" name="anexos[]" value="13">Anexo 13
          </label>
        </div>
        <p id="aviso4" style="display:none; color: red;">Debes seleccionar como mínimo un anexo<br></p>
        
        <label>Tipo periodo</label>
        <select id="t_periodo" class="form-control">
          <option value="" selected>Selecciona una opción...</option>
          <option value="0">Ordianrio</option>
          <option value="1">Extraordinario</option>
        </select>
        <p id="aviso5" style="display:none; color: red;">Debes seleccionar una opción<br></p>
        <br>
        <button type="button" class="btn btn-primary" onclick="val_periodo(event)" ><span class="glyphicon glyphicon-send"></span> Crear</button>
    <?php endif; $entregas_dis->closeCursor(); ?>
  </div>

</div>

