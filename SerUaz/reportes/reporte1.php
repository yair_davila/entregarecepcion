<html>
  <head>
    <link rel="icon" type="image/png" href="icon.png">
  </head>
</html>
<?php

session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }

require '../conexion.php';
include '../mpdf/mpdf.php';

if(!is_numeric($_GET['anexo']) or (empty($_GET['anexo'])) or ($_GET['anexo'] <= 0)){
	echo "ups";


}else{


	$sql="SELECT C.NOMBRE,ESPACIO,RESP_ENTREGA,RESP_RECIBE,R.NOMBRE AS N_RCI FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA JOIN RESP_CI R ON R.RFC_CI = E.RFC_CI WHERE P.ID_ENTREGA=:id AND P.FOLIO_PERIODO = :fp";

	$con=$conexion-> prepare($sql);

	$con->bindValue(":id",$_SESSION['id_entrega']);
	$con->bindValue(":fp",$_SESSION['periodo']);

	$con ->execute();

	$res=$con->fetch(PDO::FETCH_ASSOC);
	$unidad=strtoupper($res['NOMBRE']);
	$espacio=strtoupper($res['ESPACIO']);
	$resE= strtoupper($res['RESP_ENTREGA']);
	$resR=strtoupper($res['RESP_RECIBE']);
	$resC=strtoupper($res['N_RCI']);

	//$conexion=null;	
	$con->closeCursor();

	$tabla="";
	$n_anexo="";
	$t_anexo="";

	switch ($_GET['anexo']) {
		case 1:

		$sql1="SELECT * FROM ANEXO1 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
		$res=$conexion->prepare($sql1);
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
     	$res->execute();
     	$tabla.="<table border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='400px'>OBJETIVO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='82px'>CREACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='618px'>LEGAL</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='80px'>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='80px'>ENLACE</th>
      			</tr>";

      	while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      		

      		$tabla .= "<tr>
      				  	<td style='width: 400px; font-size:20px;'>" . $fila['OBJETIVO'] . "</td>
						<td style='width: 82px; font-size:20px;'>" . $fila['CREACION'] . "</td>
						<td style='width: 618px; font-size:20px;'>". $fila['LEGAL'] . " </td>
						<td style='width: 80px; font-size:20px;'>". $fila['F_CAPTURA'] ."</td>
						<td style='width: 80px; font-size:20px;'><a href=". $fila['ENLACE']. ">Enlace</a></td>
					</tr>";
      	}
      	$conexion=null;

      	$tabla .="</table>";
      	$n_anexo=1;
      	$t_anexo="MARCO LEGAL DE ACTUACIÓN";
			break;
		case 2:

			$sql="SELECT * FROM ANEXO2 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='310px'>ASUNTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='170'>UNIDAD DE TRÁMITE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100'>AVANCE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='550'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='80'>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='85'>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];



      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='310x'>" . $fila['ASUNTO'] . "</td>
						<td style=' font-size:20px;' width='170'>" . $fila['UNIDAD_TRAMITE'] . "</td>
						<td style=' font-size:20px;' width='100'>". $fila['AVANCE'] . "</td>
						<td style=' font-size:20px;' width='550'>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width='80'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='85'>". $dir. "</td>
					</tr>";

      		}
      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=2;
      		$t_anexo="ASUNTOS EN TRÁMITE";

			break;

		case 3:

			$sql="SELECT * FROM ANEXO3 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>NOMINA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='200px'>FECHA DE CORTE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='800px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='220px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width=''>ENLACE</th>
      			</tr>";


      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['NOMINA'] . "</td>
						<td style=' font-size:20px;' width='200px'>" . $fila['CORTE'] . "</td>
						<td style=' font-size:20px;' width='800px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='220px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='80px'>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=3;
      		$t_anexo="RECURSOS HUMANOS";
			break;
		case 4:

			$sql="SELECT * FROM ANEXO4 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();


     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='200px'>INVENTARIO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='125px'>FECHA DE CORTE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='680px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='200px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='px'>ENLACE</th>
      			</tr>";
			
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='200px'>" . $fila['INVENTARIO'] . "</td>
						<td style=' font-size:20px;' width='200px'>" . $fila['FECHA'] . "</td>
						<td style=' font-size:20px;' width='680px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='210px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='80px'>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=4;
      		$t_anexo="RECURSOS MATERIALES";
			break;
		case 5:

			$sql="SELECT * FROM ANEXO5 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>CANTIDAD</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='430px'>DESCRIPCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>UBICACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='450px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='220px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['CANTIDAD'] . "</td>
						<td style=' font-size:20px;' width='430px'>" . $fila['DESCRIPCION'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['UBICACION'] . "</td>
						<td style=' font-size:20px;' width='450px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='200px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=5;
      		$t_anexo="LIBROS, BOLETINES Y LIBRETOS DE CONSULTA";
			break;
		case 6:

			$sql="SELECT * FROM ANEXO6 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>ÁREA QUE RESGUARDA </th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='430px'>TIPO DE DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>TIPO ARCHIVO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='450px'>CONTENIDO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='220px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";


      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['AREA_RESGUARDA'] . "</td>
						<td style=' font-size:20px;' width='430px'>" . $fila['TIPO_DOC'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['TIPO_ARCHIVO'] . "</td>
						<td style=' font-size:20px;' width='450px'>". $fila['CONTENIDO'] . "</td>
						<td style=' font-size:20px;' width='200px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=6;
      		$t_anexo="PRINCIPALES ARCHIVOS DE DOCUMENTOS";
			
			break;

		case 7:

			$sql="SELECT * FROM ANEXO7 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='180px'>TIPO DE INFORME</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>REPORTE FINACIERO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='150px'>FECHA DE INFORME</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='650px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>FECHA CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";


      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='180px'>" . $fila['TIPO_INFORME'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['REPORTE_FINANCIERO'] . "</td>
						<td style=' font-size:20px;' width='150px'>" . $fila['FECHA_INFORME'] . "</td>
						<td style=' font-size:20px;' width='650px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='100px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=7;
      		$t_anexo="RECURSOS FINANCIEROS - INFOMES";
			break;
		case 8:
			$sql="SELECT * FROM ANEXO8 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>TIPO DE CUENTA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>NÚMERO DE CUENTA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>INSTITUCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>SALDO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>CONCILIACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='600px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['T_CUENTA'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['NO_CUENTA'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['INSTITUCION'] . "</td>
						<td style=' font-size:20px;' width='50px'>". $fila['SALDO'] . "</td>
						<td style=' font-size:20px;' width='100px'>". $fila['CONCILIACION'] ."</td>
						<td style=' font-size:20px;' width='600px'>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=8;
      		$t_anexo="RECURSOS FINANCIEROS - BANCOS";
			break;
		case 9:
			$sql="SELECT * FROM ANEXO9 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>TIPO DE DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>NÚMERO DE CUENTA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>INSTITUCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>FOLIO DEL ÚLTIMO DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>FOLIO DEL DOCUMENTO A INICIAR</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['TIPO_DOCUMENTO'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['NUMERO_CUENTA'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['INSTITUCION'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['N_FINAL'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['N_INICIAL'] ."</td>
						<td style=' font-size:20px;' width='500px'>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=9;
      		$t_anexo="CORTE DOCUMENTAL";
			
			break;
		case 10:

			$sql="SELECT * FROM ANEXO10 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100px'>FECHA DE INFORME</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>FONDO REVOLVENTE AUTORIZADO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='180px'>FONDO DISPONIBLE EN CAJA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='270px'>DOCUMENTOS PAGADOS, PENDIENTES DE TRÁMITE Y REPOSICIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>GASTOS PENDIENTES DE COMPROBAR</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['FECHA_INFORME'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['MONTO_AUTORIZADO'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['MONTO_DISPONIBLE'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['GASTOS_PENDIENTES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['DOCUMENTOS_PAGADOS'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=10;
      		$t_anexo="FONDO REVOLVENTE";
			break;

		case 11:

			$sql="SELECT * FROM ANEXO11 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='50px'>NÚMERO DE DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='300px'>DESCRIPCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100px'>IMPORTE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='130px'>FECHA DE INICIO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='145px'>FECHA DE TÉRMINO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='300px'>OBSERVACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['NO_DOC'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['DESC_DOC'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['IMPORTE'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['F_INICIO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_TERMINO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=11;
      		$t_anexo="ACUERDOS, CONVENIOS Y CONTRATOS DE SERVICIO";
			
			break;
		case 12:

			$sql="SELECT * FROM ANEXO12 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='90px'>FECHA DE APERTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='90px'>FECHA DE FALLO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>NOMBRE DEL PROYECTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>UNIDAD Y/O PROGRAMA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>TIPO DE CONCURSO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ORIGEN DEL RECURSO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> EMPRESA GANADORA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['F_APERTURA'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['F_FALLO'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['PROYECTO'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['UNIDAD'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['TIPO_CONCURSO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['ORIGEN_RECURSO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['EMPRESA_GANADORA'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=12;
      		$t_anexo="ACTAS DE CONCURSO";
			break;
		case 13:
			$sql="SELECT * FROM ANEXO13 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_SESSION['periodo']);
     		$res->execute();

     		$tabla.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='250px'>DESCRIPCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>UBICACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100px'>MONTO AUTORIZADO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='80px'>MONTO EJERCIDO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100'>MONTO POR EJERCER</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='80px'>AVANCE FÍSICO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='80px'>AVANCE FINANCIERO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='270'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla .= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['DESC_OBRA'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['UBICACION'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['MONTO_AUTORIZA'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['MONTO_EJERCIDO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['MONTO_EJERCER'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['AVANCE_FISICO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['AVANCE_FINANCIERO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla .="</table>";
      		$n_anexo=13;
      		$t_anexo="OBRAS PÚBLICAS";
			
			break;

		default:
			
			break;
	}

	$uni = '"FRANCISCO GARCÍA SALINAS"';
	$header="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>". $t_anexo ."</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO ". $n_anexo."</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 
		 ";
$footer="<table>

			<tr>
                <td width='366px;'><hr></td>
                <td width='366px;'><hr></td>
                <td width='366px;'><hr></td>
            </tr>
            <tr>
                <td width='366px;' align='center' >".$resE."</td>
                <td width='366px;' align='center'>".$resC."</td>
                <td width='366px;' align='center'>".$resR."</td>
            </tr>
            <tr>
                <td width='366px;' align='center' >ENTREGA</td>
                <td width='366px;' align='center'>ATESTIGUA POR LA CONTRALORÍA INTERNA</td>
                <td width='366px;' align='center'>RECIBE</td>
            </tr>
        </table>
        ";
$mpdf=new mPDF('c','A4-L','','',10,10,56,47,10,10);

$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
 
 $mpdf->WriteHTML($tabla);
 $tabla="";
 $mpdf->Output();
 exit;



}



?>



