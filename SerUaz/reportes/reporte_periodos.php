<?php
session_start(); 
if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  		header("location:index.php");  
  }


 if(!is_numeric($_GET['id']) or (empty($_GET['id'])) ){
 	echo "ups";
 }
 else{


 require '../conexion.php';
 include '../mpdf/mpdf.php';
 	
 $sql="SELECT C.NOMBRE,ESPACIO,RESP_ENTREGA,RESP_RECIBE,R.NOMBRE AS N_RCI,P.ANEXOS  FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA JOIN RESP_CI R ON R.RFC_CI = E.RFC_CI WHERE P.ID_ENTREGA=:id AND P.FOLIO_PERIODO = :fp";

	$con=$conexion-> prepare($sql);

	$con->bindValue(":id",$_SESSION['id_entrega']);
	$con->bindValue(":fp",$_GET['id']);

	$con ->execute();

	$res=$con->fetch(PDO::FETCH_ASSOC);
	$unidad=strtoupper($res['NOMBRE']);
	$espacio=strtoupper($res['ESPACIO']);
	$resE= strtoupper($res['RESP_ENTREGA']);
	$resR=strtoupper($res['RESP_RECIBE']);
	$resC=strtoupper($res['N_RCI']);
	$anexo1=$res['ANEXOS'];

	$con->closeCursor();
	//$conexion=null;
	//echo $_GET['id'];

	$t_anexo="CONCENTRADO DE ANEXOS";

	//echo var_dump($anexo);
	$anexos="relleno|
	Marco Legal de Actuación|
	Asuntos en Trámite|
	Recursos Humanos|
	Recursos Materiales|
	Libros, Boletines y  Documentos de Consulta|
	Principales Archivos de Documentos|
	Recursos Financieros - Informes|
	Recursos Financieros - Bancos|
	Corte Documental|
	Fondo Revolvente|
	Acuerdos, Convenios, y Contratos de Servicios|
	Actas de Concursos|
	Obra Pública";
	$n_an= explode("|",$anexos);

	$anexo = explode(',',$anexo1);
	//echo var_dump($anexo);

	$tabla="<table table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
					<th bgcolor='#E5E7E9'>No</th>
					<th bgcolor='#E5E7E9'>NOMBRE DEL ANEXO</th>
					<th bgcolor='#E5E7E9'>REQUISITADO</th>
				</tr>";

	for ($i = 1; $i <=13; $i++){
		if(in_array($i,$anexo)){
			$reg .= '<tr><td>'.$i.'</td><td>'.$n_an[$i].'</td><td>Requisitado</td></tr>';
		}else{
			$reg.='<tr><td>'.$i.'</td><td>'.$n_an[$i].'</td><td>No Requisitado</td></tr>';
		}
	}



$x=0;
while($anexo[$x]){
 switch ($anexo[$x]) {
		case 1:
		$sql1="SELECT * FROM ANEXO1 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
		$res=$conexion->prepare($sql1);
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_GET['id']);
     	$res->execute();
     	$tabla1="<table border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='400px'>OBJETIVO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='82px'>CREACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='618px'>LEGAL</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='80px'>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='80px'>ENLACE</th>
      			</tr>";

      	while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      		
      		$part = explode('/', $fila['ENLACE'] , 4);
      		$dir=$part[3];

      		$tabla1.= "<tr>
      				  	<td style='width: 400px; font-size:20px;'>" . $fila['OBJETIVO'] . "</td>
						<td style='width: 82px; font-size:20px;'>" . $fila['CREACION'] . "</td>
						<td style='width: 618px; font-size:20px;'>". $fila['LEGAL'] . " </td>
						<td style='width: 80px; font-size:20px;'>". $fila['F_CAPTURA'] ."</td>
						<td style='width: 80px; font-size:20px;'>". $dir. "</td>
					</tr>";
      	}
      	//$conexion=null;

      	$tabla1.="</table>";
      	$n_anexo=1;
      	$t_anexo="MARCO LEGAL DE ACTUACIÓN";
			break;
		case 2:

			$sql="SELECT * FROM ANEXO2 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla2="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='310px'>ASUNTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='170px'>UNIDAD DE TRÁMITE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>AVANCE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='550px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='80px'>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='85px'>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];



      		$tabla2.= "<tr>
      				  	<td style=' font-size:20px;' width='310x'>" . $fila['ASUNTO'] . "</td>
						<td style=' font-size:20px;' width='170px'>" . $fila['UNIDAD_TRAMITE'] . "</td>
						<td style=' font-size:20px;' width='100px'>". $fila['AVANCE'] . "</td>
						<td style=' font-size:20px;' width='550px'>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width='80px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='85px'>". $dir. "</td>
					</tr>";

      		}
      		$tabla2.="</table>";
			break;
		case 3:
			$sql="SELECT * FROM ANEXO3 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla3.="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
					<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>NOMINA</th>
					<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='200px'>FECHA DE CORTE</th>
					<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='800px'>OBSERVACIONES</th>
					<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='220px'>FECHA DE CAPTURA</th>
					<th align='left' bgcolor='#E5E7E9' style='font-size:20px;'>ENLACE</th>
        		</tr>";

        	while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla3.= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['NOMINA'] . "</td>
						<td style=' font-size:20px;' width='200px'>" . $fila['CORTE'] . "</td>
						<td style=' font-size:20px;' width='800px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='220px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='80px'>". $dir. "</td>
					</tr>";

      		}
      		$tabla3.="</table>";

			break;
		case 4:

			$sql="SELECT * FROM ANEXO4 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();


     		$tabla4="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='200px'>INVENTARIO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='125px'>FECHA DE CORTE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='680px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='200px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='px'>ENLACE</th>
      			</tr>";
			
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla4.= "<tr>
      				  	<td style=' font-size:20px;' width='200px'>" . $fila['INVENTARIO'] . "</td>
						<td style=' font-size:20px;' width='200px'>" . $fila['FECHA'] . "</td>
						<td style=' font-size:20px;' width='680px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='210px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='80px'>". $dir. "</td>
					</tr>";

      		}

      		$tabla4.="</table>";
			break;
		case 5:
			
			$sql="SELECT * FROM ANEXO5 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla5="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>CANTIDAD</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='430px'>DESCRIPCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>UBICACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='450px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='220px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla5.= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['CANTIDAD'] . "</td>
						<td style=' font-size:20px;' width='430px'>" . $fila['DESCRIPCION'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['UBICACION'] . "</td>
						<td style=' font-size:20px;' width='450px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='200px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$tabla5.="</table>";
			break;
		case 6:
			$sql="SELECT * FROM ANEXO6 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla6="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>ÁREA QUE RESGUARDA </th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='430px'>TIPO DE DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>TIPO ARCHIVO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='450px'>CONTENIDO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='220px'>FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";


      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla6.= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['AREA_RESGUARDA'] . "</td>
						<td style=' font-size:20px;' width='430px'>" . $fila['TIPO_DOC'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['TIPO_ARCHIVO'] . "</td>
						<td style=' font-size:20px;' width='450px'>". $fila['CONTENIDO'] . "</td>
						<td style=' font-size:20px;' width='200px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$tabla6.="</table>";
      
			
			break;
		case 7:
			$sql="SELECT * FROM ANEXO7 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla7="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='180px'>TIPO DE INFORME</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>REPORTE FINACIERO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='150px'>FECHA DE INFORME</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='650px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>FECHA CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";


      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla7.= "<tr>
      				  	<td style=' font-size:20px;' width='180px'>" . $fila['TIPO_INFORME'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['REPORTE_FINANCIERO'] . "</td>
						<td style=' font-size:20px;' width='150px'>" . $fila['FECHA_INFORME'] . "</td>
						<td style=' font-size:20px;' width='650px'>". $fila['OBSERVACIONES'] . "</td>
						<td style=' font-size:20px;' width='100px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$tabla7.="</table>";
     
			break;
		case 8:
			$sql="SELECT * FROM ANEXO8 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla8="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>TIPO DE CUENTA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>NÚMERO DE CUENTA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>INSTITUCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>SALDO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='100px'>CONCILIACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='600px'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:20px;' width='50px'>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla8.= "<tr>
      				  	<td style=' font-size:20px;' width='100px'>" . $fila['T_CUENTA'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['NO_CUENTA'] . "</td>
						<td style=' font-size:20px;' width='100px'>" . $fila['INSTITUCION'] . "</td>
						<td style=' font-size:20px;' width='50px'>". $fila['SALDO'] . "</td>
						<td style=' font-size:20px;' width='100px'>". $fila['CONCILIACION'] ."</td>
						<td style=' font-size:20px;' width='600px'>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width='50px'>". $dir. "</td>
					</tr>";

      		}

      		$tabla8.="</table>";
			break;
		case 9:
			$sql="SELECT * FROM ANEXO9 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla9="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>TIPO DE DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>NÚMERO DE CUENTA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>INSTITUCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>FOLIO DEL ÚLTIMO DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>FOLIO DEL DOCUMENTO A INICIAR</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla9.= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['TIPO_DOCUMENTO'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['NUMERO_CUENTA'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['INSTITUCION'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['N_FINAL'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['N_INICIAL'] ."</td>
						<td style=' font-size:20px;' width='500px'>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}
      		$tabla9.="</table>";
      
			break;
		case 10:
			$sql="SELECT * FROM ANEXO10 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla10="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100px'>FECHA DE INFORME</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>FONDO REVOLVENTE AUTORIZADO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='180px'>FONDO DISPONIBLE EN CAJA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='270px'>DOCUMENTOS PAGADOS, PENDIENTES DE TRÁMITE Y REPOSICIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>GASTOS PENDIENTES DE COMPROBAR</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla10.= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['FECHA_INFORME'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['MONTO_AUTORIZADO'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['MONTO_DISPONIBLE'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['GASTOS_PENDIENTES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['DOCUMENTOS_PAGADOS'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}
      		$tabla10.="</table>";
      
			break;
		case 11:
			$sql="SELECT * FROM ANEXO11 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla11="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='50px'>NÚMERO DE DOCUMENTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='300px'>DESCRIPCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100px'>IMPORTE</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='130px'>FECHA DE INICIO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='145px'>FECHA DE TÉRMINO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='300px'>OBSERVACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla11.= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['NO_DOC'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['DESC_DOC'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['IMPORTE'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['F_INICIO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_TERMINO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}
      		$tabla11.="</table>";
      
			# code...
			break;
		case 12:
			$sql="SELECT * FROM ANEXO12 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla12="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='90px'>FECHA DE APERTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='90px'>FECHA DE FALLO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>NOMBRE DEL PROYECTO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>UNIDAD Y/O PROGRAMA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>TIPO DE CONCURSO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ORIGEN DEL RECURSO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> EMPRESA GANADORA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla12.= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['F_APERTURA'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['F_FALLO'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['PROYECTO'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['UNIDAD'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['TIPO_CONCURSO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['ORIGEN_RECURSO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['EMPRESA_GANADORA'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}
      		$tabla12.="</table>";
			break;
		case 13:
			$sql="SELECT * FROM ANEXO13 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp";
			$res=$conexion->prepare($sql);
	 		$res->bindValue(":id",$_SESSION['id_entrega']);
     		$res->bindValue(":fp",$_GET['id']);
     		$res->execute();

     		$tabla13="<table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='250px'>DESCRIPCIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='150px'>UBICACIÓN</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100px'>MONTO AUTORIZADO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='80px'>MONTO EJERCIDO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='100'>MONTO POR EJERCER</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='80px'>AVANCE FÍSICO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='80px'>AVANCE FINANCIERO</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width='270'>OBSERVACIONES</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''> FECHA DE CAPTURA</th>
        			<th align='left' bgcolor='#E5E7E9' style='font-size:15px;' width=''>ENLACE</th>
      			</tr>";

      		while($fila = $res -> fetch(PDO::FETCH_ASSOC)){
      			$part = explode('/', $fila['ENLACE'] , 4);
      			$dir=$part[3];

      		$tabla13.= "<tr>
      				  	<td style=' font-size:20px;' width=''>" . $fila['DESC_OBRA'] . "</td>
						<td style=' font-size:20px;' width=''>" . $fila['UBICACION'] . "</td>
						<td style=' font-size:20px;' width=''>". $fila['MONTO_AUTORIZA'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['MONTO_EJERCIDO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['MONTO_EJERCER'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['AVANCE_FISICO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['AVANCE_FINANCIERO'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['OBSERVACIONES'] ."</td>
						<td style=' font-size:20px;' width=''>". $fila['F_CAPTURA'] ."</td>
						<td style=' font-size:20px;' width=''>". $dir. "</td>
					</tr>";

      		}

      		$conexion=null;

      		$tabla13.="</table>";
      
			break;
		default:

			break;
	}

	$x++;
}




	$uni = '"FRANCISCO GARCÍA SALINAS"';
	$header="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>CONCENTRADOS DE ANEXOS</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'></td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 
		 ";

$footer="<table>

			<tr>
                <td width='366px;'><hr></td>
                <td width='366px;'><hr></td>
                <td width='366px;'><hr></td>
            </tr>
            <tr>
                <td width='366px;' align='center' >".$resE."</td>
                <td width='366px;' align='center'>".$resC."</td>
                <td width='366px;' align='center'>".$resR."</td>
            </tr>
            <tr>
                <td width='366px;' align='center' >ENTREGA</td>
                <td width='366px;' align='center'>ATESTIGUA POR LA CONTRALORÍA INTERNA</td>
                <td width='366px;' align='center'>RECIBE</td>
            </tr>
        </table>
        ";

$header1="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>MARCO LEGAL DE ACTUACIÓN</th>
		 		<th style='width: 366;'><img src='../../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 1</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header2="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>ASUNTOS EN TRÁMITE</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 2</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header3="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>RECURSOS HUMANOS</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 3</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";

$header4="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>RECURSOS MATERIALES</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 4</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header5="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>LIBROS, BOLETINES Y LIBRETOS DE CONSULTA</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 5</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header6="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>PRINCIPALES ARCHIVOS DE DOCUMENTOS</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 6</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";

$header7="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>RECURSOS FINANCIEROS - INFORMES</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 7</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header8="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>RECURSOS FINANCIEROS - BANCOS</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>../
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 8</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header9="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>CORTE DOCUMENTAL</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 9</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>../
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header10="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>FONDO REVOLVENTE</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 10</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header11="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>ACURDOS, CONVENIOS Y CONTRATOS DE SERVICIOS</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 11</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header12="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>ACTAS DE CONCURSO</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 12</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";
$header13="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>OBRA PÚBLICA</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'>ANEXO 13</td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 ";

$mpdf=new mPDF('c','A4-L','','',10,10,56,47,10,10);
$y=0;
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
$mpdf->WriteHTML($tabla.$reg."</table>");

while ($anexo[$y]) {

switch ($anexo[$y]){
	case 1:
		$mpdf->SetHTMLHeader($header1);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla1);

		break;
	case 2:
		$mpdf->SetHTMLHeader($header2);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla2);
		break;
	case 3:
		$mpdf->SetHTMLHeader($header3);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla3);
		break;
	case 4:
		$mpdf->SetHTMLHeader($header4);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla4);
		break;
	case 5:
		$mpdf->SetHTMLHeader($header5);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla5);
		break;
	case 6:
		$mpdf->SetHTMLHeader($header6);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla6);
		break;
	case 7:
		$mpdf->SetHTMLHeader($header7);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla7);
		break;
	case 8:
		$mpdf->SetHTMLHeader($header8);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla8);
		break;
	case 9:
		$mpdf->SetHTMLHeader($header9);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla9);
		break;
	case 10:
		$mpdf->SetHTMLHeader($header10);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla10);
		break;
	case 11:
		$mpdf->SetHTMLHeader($header11);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla11);
		break;
		case 12:
		$mpdf->SetHTMLHeader($header12);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla12);
		break;
		case 13:
		$mpdf->SetHTMLHeader($header13);
		$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
		$mpdf -> AddPage();
		$mpdf->WriteHTML($tabla13);
		break;
	default:
		# code...
		break;
}
$y++;

}

$mpdf->Output();
exit;
}
?>