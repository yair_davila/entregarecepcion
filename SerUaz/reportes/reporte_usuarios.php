<?php
session_start();
if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index

	header("location:index.php");
}
require '../conexion.php';
$res =  $conexion->prepare("SELECT NOMBRE,USUARIO,ESPACIO FROM USUARIOS U LEFT OUTER JOIN ENTREGA_USUARIOS E ON U.ID_USUARIO=E.ID_USUARIO LEFT OUTER JOIN ENTREGA N ON N.ID_ENTREGA=E.ID_ENTREGA ORDER BY USUARIO");
$res->execute();
?>

<div class="continer-fluid">
	<h4>Reporte de usuarios<span class="glyphicon glyphicon-user"></span></h4>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Usuario</th>
				<th>Entrega</th>
			</tr>
		</thead>
		<tbody>
		<?php while ($fila = $res->fetch(PDO::FETCH_ASSOC)) :?>
			<tr>
				<td><?php echo $fila['USUARIO'] ?></td>
				<td><?php echo $fila['NOMBRE']; ?></td>
				<td><?php if ($fila['ESPACIO']==null){echo "No asignado";}else echo $fila['ESPACIO']; ?></td>
			</tr>
			<?php endwhile;$res->closeCursor();$conexion = null; ?>
		</tbody>
	</table>
</div>