<?php
if (!isset($_SESSION["usuario"])){//si la varible de sesion del usuario no esta establecida se manda al index
header("Location: ../");
}
require "../conexion.php";

class repo1{

    public function __construct($id_anexo)
    {
        $this->id_anexo=$id_anexo;
        $this->date=date("d-m-Y");
    }
    /**
     * Metodo set que establece la descripcion del anexo que se generara, dependiendo del anexo y el periodo.
     */

    public function setdesc_bdd(){
        global $conexion;
        $desc=$conexion->prepare("SELECT C.NOMBRE,ESPACIO,RESP_ENTREGA,RESP_RECIBE,DATE_FORMAT(FINICIO, '%d-%m-%Y')AS INICIO,DATE_FORMAT(FFIN, '%d-%m-%Y')AS FIN,R.NOMBRE AS N_RCI FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA JOIN RESP_CI R ON R.RFC_CI = E.RFC_CI WHERE P.ID_ENTREGA=:id AND P.FOLIO_PERIODO =:fp");
        $desc->bindValue(":id",$_SESSION['id_entrega']);
        $desc->bindValue(":fp",$_SESSION['periodo']);
        $desc->execute();

	    $res=$desc->fetch(PDO::FETCH_ASSOC);
        $this->desc=array(
            "Unidad"=>mb_strtoupper($res['NOMBRE'],'utf-8'),
            "Espacio"=>mb_strtoupper($res['ESPACIO'],'utf-8'),
            "Rentrega"=>mb_strtoupper($res['RESP_ENTREGA'],'utf-8'),
            "Inicio"=>mb_strtoupper($res['INICIO'],'utf-8'),
            "Fin"=>mb_strtoupper($res['FIN'],'utf-8'),
            "Rrecibe"=>mb_strtoupper($res['RESP_RECIBE'],'utf-8'),
            "Rcontra"=>mb_strtoupper($res['N_RCI'],'utf-8')
        );
        $desc->closeCursor();

    }

    public function setheader(){
        $this->header='<table width="100%"> 
        <tr>
            <th><img src="../imagenes/uaz2.png" width="90"></th>
            <th>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>"FANCISCO GARCÍA SALINAS<br>CONTRALORÍA INTERNA<br>'.$this->namea.'</th>
            <th><img src="../imagenes/LogoCI_fondo.png" width="90"></th>
        </tr>
        <tr>
            <td>UNIDAD: '.$this->desc['Unidad'].'</td>
            <td></td>
            <td>ANEXO:'.$this->id_anexo.'</td>
        </th>
        <tr>
            <td>PROGRAMA</td>
            <td></th>
            <td>PERIODO: '.$this->desc['Inicio'].' - '.$this->desc['Fin'].'</td>
        </th>
        <tr>
            <td>ESPACIO: '.$this->desc['Espacio'].'</td>
            <td></th>
            <td>FECHA: '.$this->date.'</td>
        </th>
        </table>';
    }

    public function setfooter(){
        $this->footer='<table width="100%">
        <tr>
            <td><hr width="80%"></td>
            <td><hr width="80%"></td>
            <td><hr width="80%"></td>
        </tr>
        <tr>
            <td align="center">'.$this->desc['Rentrega'].'</td>
            <td align="center">'.$this->desc['Rcontra'].'</td>
            <td align="center">'.$this->desc['Rrecibe'].'</td>
        </tr>
        <tr>
            <td width="32%" align="center">ENTREGA</td>
            <td width="32%" align="center">ATESTIGUA POR LA CONTRALORÍA INTERNA</td>
            <td width="32%" align="center">RECIBE</td>
        </tr>
    </table>';
    }

    public function setbody(){
        switch($this->id_anexo)
        {
            case 1:
                $this->namea="MARCO LEGAL DE ACTUACIÓN";
                $this->setanexo1();
            break;
            case 2:
                $this->namea="ASUNTOS EN TRÁMITE";
                $this->setanexo2();
            break;
            case 3:
                $this->namea="RECURSOS HUMANOS";
                $this->setanexo3();
            break;
            case 4:
                $this->namea="RECURSOS MATERIALES";
                $this->setanexo4();
            break;
            case 5:
                $this->namea="LIBROS, BOLETINES Y LIBRETOS DE CONSULTA";
                $this->setanexo5();
            break;
            case 6:
                $this->namea="PRINCIPALES ARCHIVOS DE DOCUMENTOS";
                $this->setanexo6();
            break;
            case 7:
                $this->namea="RECURSOS FINANCIEROS - INFOMES";
                $this->setanexo7();
            break;
            case 8:
                $this->namea="RECURSOS FINANCIEROS - BANCOS";
                $this->setanexo8();
            break;
            case 9:
                $this->namea="CORTE DOCUMENTAL";
                $this->setanexo9();
            break;
            case 10:
                $this->namea="FONDO REVOLVENTE";
                $this->setanexo10();
            break;
            case 11:
                $this->namea="ACUERDOS, CONVENIOS Y CONTRATOS DE SERVICIO";
                $this->setanexo11();

            break;
            case 12:
                $this->namea="ACTAS DE CONCURSO";
                $this->setanexo12();
            break;
            case 13:
                $this->namea="OBRAS PÚBLICAS";
                $this->setanexo13();
            break;
            default:
            $this->error();
            break;
        }
    }

    public function error()
    {
        $this->body="ANEXO INEXISTENTE";
    }
    public function setanexo1()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT OBJETIVO,DATE_FORMAT(CREACION,'%d-%m-%Y')AS CRE,LEGAL,ENLACE,DATE_FORMAT(F_CAPTURA,'%d-%m-%Y')AS CAPTURA FROM ANEXO1 WHERE ID_ENTREGA=:id AND FOLIO_PERIODO=:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="40%">OBJETIVO</th>
                        <th width="10%">FECHA DE CREACIÓN</th>
                        <th>LEGAL</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['OBJETIVO'].'</td>
                            <td>'.$fila['CRE'].'</td>
                            <td>'.$fila['LEGAL'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
    }

    public function setanexo2()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT ASUNTO, UNIDAD_TRAMITE, AVANCE, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO2 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">ASUNTO</th>
                        <th width="15%">UNIDAD DE TRÁMITE</th>
                        <th>AVANCE</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['ASUNTO'].'</td>
                            <td>'.$fila['UNIDAD_TRAMITE'].'</td>
                            <td>'.$fila['AVANCE'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setanexo3()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT NOMINA, DATE_FORMAT(CORTE,'%d-%m-%Y')AS FCORTE, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO3 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">NOMINA</th>
                        <th width="15%">FECHA DE CORTE</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['NOMINA'].'</td>
                            <td>'.$fila['FCORTE'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    public function setanexo4()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT INVENTARIO, DATE_FORMAT(FECHA, '%d-%m-%Y') AS FECH, OBSERVACIONES, ENLACE, DATE_FORMAT(FECHA, '%d-%m-%Y') AS CAPTURA FROM ANEXO4 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">INVENTARIO</th>
                        <th width="10%">FECHA</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['INVENTARIO'].'</td>
                            <td>'.$fila['FECH'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    public function setanexo5()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT CANTIDAD, DESCRIPCION, UBICACION, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO5 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="10%">CANTIDAD</th>
                        <th width="30%">DESCRIPCIÓN</th>
                        <th width="17%">UBICACIÓN</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['CANTIDAD'].'</td>
                            <td>'.$fila['DESCRIPCION'].'</td>
                            <td>'.$fila['UBICACION'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';   
    }
    public function setanexo6()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT AREA_RESGUARDA, TIPO_DOC, TIPO_ARCHIVO, CONTENIDO, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO6 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">ÁREA QUE RESGUARDA</th>
                        <th width="12%">TIPO DE DOCUMENTO</th>
                        <th width="12%">TIPO DE ARCHIVO</th>
                        <th>CONTENIDO</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['AREA_RESGUARDA'].'</td>
                            <td>'.$fila['TIPO_DOC'].'</td>
                            <td>'.$fila['TIPO_ARCHIVO'].'</td>
                            <td>'.$fila['CONTENIDO'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setanexo7()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT TIPO_INFORME, REPORTE_FINANCIERO, DATE_FORMAT(FECHA_INFORME, '%d-%m-%Y') AS INFORME, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO7 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">TIPO DE INFORME</th>
                        <th width="10%">REPORTE FINANCIERO</th>
                        <th width="12%">FECHA DE INFORME</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['TIPO_INFORME'].'</td>
                            <td>'.$fila['REPORTE_FINANCIERO'].'</td>
                            <td>'.$fila['INFORME'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
    }

    public function setanexo8()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT T_CUENTA, NO_CUENTA, INSTITUCION, SALDO, CONCILIACION, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO8 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="10%">TIPO DE CUENTA</th>
                        <th width="15%">NUMERO DE CUENTA</th>
                        <th width="12%">INSTITUCIÓN</th>
                        <th width="10%">SALDO</th>
                        <th>CONCILIACION</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['T_CUENTA'].'</td>
                            <td>'.$fila['NO_CUENTA'].'</td>
                            <td>'.$fila['INSTITUCION'].'</td>
                            <td>'.$fila['SALDO'].'</td>
                            <td>'.$fila['CONCILIACION'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
    }

    public function setanexo9()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT TIPO_DOCUMENTO, NUMERO_CUENTA, INSTITUCION, N_FINAL, N_INICIAL, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO9 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="10%">TIPO DOCUMENTO</th>
                        <th width="15%">NUMERO DE CUENTA</th>
                        <th width="12%">INSTITUCIÓN</th>
                        <th width="15%">FOLIO ÚLTIMO DOCUMENTO</th>
                        <th>FOLIO NUEVO DOCUMENTO</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['TIPO_DOCUMENTO'].'</td>
                            <td>'.$fila['NUMERO_CUENTA'].'</td>
                            <td>'.$fila['INSTITUCION'].'</td>
                            <td>'.$fila['N_FINAL'].'</td>
                            <td>'.$fila['N_INICIAL'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setanexo10()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT DATE_FORMAT(FECHA_INFORME, '%d-%m-%Y') AS INFORME, MONTO_AUTORIZADO, MONTO_DISPONIBLE, GASTOS_PENDIENTES, DOCUMENTOS_PAGADOS, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO10 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="8%">FECHA</th>
                        <th width="15%">M.AUTORIZADO</th>
                        <th>M.DISPONIBLE</th>
                        <th width="15%">GASTOS POR COMPROBAR</th>
                        <th>DOCUMENTOS PAGADOS</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['INFORME'].'</td>
                            <td>'.$fila['MONTO_AUTORIZADO'].'</td>
                            <td>'.$fila['MONTO_DISPONIBLE'].'</td>
                            <td>'.$fila['GASTOS_PENDIENTES'].'</td>
                            <td>'.$fila['DOCUMENTOS_PAGADOS'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
         

    }

    public function setanexo11(){
        global $conexion;
        $res=$conexion->prepare("SELECT NO_DOC, DESC_DOC, IMPORTE, DATE_FORMAT(F_INICIO, '%d-%m-%Y') AS INI, DATE_FORMAT(F_TERMINO, '%d-%m-%Y') AS FIN, OBSERVACIONES, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA, ENLACE FROM ANEXO11 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
     	$res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="7%">NO.DOC</th>
                        <th width="27%">DESCRIPCIÓN</th>
                        <th>IMPORTE</th>
                        <th width="8%">INICIO</th>
                        <th>TÉRMINO</th>
                        <th>OBSERVACIÓNES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';
        
        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['NO_DOC'].'</td>
                            <td>'.$fila['DESC_DOC'].'</td>
                            <td>'.$fila['IMPORTE'].'</td>
                            <td>'.$fila['INI'].'</td>
                            <td>'.$fila['FIN'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();

        $this->body.='</table>';

    }

    public function setanexo12()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT DATE_FORMAT(F_APERTURA,'%d-%m-%Y')AS APERTURA,DATE_FORMAT(F_FALLO,'%d-%m-%Y')AS FALLO,PROYECTO,UNIDAD,TIPO_CONCURSO,ORIGEN_RECURSO,EMPRESA_GANADORA,OBSERVACIONES,ENLACE,DATE_FORMAT(F_CAPTURA,'%d-%m-%Y') AS CAPTURA FROM ANEXO12 WHERE ID_ENTREGA =:id AND FOLIO_PERIODO =:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();
        
        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="9%">APERTURA</th>
                        <th width="8%">FALLO</th>
                        <th>PROYECTO</th>
                        <th width="8%">UNIDAD</th>
                        <th>CONCURSO</th>
                        <th>ORIGEN RECURSO</th>
                        <th>EMPRESA GANADORA</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>ENLACE</th>
                    </tr>';
        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.= '<tr>
                                <td>'.$fila['APERTURA'].'</td>
                                <td>'.$fila['FALLO'].'</td>
                                <td>'.$fila['PROYECTO'].'</td>
                                <td>'.$fila['UNIDAD'].'</td>
                                <td>'.$fila['TIPO_CONCURSO'].'</td>
                                <td>'.$fila['ORIGEN_RECURSO'].'</td>
                                <td>'.$fila['EMPRESA_GANADORA'].'</td>
                                <td>'.$fila['OBSERVACIONES'].'</td>
                                <td>'.$fila['CAPTURA'].'</td>
                                <td><a href="'.$fila['ENLACE'].'">Link</a></td>
                            </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    public function setanexo13()
    {
        global $conexion;
        $res=$conexion->prepare("SELECT DESC_OBRA,UBICACION,MONTO_AUTORIZA,MONTO_EJERCIDO,MONTO_EJERCER,AVANCE_FISICO,AVANCE_FINANCIERO,OBSERVACIONES,ENLACE,DATE_FORMAT(F_CAPTURA,'%d-%m-%Y')AS CAPTURA FROM ANEXO13 WHERE ID_ENTREGA=:id AND FOLIO_PERIODO=:fp");
	 	$res->bindValue(":id",$_SESSION['id_entrega']);
     	$res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="12%">DESC DE OBRA</th>
                        <th width="9%">UBICACIÓN</th>
                        <th>M.AUTO</th>
                        <th width="8%">M.EJER</th>
                        <th>M.POR EJER</th>
                        <th>A.FÍSICO %</th>
                        <th>A.FINANCIERO</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';
        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.= '<tr>
                                <td>'.$fila['DESC_OBRA'].'</td>
                                <td>'.$fila['UBICACION'].'</td>
                                <td>'.$fila['MONTO_AUTORIZA'].'</td>
                                <td>'.$fila['MONTO_EJERCIDO'].'</td>
                                <td>'.$fila['MONTO_EJERCER'].'</td>
                                <td>'.$fila['AVANCE_FISICO'].'</td>
                                <td>'.$fila['AVANCE_FINANICERO'].'</td>
                                <td>'.$fila['OBSERVACIONES'].'</td>
                                <td>'.$fila['CAPTURA'].'</td>
                                <td><a href="'.$fila['ENLACE'].'">Link</a></td>
                            </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setclose()
    {
        global $conexion;
        $conexion=null;
    }

    public function getbody()
    {
        return $this->body;
    }
    public function  getheader()
    {
        return $this->header;
    }
    public function getfooter()
    {
        return $this->footer;
    }

    
private $id_anexo;
private $header;
private $footer;
private $desc;
private $body;
private $namea;
private $date;
}
