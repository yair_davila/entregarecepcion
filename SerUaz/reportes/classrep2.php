<?php

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
    header("location:index.php");
    }
    require "../conexion.php";

class rep2{

    public function __construct($fp)
    {
        $this->fp=$fp;
        $this->date=date("d-m-Y");
        $this->titulo="CONCENTRADO DE ANEXOS";
        $this->set_nameanexos();
    }

    public function set_nameanexos()
    {
        $this->name_anexos=array("relleno","Marco Legal de Actuación","Asuntos en Trámite","Recursos Humanos","Recursos Materiales",
                           "Libros, Boletines y  Documentos de Consulta","Principales Archivos de Documentos","Recursos Financieros - Informes",
                            "Recursos Financieros - Bancos","Corte Documental","Fondo Revolvente","Acuerdos, Convenios, y Contratos de Servicios",
                            "Actas de Concursos","Obra Pública");
    }

    public function setdes_bdd1()
    {
        global $conexion;
        $desc=$conexion->prepare("SELECT C.NOMBRE,ESPACIO,TIPO_UNIDAD,RESP_ENTREGA,RESP_RECIBE,DATE_FORMAT(FINICIO, '%d-%m-%Y')AS INICIO,DATE_FORMAT(FFIN, '%d-%m-%Y')AS FIN,R.NOMBRE AS N_RCI,ANEXOS FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA JOIN RESP_CI R ON R.RFC_CI = E.RFC_CI WHERE P.FOLIO_PERIODO =:fp");
        $desc->bindValue(":fp",$this->fp);
        $desc->execute();

	    $res=$desc->fetch(PDO::FETCH_ASSOC);
        $this->desc1=array(
            "Unidad"=>mb_strtoupper($res['NOMBRE'],'utf-8'),
            "Espacio"=>mb_strtoupper($res['ESPACIO'],'utf-8'),
            "Rentrega"=>mb_strtoupper($res['RESP_ENTREGA'],'utf-8'),
            "Inicio"=>mb_strtoupper($res['INICIO'],'utf-8'),
            "Fin"=>mb_strtoupper($res['FIN'],'utf-8'),
            "Rrecibe"=>mb_strtoupper($res['RESP_RECIBE'],'utf-8'),
            "Rcontra"=>mb_strtoupper($res['N_RCI'],'utf-8'),
            "Anexos"=>mb_strtoupper($res['ANEXOS'],'utf-8')
        );
        $desc->closeCursor();

    }


    public function setheader(){
        $this->header='<table width="100%"> 
        <tr>
            <th><img src="../imagenes/uaz2.png" width="90"></th>
            <th>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>"FANCISCO GARCÍA SALINAS<br>CONTRALORÍA INTERNA<br>'.$this->namea.'</th>
            <th><img src="../imagenes/LogoCI_fondo.png" width="90"></th>
        </tr>
        <tr>
            <td>UNIDAD: '.$this->desc1['Unidad'].'</td>
            <td></td>
            <td>ANEXO:'.$this->no.'</td>
        </th>
        <tr>
            <td>ESPACIO: '.$this->desc1['Espacio'].'</td>
            <td></th>
            <td>PERIODO: '.$this->desc1['Inicio'].' - '.$this->desc1['Fin'].'</td>
        </th>
        <tr>
            <td></td>
            <td></th>
            <td>FECHA: '.$this->date.'</td>
        </th>
        </table>';
    }

    public function setfooter(){
        $this->footer='<table width="100%">
        <tr>
            <td><hr width="80%"></td>
            <td><hr width="80%"></td>
            <td><hr width="80%"></td>
        </tr>
        <tr>
            <td align="center">'.$this->desc1['Rentrega'].'</td>
            <td align="center">'.$this->desc1['Rcontra'].'</td>
            <td align="center">'.$this->desc1['Rrecibe'].'</td>
        </tr>
        <tr>
            <td width="32%" align="center">ENTREGA</td>
            <td width="32%" align="center">ATESTIGUA POR LA CONTRALORÍA INTERNA</td>
            <td width="32%" align="center">RECIBE</td>
        </tr>
    </table>';
    }


    public function setbody1()
    {
        $this->no_anexos = explode(',',$this->desc1['Anexos']);

        $this->body1='<table width="100%" border=1 cellspacing=0 cellpadding=1>
				<tr>
					<th bgcolor="#E5E7E9">No</th>
					<th bgcolor="#E5E7E9">NOMBRE DEL ANEXO</th>
					<th bgcolor="#E5E7E9">REQUISITADO</th>
				</tr>';

	    for ($i = 1; $i <=13; $i++){
            if(in_array($i,$this->no_anexos))
            {
			    $this->body1.= '<tr><td>'.$i.'</td><td>'.$this->name_anexos[$i].'</td><td>Requisitado</td></tr>';
		    }else{
			    $this->body1.='<tr><td>'.$i.'</td><td>'.$this->name_anexos[$i].'</td><td>No Requisitado</td></tr>';
		    }
        }
        
        $this->body1.='</table>';

    }

    public function setheader10()
    {
        $this->header1='<table width="100%" cellspacing=0 cellpadding=1> 
        <tr>
            <th><img src="../imagenes/uaz2.png" width="90"></th>
            <th>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>"FANCISCO GARCÍA SALINAS<br>CONTRALORÍA INTERNA<br>'.$this->titulo.'</th>
            <th><img src="../imagenes/LogoCI_fondo.png" width="90"></th>
        </tr>
        <tr>
            <td>UNIDAD: '.$this->desc1['Unidad'].'</td>
            <td></td>
            <td>ANEXOS:'.$this->desc1['Anexos'].'</td>
        </th>
        <tr>
            <td>ESPACIO: '.$this->desc1['Espacio'].'</td>
            <td></th>
            <td>PERIODO: '.$this->desc1['Inicio'].' - '.$this->desc1['Fin'].'</td>
        </th>
        <tr>
            <td></td>
            <td></th>
            <td>FECHA: '.$this->date.'</td>
        </th>
        </table>';

    }
    public function setfooter1()
    {
        $this->footer1='<table width="100%">
        <tr>
            <td><hr width="80%"></td>
            <td><hr width="80%"></td>
            <td><hr width="80%"></td>
        </tr>
        <tr>
            <td align="center">'.$this->desc1['Rentrega'].'</td>
            <td align="center">'.$this->desc1['Rcontra'].'</td>
            <td align="center">'.$this->desc1['Rrecibe'].'</td>
        </tr>
        <tr>
            <td width="32%" align="center">ENTREGA</td>
            <td width="32%" align="center">ATESTIGUA POR LA CONTRALORÍA INTERNA</td>
            <td width="32%" align="center">RECIBE</td>
        </tr>
    </table>';

    }

    public function setbody($id){   
        switch($id)
            {
                case 1:
                    $this->setanexo1();
                break;
                case 2:
                    
                    $this->setanexo2();
                break;
                case 3:
                    $this->setanexo3();
                break;
                case 4:
                    $this->setanexo4();
                break;
                case 5:
                    $this->setanexo5();
                break;
                case 6:
                    $this->setanexo6();
                break;
                case 7:
                    $this->setanexo7();
                break;
                case 8:
                    $this->setanexo8();
                break;
                case 9:
                    $this->setanexo9();
                break;
                case 10:
                    $this->setanexo10();
                break;
                case 11:
                    $this->setanexo11();
                break;
                case 12:
                    $this->setanexo12();
                break;
                case 13:
                    $this->setanexo13();
                break;

            }
            
    }

    public function setanexo1()
    {
        $this->namea="MARCO LEGAL DE ACTUACIÓN";
        $this->no="1";
        global $conexion;
        $res=$conexion->prepare("SELECT OBJETIVO,DATE_FORMAT(CREACION,'%d-%m-%Y')AS CRE,LEGAL,ENLACE,DATE_FORMAT(F_CAPTURA,'%d-%m-%Y')AS CAPTURA FROM ANEXO1 WHERE FOLIO_PERIODO=:fp");
	 	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="40%">OBJETIVO</th>
                        <th width="10%">FECHA DE CREACIÓN</th>
                        <th>LEGAL</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['OBJETIVO'].'</td>
                            <td>'.$fila['CRE'].'</td>
                            <td>'.$fila['LEGAL'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
    }

    public function setanexo2()
    {
        $this->namea="ASUNTOS EN TRÁMITE";
        $this->no="2";
        global $conexion;
        $res=$conexion->prepare("SELECT ASUNTO, UNIDAD_TRAMITE, AVANCE, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO2 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">ASUNTO</th>
                        <th width="15%">UNIDAD DE TRÁMITE</th>
                        <th>AVANCE</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['ASUNTO'].'</td>
                            <td>'.$fila['UNIDAD_TRAMITE'].'</td>
                            <td>'.$fila['AVANCE'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setanexo3()
    {
        $this->namea="RECURSOS HUMANOS";
        $this->no="3";
        global $conexion;
        $res=$conexion->prepare("SELECT NOMINA, DATE_FORMAT(CORTE,'%d-%m-%Y')AS FCORTE, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO3 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">NOMINA</th>
                        <th width="15%">FECHA DE CORTE</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['NOMINA'].'</td>
                            <td>'.$fila['FCORTE'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    public function setanexo4()
    {
        $this->namea="RECURSOS MATERIALES";
        $this->no="4";
        global $conexion;
        $res=$conexion->prepare("SELECT INVENTARIO, DATE_FORMAT(FECHA, '%d-%m-%Y') AS FECH, OBSERVACIONES, ENLACE, DATE_FORMAT(FECHA, '%d-%m-%Y') AS CAPTURA FROM ANEXO4 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">INVENTARIO</th>
                        <th width="10%">FECHA</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['INVENTARIO'].'</td>
                            <td>'.$fila['FECH'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    public function setanexo5()
    {
        $this->namea="LIBROS, BOLETINES Y LIBRETOS DE CONSULTA";
        $this->no="5";
        global $conexion;
        $res=$conexion->prepare("SELECT CANTIDAD, DESCRIPCION, UBICACION, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO5 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="10%">CANTIDAD</th>
                        <th width="30%">DESCRIPCIÓN</th>
                        <th width="17%">UBICACIÓN</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['CANTIDAD'].'</td>
                            <td>'.$fila['DESCRIPCION'].'</td>
                            <td>'.$fila['UBICACION'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';   
    }
    public function setanexo6()
    {

        $this->namea="PRINCIPALES ARCHIVOS DE DOCUMENTOS";
        $this->no="6";
        global $conexion;
        $res=$conexion->prepare("SELECT AREA_RESGUARDA, TIPO_DOC, TIPO_ARCHIVO, CONTENIDO, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO6 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">ÁREA QUE RESGUARDA</th>
                        <th width="12%">TIPO DE DOCUMENTO</th>
                        <th width="12%">TIPO DE ARCHIVO</th>
                        <th>CONTENIDO</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['AREA_RESGUARDA'].'</td>
                            <td>'.$fila['TIPO_DOC'].'</td>
                            <td>'.$fila['TIPO_ARCHIVO'].'</td>
                            <td>'.$fila['CONTENIDO'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setanexo7()
    {
        $this->namea="RECURSOS FINANCIEROS - INFOMES";
        $this->no="7";
        global $conexion;
        $res=$conexion->prepare("SELECT TIPO_INFORME, REPORTE_FINANCIERO, DATE_FORMAT(FECHA_INFORME, '%d-%m-%Y') AS INFORME, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO7 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="20%">TIPO DE INFORME</th>
                        <th width="10%">REPORTE FINANCIERO</th>
                        <th width="12%">FECHA DE INFORME</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['TIPO_INFORME'].'</td>
                            <td>'.$fila['REPORTE_FINANCIERO'].'</td>
                            <td>'.$fila['INFORME'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
    }

    public function setanexo8()
    {
        $this->namea="RECURSOS FINANCIEROS - BANCOS";
        $this->no="8";
        global $conexion;
        $res=$conexion->prepare("SELECT T_CUENTA, NO_CUENTA, INSTITUCION, SALDO, CONCILIACION, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO8 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="10%">TIPO DE CUENTA</th>
                        <th width="15%">NUMERO DE CUENTA</th>
                        <th width="12%">INSTITUCIÓN</th>
                        <th width="10%">SALDO</th>
                        <th>CONCILIACION</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['T_CUENTA'].'</td>
                            <td>'.$fila['NO_CUENTA'].'</td>
                            <td>'.$fila['INSTITUCION'].'</td>
                            <td>'.$fila['SALDO'].'</td>
                            <td>'.$fila['CONCILIACION'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
    }

    public function setanexo9()
    {
        $this->namea="CORTE DOCUMENTAL";
        $this->no="9";
        global $conexion;
        $res=$conexion->prepare("SELECT TIPO_DOCUMENTO, NUMERO_CUENTA, INSTITUCION, N_FINAL, N_INICIAL, OBSERVACIONES, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO9 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="10%">TIPO DOCUMENTO</th>
                        <th width="15%">NUMERO DE CUENTA</th>
                        <th width="12%">INSTITUCIÓN</th>
                        <th width="15%">FOLIO ÚLTIMO DOCUMENTO</th>
                        <th>FOLIO NUEVO DOCUMENTO</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['TIPO_DOCUMENTO'].'</td>
                            <td>'.$fila['NUMERO_CUENTA'].'</td>
                            <td>'.$fila['INSTITUCION'].'</td>
                            <td>'.$fila['N_FINAL'].'</td>
                            <td>'.$fila['N_INICIAL'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }

    public function setanexo10()
    {
        $this->namea="FONDO REVOLVENTE";
        $this->no="10";
        global $conexion;
        $res=$conexion->prepare("SELECT DATE_FORMAT(FECHA_INFORME, '%d-%m-%Y') AS INFORME, MONTO_AUTORIZADO, MONTO_DISPONIBLE, GASTOS_PENDIENTES, DOCUMENTOS_PAGADOS, ENLACE, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA FROM ANEXO10 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="8%">FECHA</th>
                        <th width="15%">M.AUTORIZADO</th>
                        <th>M.DISPONIBLE</th>
                        <th width="15%">GASTOS POR COMPROBAR</th>
                        <th>DOCUMENTOS PAGADOS</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';

        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['INFORME'].'</td>
                            <td>'.$fila['MONTO_AUTORIZADO'].'</td>
                            <td>'.$fila['MONTO_DISPONIBLE'].'</td>
                            <td>'.$fila['GASTOS_PENDIENTES'].'</td>
                            <td>'.$fila['DOCUMENTOS_PAGADOS'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';
         

    }

    public function setanexo11()
    {
        $this->namea="ACUERDOS, CONVENIOS Y CONTRATOS DE SERVICIO";
        $this->no="11";
        global $conexion;
        $res=$conexion->prepare("SELECT NO_DOC, DESC_DOC, IMPORTE, DATE_FORMAT(F_INICIO, '%d-%m-%Y') AS INI, DATE_FORMAT(F_TERMINO, '%d-%m-%Y') AS FIN, OBSERVACIONES, DATE_FORMAT(F_CAPTURA, '%d-%m-%Y') AS CAPTURA, ENLACE FROM ANEXO11 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
     	$res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="7%">NO.DOC</th>
                        <th width="27%">DESCRIPCIÓN</th>
                        <th>IMPORTE</th>
                        <th width="8%">INICIO</th>
                        <th>TÉRMINO</th>
                        <th>OBSERVACIÓNES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';
        
        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.='<tr>
                            <td>'.$fila['NO_DOC'].'</td>
                            <td>'.$fila['DESC_DOC'].'</td>
                            <td>'.$fila['IMPORTE'].'</td>
                            <td>'.$fila['INI'].'</td>
                            <td>'.$fila['FIN'].'</td>
                            <td>'.$fila['OBSERVACIONES'].'</td>
                            <td>'.$fila['CAPTURA'].'</td>
                            <td><a href="'. $fila['ENLACE'].'">Link</a></td>
                          </tr>';
        }
        $res->closeCursor();

        $this->body.='</table>';

    }

    public function setanexo12()
    {
        $this->namea="ACTAS DE CONCURSO";
        $this->no="12";
        global $conexion;
        $res=$conexion->prepare("SELECT DATE_FORMAT(F_APERTURA,'%d-%m-%Y')AS APERTURA,DATE_FORMAT(F_FALLO,'%d-%m-%Y')AS FALLO,PROYECTO,UNIDAD,TIPO_CONCURSO,ORIGEN_RECURSO,EMPRESA_GANADORA,OBSERVACIONES,ENLACE,DATE_FORMAT(F_CAPTURA,'%d-%m-%Y') AS CAPTURA FROM ANEXO12 WHERE FOLIO_PERIODO =:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();
        
        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="9%">APERTURA</th>
                        <th width="8%">FALLO</th>
                        <th>PROYECTO</th>
                        <th width="8%">UNIDAD</th>
                        <th>CONCURSO</th>
                        <th>ORIGEN RECURSO</th>
                        <th>EMPRESA GANADORA</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>ENLACE</th>
                    </tr>';
        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.= '<tr>
                                <td>'.$fila['APERTURA'].'</td>
                                <td>'.$fila['FALLO'].'</td>
                                <td>'.$fila['PROYECTO'].'</td>
                                <td>'.$fila['UNIDAD'].'</td>
                                <td>'.$fila['TIPO_CONCURSO'].'</td>
                                <td>'.$fila['ORIGEN_RECURSO'].'</td>
                                <td>'.$fila['EMPRESA_GANADORA'].'</td>
                                <td>'.$fila['OBSERVACIONES'].'</td>
                                <td>'.$fila['CAPTURA'].'</td>
                                <td><a href="'.$fila['ENLACE'].'">Link</a></td>
                            </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    public function setanexo13()
    {
        $this->namea="OBRAS PÚBLICAS";
        $this->no="13";
        global $conexion;
        $res=$conexion->prepare("SELECT DESC_OBRA,UBICACION,MONTO_AUTORIZA,MONTO_EJERCIDO,MONTO_EJERCER,AVANCE_FISICO,AVANCE_FINANCIERO,OBSERVACIONES,ENLACE,DATE_FORMAT(F_CAPTURA,'%d-%m-%Y')AS CAPTURA FROM ANEXO13 WHERE FOLIO_PERIODO=:fp");
     	$res->bindValue(":fp",$this->fp);
        $res->execute();

        $this->body='<table width="100%" border=1 cellspacing=0 cellpadding=1>
                    <tr>
                        <th width="12%">DESC DE OBRA</th>
                        <th width="9%">UBICACIÓN</th>
                        <th>M.AUTO</th>
                        <th width="8%">M.EJER</th>
                        <th>M.POR EJER</th>
                        <th>A.FÍSICO %</th>
                        <th>A.FINANCIERO</th>
                        <th>OBSERVACIONES</th>
                        <th>CAPTURA</th>
                        <th>LINK</th>
                    </tr>';
        while($fila = $res -> fetch(PDO::FETCH_ASSOC))
        {
            $this->body.= '<tr>
                                <td>'.$fila['DESC_OBRA'].'</td>
                                <td>'.$fila['UBICACION'].'</td>
                                <td>'.$fila['MONTO_AUTORIZA'].'</td>
                                <td>'.$fila['MONTO_EJERCIDO'].'</td>
                                <td>'.$fila['MONTO_EJERCER'].'</td>
                                <td>'.$fila['AVANCE_FISICO'].'</td>
                                <td>'.$fila['AVANCE_FINANICERO'].'</td>
                                <td>'.$fila['OBSERVACIONES'].'</td>
                                <td>'.$fila['CAPTURA'].'</td>
                                <td><a href="'.$fila['ENLACE'].'">Link</a></td>
                            </tr>';
        }
        $res->closeCursor();
        $this->body.='</table>';

    }
    /**
     * Acta
     */

    public function setdes_desacta()
    {
        global $conexion;
        $desc=$conexion->prepare("SELECT C.NOMBRE,ESPACIO,TIPO_UNIDAD,RESP_ENTREGA,RESP_RECIBE FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA JOIN RESP_CI R ON R.RFC_CI = E.RFC_CI WHERE P.FOLIO_PERIODO =:fp");
        $desc->bindValue(":fp",$this->fp);
        $desc->execute();

	    $res=$desc->fetch(PDO::FETCH_ASSOC);
        $this->desc_acta=array(
            "Unidad"=>$res['NOMBRE'],
            "Espacio"=>$res['ESPACIO'],
            "Rentrega"=>$res['RESP_ENTREGA'],
            "Rrecibe"=>$res['RESP_RECIBE'],
            "Tunidad"=>$res['TIPO_UNIDAD']
        );
        $desc->closeCursor();

    }

    public function setheader_acta()
    {
        $this->header_acta='<table width="100%">
                            <tr>
                                <th><img src="../imagenes/encabezado.png"></th>
                            <tr/>
                            </table>';
    }
    public function setfooter_acta()
    {
        $this->footer_acta='<table width="100%">
                                <tr>
                                    <th><img src="../imagenes/piedepagina.png"></th>
                                <tr/>
                            </table>';
    }

    public function set_tipoentrega()
    {
        if($this->desc_acta['Tunidad']=="administrativa")
        {
            $this->tentrega="área administrativa de";
        }else
        {
            $this->tentrega="unidad academica de";
        }
    }


    public function setbody_acta($int,$tit,$hi,$fecha,$lu,$cd)
    {
        $this->set_tipoentrega();
        $this->set_fecha($fecha);
        $this->body_acta='<table width="100%" cellspacing=0 cellpadding=1>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:18px; text-align: justify;">Con fundamento en el Acuerdo del Rector y del Contralor Interno de la Universidad Autónoma de Zacatecas, mediante el cual se fijan las bases para la Entrega y Recepción de las Unidades Académicas y Administrativas de la Universidad Autónoma de Zacatecas;
                                    expedido el día 27 de febrero del 2009 y publicado en la edición especial número 2 de la Gaceta UAZ de fecha 13 de marzo del 2009, la Contraloría Interna, por conducto de su Titular, y de conformidad a los Artículos 7 y 9 del citado ordenamiento, del cual se desprende que de
                                    la diligencia de Entrega y Recepción se levantará acta circunstanciada, de la que se expedirá copia autógrafa al Titular saliente y al Titular entrante y en virtud de que la misma, queda facultada para dictar las disposiciones complementarias que requiera el debido cumplimiento de este Acuerdo,
                                    estando apta para interpretar sus términos en materia de efectos administrativos, sin rebasar el Marco Legal del Acuerdo citado, por ello es que en este marco se levanta la presente:</td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td width="5%"></td>
                                <td style="text-align: center; font-size:19px"><br><b>Acta administrativa</b></td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:18px; text-align: justify;"><br>Que la Entrega y Recepción de la '.$this->tentrega.' <b><u>'.$this->desc_acta["Espacio"].',</u></b> se encuentra debidamente sustentada, lo anterior de conformidad con el Acuerdo citado, que fija las bases conforme a las cuales los trabajadores de confianza y los funcionarios universitarios y
                                    que sean titulares de las Unidades Académicas y Administrativas de la Universidad Autónoma de Zacatecas, deben elaborar un informe y entregar los Recursos Humanos, 
                                    Financieros y Materiales a quienes los sustituyen en sus cargos.<br><br>En la ciudad de Zacatecas, capital del Estado del mismo nombre siendo las <b><u>'.$hi.'</u></b> hrs. del día <b><u>'.$this->fecha.'</u></b> se reunieron en las oficinas que ocupa el(la) <b><u>'.$lu.'; 
                                    '.$this->desc_acta["Rentrega"].',</u></b> quien deja de ocupar el cargo de <b><u>'.$cd.'</u></b> y <b><u>'.$this->desc_acta["Rrecibe"].',</u></b> quien a partir del día en que se actúa ocupa la titularidad de esta <b><u>área '.$this->desc_acta["Tunidad"].'.</u></b><br>
                                    <br>Para los efectos de la presente Acta, se entenderá que el Funcionario Universitario entrante, es la persona que recibe y el Funcionario Universitario saliente, es el que entrega.<br>
                                    <br>De conformidad con el Artículo 13º del mismo ordenamiento, interviene en este acto <b><u>'.$int.',</u></b> Titular de la <b><u>'.$tit.'</u></b>.<br></td>
                                <td width="5%"></td>
                            </tr>
                          </table>';
    }
    public function setbody_acta1($int,$tit)
    {
        $this->body_acta='<table width="100%" cellspacing=0 cellpadding=1>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:18px; text-align: justify;">Acreditadas las personalidades con que comparecen los participantes en esta diligencia, se procede en los términos del Artículo 8º del Acuerdo en comento, a entregar los Recursos Humanos, Materiales y Financieros asignados para el ejercicio de sus atribuciones legales, 
                                    así como los asuntos de su competencia, por lo que, para estos efectos se hace entrega del documento de Entrega Recepción,
                                    así como del respaldo del Sistema SERUAZ en dispositivo electrónico CD que contiene los informes, formatos y documentación que ampara la Entrega Recepción. </td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:18px; text-align: justify;"><br><b><u>'.$this->desc_acta["Rentrega"].',</u></b> Funcionario Universitario que entrega, manifiesta haber proporcionado con veracidad y sin omisión alguna, todos los elementos necesarios para la formulación de la presente Acta.<br> 
                                    <br>Los informes, formatos y documentos anexos que se mencionan, son parte integrante de la misma y se firman en todas sus fojas para su identificación y efectos legales a que haya lugar, las cuales están foliadas en forma consecutiva por las personas designadas para elaborarlas y verificarlas.<br>
                                    <br>Cabe señalar que la presente Acta, no implica liberación alguna de responsabilidad derivada del ejercicio de las atribuciones del Funcionario Universitario saliente, que pudieran llegarse a determinar con posterioridad por la autoridad competente.</td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:18px; text-align: justify;"><br><b><u>'.$this->desc_acta["Rrecibe"].',</u></b> recibe con las reservas de ley, de <b><u>'.$this->desc_acta["Rentrega"].',</u></b> los bienes comprendidos en los anexos, así como toda la información de los recursos y documentos que se precisan en el contenido de la presente acta.<br>
                                <br><b><u>'.$int.',</u></b> Titular de la <b><u>'.$tit.'</u></b>, hace constar que se ha acreditado la Entrega y Recepción de los anexos que se agregan y que en la elaboración de este documento se cumplieron las disposiciones legales aplicables y los requisitos formales de celebración.<br>
                                <br>De igual manera hace saber a quienes participan en este acto que se concede un plazo de 30 días hábiles para que hagan del conocimiento de la Contraloría Interna las observaciones y aclaraciones al Acta de Entrega y Recepción. </td>
                                <td width="5%"></td>
                            </tr>
                          </table>';
    }

    public function setbody_acta2($int)

    {
        $this->body_acta='<table width="100%" cellspacing=0 cellpadding=1>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:18px; text-align: justify;">De conformidad a lo señalado en el artículo 10 del Acuerdo mediante el cual se fija y establece la obligación de los trabajadores de confianza y funcionarios universitarios para instrumentar el Registro de su Situación Patrimonial,
                                    en el Sistema de Declaración de Evolución Patrimonial y Conflicto de Intereses; se hace del conocimiento del Universitario '.$this->texto.'</td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:19px; text-align: center;"><br><br><b>Cierre de acta<b></td>
                                <td width="5%"></td>
                            </tr>
                          </table><br>';
        $this->body_acta.='<table width="100%" cellspacing=0 cellpadding=1>
                            <tr>
                                <td width="5%"></td>
                                <td style="font-size:19px; text-align: center;"><br><b>Rubricas<b></td>
                                <td width="5%"></td>
                            <tr>
                          </table>';
        $this->body_acta.='<br><table width="100%" cellspacing=0 cellpadding=1>
                            <tr>
                              <td width="5%"></td>
                              <td style="text-align: center; font-size:19px;"><b>Entrega</b><br><br><br><br><hr width="90%"><br><b>'.$this->desc_acta["Rentrega"].'</b></td>
                              <td style="text-align: center; font-size:19px;"><b>Recibe</b><br><br><br><br><hr width="90%"><br><b>'.$this->desc_acta["Rrecibe"].'</b></td>
                              <td width="5%"></td>
                            <tr>
                        </table>';
        $this->body_acta.='<table width="100%" cellspacing=0 cellpadding=1>
                            <tr>
                                <td width="5%"></td>
                                <td style="text-align: center; font-size:19px;"><br><b>Por la Contraloría Interna</b><br><br><br><br><hr width="50%"><br><b>'.$int.'</b></td>
                                <td width="5%"></td>
                            <tr>
                          </table>';

    }

    public function set_declaracion($op)
    {
        switch($op)
        {
            case 1:
                $this->texto='<b>saliente</b>, que cuenta con <b>treinta días naturales</b> a partir de la fecha en que se actúa para presentar ante la Contraloría Interna su declaración de evolución patrimonial y conflicto de intereses por <b>conclusión del encargo</b>.<br>
                                De igual manera se hace del conocimiento del Universitario <b>entrante</b>, que con similar fundamento, cuenta con <b>treinta días naturales</b> para presentar su declaración de evolución patrimonial y conflicto de intereses por <b>inicio del encargo</b>.';
            break;
            case 2:
                $this->texto='<b>saliente</b>, que <b>no presentará</b> ante la Contraloría Interna declaración de evolución patrimonial y conflicto de intereses por <b>conclusión del encargo</b>; lo anterior en virtud de que en nuestro archivo <b>no se cuenta con antecedente</b> de presentación de declaración inicial.<br>
                                De igual manera se hace del conocimiento del Universitario <b>entrante</b>, que con similar fundamento, cuenta con <b>treinta días naturales</b> para presentar su declaración de evolución patrimonial y conflicto de intereses por <b>inicio del encargo</b>.';
            break;
            case 3:
                $this->texto='<b>saliente</b>, que cuenta con <b>treinta días naturales</b> a partir de la fecha en que se actúa para presentar ante la Contraloría Interna su declaración de evolución patrimonial y conflicto de intereses por <b>conclusión del encargo</b>.
                                De igual manera se hace del conocimiento del Universitario <b>entrante</b>, que en virtud de que <b>ya se cuenta con expediente a su nombre</b> en la materia, <b>no presentara</b> declaración de evolución patrimonial y conflicto de intereses <b>por inicio del encargo</b>; lo anterior tomando en cuenta que ocupa otro cargo en la Universidad.';
            break;
            case 4:
                $this->texto='<b>saliente</b>, que <b>no presentará</b> ante la Contraloría Interna declaración de evolución patrimonial y conflicto de intereses por <b>conclusión del encargo</b>; lo anterior en virtud de que en nuestro archivo <b>no se cuenta con antecedente</b> de presentación de declaración inicial.<br>
                                De igual manera se hace del conocimiento del Universitario <b>entrante</b>, que en virtud de que <b>ya se cuenta con expediente a su nombre</b> en la materia, <b>no presentara</b> declaración de evolución patrimonial y conflicto de intereses <b>por inicio del encargo</b>; lo anterior tomando en cuenta que ocupa otro cargo en la Universidad.';
            break;
            case 5:
                $this->texto='<b>saliente</b>, que <b>no presentará</b> ante la Contraloría Interna declaración de evolución patrimonial y conflicto de intereses por <b>conclusión del encargo</b>; lo anterior en virtud de que ocupara otro cargo en la universidad.<br>
                                De igual manera se hace del conocimiento del Universitario <b>entrante</b>, que con similar fundamento, cuenta con <b>treinta días naturales</b> para presentar su declaración de evolución patrimonial y conflicto de intereses por <b>inicio del encargo</b>.';
            break;
            case 6:
                $this->texto='<b>saliente</b>, que <b>no presentará</b> ante la Contraloría Interna declaración de evolución patrimonial y conflicto de intereses por <b>conclusión del encargo</b>; lo anterior en virtud de que ocupara otro cargo en la universidad.<br>
                                De igual manera se hace del conocimiento del Universitario <b>entrante</b>, que en virtud de que <b>ya se cuenta con expediente a su nombre</b> en la materia, <b>no presentara</b> declaración de evolución patrimonial y conflicto de intereses <b>por inicio del encargo</b>; lo anterior tomando en cuenta que ocupaba otro cargo en la Universidad.';
            break;
            case 7:
                $this->texto='<b>entrante</b>, que en virtud de <b>que no se cuenta con expediente a su nombre</b> en la materia, cuenta con <b>treinta días naturales</b> para presentar su declaración de evolución patrimonial y conflicto de intereses por <b>inicio del encargo</b>; lo anterior tomando en cuenta que ocupaba el mismo cargo y que es quien se entrega y recibe.';
            break;
            case 8:
                $this->texto='que <b>entrante</b>, que en virtud de <b>que ya se cuenta con expediente a su nombre</b> en la materia, <b>no presentara</b> declaración de evolución patrimonial y conflicto de intereses <b>por inicio del encargo</b>; lo anterior tomando en cuenta que ocupaba el mismo cargo y que por tanto es quien se entrega y recibe.';
            break;
        }
        
    }

    public function set_fecha($fecha)
    {
        $fecha = substr($fecha, 0, 10);
        $numeroDia = date('d', strtotime($fecha));
        $dia = date('l', strtotime($fecha));
        $mes = date('F', strtotime($fecha));
        $anio = date('Y', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        $this->fecha=$numeroDia ." de ".$nombreMes." de ".$anio;
    }

    public function setclose_conexion()
    {
        global $conexion;
        $conexion=null; 
    }


    public function getbody_acta2()
    {
        return $this->body_acta;
    }

    public function getbody_acta1()
    {
        return $this->body_acta;
    }

    public function getbody_acta()
    {
        return $this->body_acta;
    }

    public function getdesc()
    {
        return $this->desc1;
    }
    public function getheader1()
    {
        return $this->header1;
    }
    public function getfooter1()
    {
        return $this->footer1;
    }
    
    public function getbody1()
    {
        return $this->body1;
    }

    public function getno_anexos()
    {
        return $this->no_anexos;
    }
    public function getheader()
    {
        return $this->header;

    }
    public function getfooter()
    {
        return $this->footer;
    }
    public function getbody()
    {
        return $this->body;
    }
    public function getheader_acta()
    {
        return $this->header_acta;
    }
    public function getfooter_acta()
    {
        return $this->footer_acta;
    }

    

    private $fp;
    private $desc1;
    private $header1;
    private $footer1;
    private $header;
    private $footer;
    private $body1;
    private $body;
    private $titulo;
    private $date;
    private $name_anexos;
    private $no_anexos;
    private $namea;
    private $no;
    private $texto;
    private $header_acta;
    private $footer_acta;
    private $body_acta;
    private $desc_acta;
    private $tentrega;
    private $fecha;
}

?>