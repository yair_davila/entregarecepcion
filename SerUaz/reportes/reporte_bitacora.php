<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index

  header("location:index.php");
}
require '../conexion.php';
?>

<div class="form-group" id="selecion_ver">
  <label>Entregas</label>
  <?php
  $resultado =  $conexion->prepare("SELECT E.ID_ENTREGA, C.NOMBRE,E.ESPACIO FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD");  //preparacion de la consulta

  $resultado->execute();
  ?>

  <select class="form-control" id="entrega1" style="width: 500px;">;
    <?php while ($fila = $resultado->fetch(PDO::FETCH_ASSOC)) : ?>

      <option value="<?php echo  $fila['ID_ENTREGA'] ?>"> <?php echo $fila["NOMBRE"] . " - " . $fila["ESPACIO"] ?></option>

    <?php endwhile;
    $resultado->closeCursor(); ?>
  </select>
  <br>
  <button type="button" class="btn btn-primary" onclick="ok()"><span class="glyphicon glyphicon-ok"></span>Aceptar</button>

</div>

<div class="table-responsive" id="tabla_ver">
  <h4>Bitacora de trabajo</h4>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th width="30%">Entrega</th>
        <th>Anexo</th>
        <th>Evento</th>
        <th>Usuario</th>
        <th>Fecha</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if (isset($_SESSION['id_bitacora'])) :

        $reg = $conexion->prepare("SELECT NOMBRE,ESPACIO,ANEMOX_M, ANEXO,EVENTO,USUARIO,DATE_FORMAT(F_EVENTO, '%d-%m-%Y')AS FECHA FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN BITACORA B ON E.ID_ENTREGA = B.ANEMOX_M WHERE B.ANEMOX_M=:id");
        //SELECT NOMBRE,ESPACIO,ANEMOX_M, ANEXO,EVENTO,USUARIO,DATE_FORMAT(F_EVENTO, '%d-%m-%Y')AS FECHA FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN BITACORA B ON E.ID_ENTREGA = B.ANEMOX_M WHERE B.ANEMOX=:id
        $reg->bindValue(":id", $_SESSION['id_bitacora']);
        $reg->execute();

        while ($fila = $reg->fetch(PDO::FETCH_ASSOC)) :

      ?>

          <tr>
            <td><?php echo $fila['NOMBRE'] . ' - ' . $fila['ESPACIO']; ?></td>
            <td><?php echo $fila['ANEXO']; ?></td>
            <td><?php echo $fila['EVENTO']; ?></td>
            <td><?php echo $fila['USUARIO']; ?></td>
            <td><?php echo $fila['FECHA']; ?></td>
          </tr>
      <?php endwhile;
        $reg->closeCursor();
        $conexion = null;
      endif;
      $_SESSION['id_bitacora'] = null; ?>
    </tbody>
  </table>
</div>