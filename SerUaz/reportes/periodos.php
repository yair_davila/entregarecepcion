<?php 
	session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require '../conexion.php';
	  /**
	   * Consulta que muestra las entregas que tienen un historico, para que una entrega pueda tener un historico se debe cunplir lo soguiente;
	   * 1.- Debe de existir al menso más de 1 vez en diferente periodo (HAVING COUNT(P.ID_ENTREGA)>1) para esto se cuenta el id de cada entrega
	   * y se evalua que sea mayor a 1.
	   * 2 .- Una vez conociendo el id de las entregas que tiene un historico no se toma en cuenta el maximo folio (FOLIO_PERIODO != MAX(FOLIO_PERIODO)) 
	   * ya que este es el periodo actual de esta entrega 
	   */
	$per=$conexion->prepare("SELECT FOLIO_PERIODO,RESP_ENTREGA,RESP_RECIBE,DATE_FORMAT(FINICIO, '%d-%m-%Y')AS FECHAI,DATE_FORMAT(FFIN, '%d-%m-%Y')AS FECHAF,ANEXOS FROM PERIODO WHERE ID_ENTREGA IN (SELECT ID_ENTREGA FROM PERIODO GROUP BY ID_ENTREGA HAVING COUNT(ID_ENTREGA)>1 AND FOLIO_PERIODO != MAX(FOLIO_PERIODO))");
	$per->execute();
?>


<div class="container-fluid">
	<h4>Reportes de periodos</h4>
	<?php if ($per->rowCount() == 0): ?>
				<select class="form-control" disabled>
					<option value="0">Actualmente no hay periodos historicos para las entregas actuales</option>
				</select>
	<?php endif;?>


	<?php if($per->rowCount() != 0 ):?>
	 <select  class="form-control" name="periodos" id="periodos">;
            <?php while($fila = $per -> fetch(PDO::FETCH_ASSOC)):?>
                <option value="<?php echo  $fila['FOLIO_PERIODO']?>"> <?php echo $fila['RESP_ENTREGA']."->".$fila['RESP_RECIBE']. " Period:".$fila['FECHAI']."-".$fila['FECHAF']." Anexos:".$fila['ANEXOS'];?></option>
            <?php endwhile;  $per -> closeCursor();
                $conexion = null; ?>
    </select>
    <br>
    <button type="button" class="btn btn-primary" onclick="getId();"><span class="glyphicon glyphicon-ok"></span> Generar reporte</button>
	<?php endif; ?>
</div>

<script type="text/javascript">
	function getId(){
		window.open("reportes/reporteca.php?fp="+ $('#periodos').val(),"_blank");
	}
</script>