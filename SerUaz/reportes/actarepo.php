<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("location:index.php");
  } 
?>

<div class="container-fluid">
	<form id="aacta">
		<div class="form-group">
			<h4>Acta de reporte <span class="glyphicon glyphicon-book"></h4>
			<label>Fecha</label>
			<input type="date" class="form-control" name="fecha_1" id="fecha_1">
			<br>
			<label>Sede</label>
			<input type="text" class="form-control" name="sede" id="sede">
			<br>
			<label>Intervienen</label>
			<input type="text" class="form-control" name="inter" id="inter">
			<br>
			<label>Hora</label>
			<input type="text" class="form-control" name="hora" id="hora">
			<br>
			<button type="submit" class="btn btn-primary">Generar Acta</button>
		</div>
	</form>
</div>

<script type="text/javascript">

$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;:\s])*$/.test(value);
      });

    $('#aacta').validate({
        rules: {
            fecha_1: { required: true},
            sede:{required: true,texto:true},
            inter:{required: true, texto:true},
            hora:{required: true, texto:true}
               
        },
        messages: {
            fecha_1: {
                required: "Completa el campo por favor",
            },
            sede:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            inter:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            hora:{
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            }
            
        },
        submitHandler: function () {
        	let fecha= $('#fecha_1').val();
			let sede=$('#sede').val();
			let inter=$('#inter').val();
			let hora=$('#hora').val();
			window.open("reportes/con_anexos.php?1="+ fecha+"&2="+sede+"&3="+inter+"&4="+hora, "_blank");
        }
    });
  });



</script>


