<?php

session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }

require '../conexion.php';
include '../mpdf/mpdf.php';


	$sql="SELECT C.NOMBRE,ESPACIO,RESP_ENTREGA,RESP_RECIBE,R.NOMBRE AS N_RCI FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA JOIN RESP_CI R ON R.RFC_CI = E.RFC_CI WHERE P.ID_ENTREGA=:id AND P.FOLIO_PERIODO = :fp";

	$con=$conexion-> prepare($sql);

	$con->bindValue(":id",$_SESSION['id_entrega']);
	$con->bindValue(":fp",$_SESSION['periodo']);

	$con ->execute();

	$res=$con->fetch(PDO::FETCH_ASSOC);
	$unidad=strtoupper($res['NOMBRE']);
	$espacio=strtoupper($res['ESPACIO']);
	$resE= strtoupper($res['RESP_ENTREGA']);
	$resR=strtoupper($res['RESP_RECIBE']);
	$resC=strtoupper($res['N_RCI']);
	
	$con->closeCursor();

	$tabla="";
	$n_anexo="";
	$t_anexo="CONCENTRADO DE ANEXOS";

	$anexo = explode(',',$_SESSION['anexos']);

	$anexos="relleno|
	Marco Legal de Actuación|
	Asuntos en Trámite|
	Recursos Humanos|
	Recursos Materiales|
	Libros, Boletines y  Documentos de Consulta|
	Principales Archivos de Documentos|
	Recursos Financieros - Informes|
	Recursos Financieros - Bancos|
	Corte Documental|
	Fondo Revolvente|
	Acuerdos, Convenios, y Contratos de Servicios|
	Actas de Concursos|
	Obra Pública";
	$n_an= explode("|",$anexos);
	

	$tabla="<table table width='100%' border=1 cellspacing=0 cellpadding=1>
				<tr>
					<th bgcolor='#E5E7E9'>No</th>
					<th bgcolor='#E5E7E9'>NOMBRE DEL ANEXO</th>
					<th bgcolor='#E5E7E9'>REQUISITADO</th>
				</tr>";

	for ($i = 1; $i <=13; $i++){
		if(in_array($i,$anexo)){
			$reg .= '<tr><td>'.$i.'</td><td>'.$n_an[$i].'</td><td>Requisitado</td></tr>';
		}else{
			$reg.='<tr><td>'.$i.'</td><td>'.$n_an[$i].'</td><td>No Requisitado</td></tr>';
		}
	}


	$uni = '"FRANCISCO GARCÍA SALINAS"';
	$header="<table>
		 	<tr>
		 		<th style='width: 366px;'><img src='../imagenes/uaz2.png' width='100px' heigth='100px'></th>
		 		<th style='width: 366px;'>UNIVERSIDAD AUTÓNOMA DE ZACATECAS<br>".$uni."<br>CONTRALORÍA INTERNA <br>". $t_anexo ."</th>
		 		<th style='width: 366;'><img src='../imagenes/LogoCI_fondo.png' width='100px' heigth='100px'></th>
		 	</tr>
		 </table>
		 <table>
			<tr>
				<td align='left' style='width: 300px;'>Unidad: ". $unidad ."</td>
				<th style='width: 620px;'></th>
				<td align='left' style='width: 220px;'></td>
			</tr>
			<tr>
				<td>Programa: ". $espacio ."</td>
				<td></td>
				<td>Fecha: ". $_SESSION['fecha_captura']."</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>Periodo: ". $_SESSION['fper_actual']."</td>
			</tr>
		 </table>
		 
		 ";
$footer="<table>

			<tr>
                <td width='366px;'><hr></td>
                <td width='366px;'><hr></td>
                <td width='366px;'><hr></td>
            </tr>
            <tr>
                <td width='366px;' align='center' >".$resE."</td>
                <td width='366px;' align='center'>".$resC."</td>
                <td width='366px;' align='center'>".$resR."</td>
            </tr>
            <tr>
                <td width='366px;' align='center' >ENTREGA</td>
                <td width='366px;' align='center'>ATESTIGUA POR LA CONTRALORÍA INTERNA</td>
                <td width='366px;' align='center'>RECIBE</td>
            </tr>
        </table>
        ";
     $footer1="<h4>Mundo</h4>";


$mpdf=new mPDF('c','A4','','',10,10,56,47,10,10);//margen izquierdo,margen derecho,posición del contenido,incognita,margen superior,margen inferior

$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer1 . "Pag {PAGENO}/{nb}");
$mpdf->WriteHTML("Texto: ");

$mpdf -> AddPage('c','A4-L','','',10,30,30,60,10,10);

$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer . "Pag {PAGENO}/{nb}");
$mpdf->WriteHTML($tabla.$reg."</table>");
 $tabla="";
 $reg="";
 $mpdf->Output();
 exit;


?>