<?php
session_start(); 

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
      //header("Location:https://localhost/SerUaz/");
      //echo '<script>window.location="https://localhost/SerUaz/"</script>';

      //die();
        
  }

  if(!is_numeric($_GET['fp']) or (empty($_GET['fp'])) ){
    echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
  }

use Mpdf\Mpdf;
require_once __DIR__ . '/mpdf/vendor/autoload.php';
require 'classrep2.php';


  $tam=count($_GET);

  if($tam == 1)
  {
    $mpdf = new Mpdf(['orientation'=>'L',
                    'margin_top'=>70,
                    'margin_left'=>5,
                    'margin_right'=>5,
                    'margin_bottom'=>40,
                    'margin_header'=>5,
                    'margin_footer'=>5,
                    'mode' => 'utf-8',
                    'tempDir' => sys_get_temp_dir().DIRECTORY_SEPARATOR.'mpdf']);
  
    $objeto= new rep2($_GET['fp']);
    $objeto->setdes_bdd1();
    $objeto->setheader10();
    $objeto->setfooter1();
    $objeto->setbody1();
    $objeto->setheader();
    $objeto->setfooter();

    $anex=$objeto->getno_anexos();
    $total=count($anex);
    
    
    $mpdf->SetHTMLHeader($objeto->getheader1());
    $mpdf->SetHTMLFooter($objeto->getfooter1()."Pag{PAGENO}/{nbpg}");
    $mpdf->WriteHTML($objeto->getbody1());

    for($i=0;$i<$total;$i++){
    $objeto->setbody($anex[$i]);
    $objeto->setheader();
    $mpdf->SetHTMLHeader($objeto->getheader());
    $mpdf->SetHTMLFooter($objeto->getfooter()."Pag{PAGENO}/{nbpg}");
    $mpdf -> AddPage('','','','','','','','50');
    $mpdf->WriteHTML($objeto->getbody());

    }
    $objeto->setclose_conexion();
    $mpdf->Output();
    
  }
  else{
    $mpdf = new Mpdf(['orientation'=>'P',
                    'margin_top'=>50,
                    'margin_left'=>2,
                    'margin_right'=>2,
                    'margin_bottom'=>20,
                    'margin_header'=>2,
                    'margin_footer'=>2,
                    'mode' => 'utf-8',
                    'tempDir' => sys_get_temp_dir().DIRECTORY_SEPARATOR.'mpdf']);
    $object=new rep2($_GET['fp']);
 /**
  * pag#1
  */ 
    $object->setdes_desacta();//acta
    $object->setheader_acta();//header_acta
    $object->setfooter_acta();//footer_acta
    $object->setbody_acta($_GET['int'],$_GET['tit'],$_GET['hi'],$_GET['fe'],$_GET['lu'],$_GET['cd']);
    
    
  
    $mpdf->SetHTMLHeader($object->getheader_acta());
    $mpdf->SetHTMLFooter($object->getfooter_acta());
    $mpdf->WriteHTML($object->getbody_acta());
/**
 * pag#2
 */

    $mpdf -> AddPage();
    $object->setbody_acta1($_GET['int'],$_GET['tit']);
    $mpdf->WriteHTML($object->getbody_acta1());
/**
 * pag3#
 */

    $mpdf -> AddPage();
    $object->set_declaracion($_GET['op']);
    $object->setbody_acta2($_GET['int']);
    $mpdf->WriteHTML($object->getbody_acta2());
/**
 * concentrado y anexos
 */

$object->setdes_bdd1();
$object->setheader10();//concentrado
$object->setfooter1();//concentrado
$object->setbody1();//concentrado
$object->setheader();//anexos
$object->setfooter();//anexos
$mpdf->SetHTMLHeader($object->getheader1());
$mpdf-> AddPage('L','','1');
$mpdf->SetHTMLFooter($object->getfooter1()."Pag{PAGENO}/{nbpg}");
$mpdf->WriteHTML($object->getbody1());

/**
 * reportes
 */
$object->setheader();
$object->setfooter();

$anex=$object->getno_anexos();
$total=count($anex);

$mpdf->SetHTMLHeader($object->getheader());
$mpdf->SetHTMLFooter($object->getfooter()."Pag{PAGENO}/{nbpg}");

for($i=0;$i<$total;$i++){
  $object->setbody($anex[$i]);
  $object->setheader();
  $mpdf->SetHTMLHeader($object->getheader());
  $mpdf->SetHTMLFooter($object->getfooter()."Pag{PAGENO}/{nbpg}");
  $mpdf -> AddPage('L','','','','','','','50');
  $mpdf->WriteHTML($object->getbody());

  }

$object->setclose_conexion();

$mpdf->Output();
  }
?>