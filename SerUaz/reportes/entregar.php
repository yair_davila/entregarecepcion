<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index

header("location:index.php");
}
require '../conexion.php';
$reporte=$conexion->prepare("SELECT FOLIO_PERIODO,NOMBRE,UPPER(ESPACIO) AS ES,DATE_FORMAT(FINICIO, '%d-%m-%Y')AS FECHAI,DATE_FORMAT(FFIN, '%d-%m-%Y')AS FECHAF,ANEXOS FROM PERIODO P JOIN ENTREGA E ON E.ID_ENTREGA=P.ID_ENTREGA JOIN CAT_UNIDADES C ON C.ID_UNIDAD=E.ID_UNIDAD WHERE FOLIO_PERIODO IN( SELECT MAX(FOLIO_PERIODO) FROM PERIODO GROUP BY ID_ENTREGA)");
$reporte->execute();
?>
<div class="container-fluid" id="inst">
    <h4>Reportes y acta administrativa de E-R </h4>
    <label>Instrucciones</label>
    <p>En esta sección puedes generar el reporte de una entrega, así como el acta administrativa de E-R segun corresponda.<br>
        Para generar el reporte de entrega marca la opcion de <strong>"NO"</strong>. Para generar acta administrativa marca la opción de <strong>"SI"</strong></p>
    <label for="acta">¿Generar acta administrativa?</label>
    <label class="radio-inline"><input type="radio" name="acta" id="acta" value="Si"> Si</label>
	<label class="radio-inline"><input type="radio" name="acta" id="acta" value="No">   No</label>
</div>

<div class="container-fluid" id="repe" style="display:none;">
    <h4 id="titulo">Reportes de entregas</h4>
        <label>Entregas disponibles</label>
        <select  class="form-control" name="repoe" id="repoe">
            <?php while($fila = $reporte -> fetch(PDO::FETCH_ASSOC)):?>
            <option value="<?php echo  $fila['FOLIO_PERIODO']?>"><?php echo $fila['NOMBRE']."-".$fila['ES']." ". $fila['FECHAI']." - ".$fila['FECHAF']." ".$fila['ANEXOS'];?></option>
        <?php endwhile;  $reporte -> closeCursor();$conexion = null; ?>
    </select>
    <br>
    <button type="button" id="fb" class="btn btn-primary" onclick="setFp();"><span class="glyphicon glyphicon-ok"></span> Generar reporte</button>
</div>
<div class="container-fluid" id="actaer" style="display:none;">
    <form id="act">
    <label for="">Hora</label>
    <input type="text"  class="form-control" name="hi" id="hi" placeholder="Hora de la entrega recepción">
    <br>
    <label for="">Fecha</label>
    <input type="date"  class="form-control" name="fecha" id="fecha">
    <br>
    <label for="">Lugar</label>
    <input type="text"  class="form-control" name="lu" id="lu" placeholder="Describe el lugar ejemplo: unidad academica de Derecho/Odontologia etc">
    <br>
    <label for="">Cargo que se deja</label>
    <input type="text"  class="form-control" name="cd" id="cd">
    <br>
    <label for="">Interviene</label>
    <input type="text"  class="form-control" name="int" id="int" placeholder="Describe quien interviene en la entrega">
    <br>
    <label for="">Cargo</label>
    <input type="text"  class="form-control" name="tit" id="tit" placeholder="Describe el cargo de departamento ejemplo: cordinadinador de normatividad">
    <br>
    <label for="">Hora de conclución</label>
    <input type="text"  class="form-control" name="ht" id="ht" placeholder="Hora de la conclución de la entrega recepción">
    <br>
    <div class="row">
    <label>¿Registrados en el Padrón de Declaración Patrimonial?</label>
        <div class="col-md-6">
            <div class="radio">
                <label><input type="radio" name="op" value="1">Opción 1</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="op" value="2">Opción 2</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="op" value="3">Opción 3</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="op" value="4">Opción 4</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="radio">
                <label><input type="radio" name="op" value="5">Opción 5</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="op" value="6">Opción 6</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="op" value="7">Opción 7</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="op" value="8">Opción 8</label>
            </div>
        </div>
        <label for="op" class="error">
    </div>
    <br>
    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Generar reporte</button>
    </form>
</div>


<script type="text/javascript">
$(document).ready(function () {
    $('input[type="radio"]').on('change', this, function(){
        if($(this).val() == 'Si')
        {
            $('#inst').hide();
            $('#repe').show();
            $('#titulo').text("Acta administrativa de E-R y reporte");
            $("#fb").remove();
            $('#actaer').show();
        }else{
            $('#inst').hide();
            $('#repe').show();  
        }
});

$.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#act').validate({
        rules: {
            hi:{required: true},
            fecha:{required: true},
            lu:{required: true, texto:true},
            cd:{required: true, texto:true},
            int:{required: true, texto:true},
            tit:{required: true, texto:true},
            ht:{required: true},
            op:{required: true}   
        },
        messages: {
            hi:{
              required: "Completa el campo por favor",
            },
            fecha:{
              required: "Completa el campo por favor",
            },
            lu:{
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            cd:{
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },

            int:{
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
              
            },
            tit:{
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
              
            },
            ht:{
                required: "Completa el campo por favor",
              
            },
            op:{
                required: "Completa el campo por favor",
              
            }
        },
        submitHandler: function () {
            var sele = $("input[name='op']:checked").val();
            window.open("reportes/reporteca.php?fp="+$('#repoe').val()+"&hi="+$('#hi').val()+"&fe="+$('#fecha').val()+"&lu="+$('#lu').val()+"&cd="+$('#cd').val()+"&int="+$('#int').val()+"&tit="+$('#tit').val()+"&ht="+$('#ht').val()+"&op="+sele,"_blank");    
            $('#panel').load('reportes/entregar.php');
            //$('#act').trigger("reset");
        }
    });
});	
function setFp(){
        window.open("reportes/reporteca.php?fp="+$('#repoe').val(),"_blank");
        $('#panel').load('reportes/entregar.php');
    }
</script>
