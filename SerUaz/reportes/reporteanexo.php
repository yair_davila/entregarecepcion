<?php
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  }
  
  require 'classrep1.php';


require_once __DIR__ . '/mpdf/vendor/autoload.php';
use Mpdf\Mpdf;
$mpdf = new Mpdf(['orientation'=>'L',
                'margin_top'=>50,
                'margin_left'=>5,
                'margin_right'=>5,
                'margin_bottom'=>40,
                'margin_header'=>5,
                'margin_footer'=>5,
                'mode' => 'utf-8',
                'tempDir' => sys_get_temp_dir().DIRECTORY_SEPARATOR.'mpdf']);

  $obj=new repo1($_GET['anexo']);
  $obj->setdesc_bdd();
  $obj->setbody();
  $obj->setheader();
  $obj->setfooter();

  
  
  $mpdf->SetHTMLHeader($obj->getheader());
  $mpdf->SetHTMLFooter($obj->getfooter()."Pag{PAGENO}/{nbpg}");
  $mpdf->WriteHTML($obj->getbody());
  $obj->setclose();
  $mpdf->Output();
  
?>