<?php
  session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  } 
?>

<div class="container-fluid">
  <form id="ina1">
    <h4>Anexo 1.- Marco Legal de Actuación</h4>
    <label >Obljetivo</label>
    <textarea class="form-control" style="height: 140px;" name="objetivo" id="objetivo"  placeholder ="Obletivo por el cual fue creada la unidad académica o administrativa" require></textarea>
    <br>
    <label>Fecha de creación</label>
    <input type="date" class="form-control" id="f_creacion" name="f_creacion">
    <br>
    <label>Ley, Decreto, Acuerdo u Otro</label>
    <textarea class="form-control" style="height: 140px;" name="decreto" id="decreto" placeholder ="Descripción del documento legal, ley, decreto u otro" require></textarea>
    <br>
    <label>Subir Archivo</label>        
    <input type="file" name="archivo_a1" id="archivo_a1">
    <br>
    <div  id="oculto" style="display:none;">
              <div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>
    </div>
    <button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>          
  </form>
</div>

<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina1').validate({
        rules: {
            objetivo: { required: true, texto:true},
            f_creacion:{required: true},
            decreto:{required: true, texto:true},
            archivo_a1:{required: true}   
        },
        messages: {
            objetivo: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            f_creacion:{
              required: "Completa el campo por favor",
            },
            decreto:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a1:{
                required: "Selecciona un archivo",
            }
            
        },
        submitHandler: function () {
          inserta_a1();
        }
    });
  });
  
</script>

