<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: ../");
}
?>
<div class="container-fluid">
	<form id="ina13">
		<h4>Anexo 13.- Obra Pública</h4>
		<label>Descripción</label>
		<textarea class="form-control" style="height: 140px;" name="des_a13" id="des_a13" placeholder="Descripción de la obra"></textarea>
		<br>
		<label>Ubicación</label>
		<textarea class="form-control" style="height: 90px;" name="ubicacion" id="ubicacion" placeholder="Ubicación de la obra"></textarea>
		<br>
		<label>Monto autorizado</label>
		<input type="text" name="monto_a" id="monto_a" class="form-control" placeholder="Cantidad autorizada">
		<br>
		<label>Monto ejercido</label>
		<input type="text" name="monto_e" id="monto_e" class="form-control" placeholder=" Cantidad ejercida">
		<br>
		<label>Monto por ejercer</label>
		<input type="text" name="monto_pe" id="monto_pe" class="form-control" placeholder="Cantidad por ejercer">
		<br>
		<label>Avance físico (%)</label>
		<input type="text" name="avance_f" id="avance_f" class="form-control" placeholder="Cantidad %">
		<br>
		<label>Avance financiero</label>
		<input type="text" name="avance_fin" id="avance_fin" class="form-control" placeholder="Avance">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 140px;" name="obs_a13" id="obs_a13" placeholder="Observaciones de la obra"></textarea>
		<br>
		<label>Subir archivo</label>
		<input type="file" id="archivo_a13" name="archivo_a13">
		<br>
		<div id="oculto" style="display:none;">
			<div class="loading" align="center"><img src="loader.gif"></img><br />Un momento, por favor...</div>
		</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
	$(document).ready(function() {

		$.validator.addMethod('texto', function(value, element) {
			return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
		});

		$('#ina13').validate({
			rules: {
				des_a13: {
					required: true,
					texto: true
				},
				ubicacion: {
					required: true,
					texto: true
				},
				monto_a: {
					required: true,
					texto: true
				},
				monto_e: {
					required: true,
					texto: true
				},
				monto_pe: {
					required: true,
					texto: true
				},
				avance_f: {
					required: true,
					texto: true
				},
				avance_fin: {
					required: true,
					texto: true
				},
				obs_a13: {
					required: true,
					texto: true
				},
				archivo_a13: {
					required: true
				}
			},
			messages: {
				des_a13: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				ubicacion: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				monto_a: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				monto_e: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				monto_pe: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				avance_f: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				avance_fin: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				obs_a13: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				archivo_a13: {
					required: "Selecciona un archivo",
				}

			},
			submitHandler: function() {
				inserta_a13();
			}
		});
	});
</script>