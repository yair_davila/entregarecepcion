<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  } 
  ?>
<div class="container-fluid">
  <form id="ina2">
    <h4>Anexo 2.- Asuntos en Trámite</h4>
    <label>Asunto</label>
    <textarea class="form-control" style="height: 140px;" name="asunto_a2" id="asunto_a2" placeholder ="Describa el asunto"></textarea>
    <br>
    <label>Unidad</label>
    <textarea class="form-control" style="height: 90px;" name="unidad_a2" id="unidad_a2" placeholder ="Unidad con quien se realiza el trámite"></textarea>
    <br>
    <label>Avance</label>
    <textarea class="form-control" style="height: 90px;" name="avance_a2" id="avance_a2" placeholder ="Describa el avance"></textarea>
    <br>
    <label>Observaciones</label>
    <textarea class="form-control" style="height: 140px;" name="obs_a2" id="obs_a2" placeholder ="Describa las observaciones necesarias"></textarea>
    <br>
    <label>Subir Archivo</label>
    <input type="file" id="archivo_a2"  name="archivo_a2">
    <br>
    <div  id="oculto" style="background: #E6E6FA; display:none;" >
      <div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>             
    </div>
    <button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
  </form>
</div>

<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina2').validate({
        rules: {
            asunto_a2: { required: true, texto:true},
            objetivo_a2: { required: true, texto:true},
            unidad_a2:{required: true, texto:true},
            avance_a2:{required: true, texto:true},
            obs_a2:{required: true, texto:true},
            archivo_a2:{required: true}   
        },
        messages: {
          asunto_a2: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            objetivo_a2: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            unidad_a2:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            avance_a2:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            obs_a2:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a2:{
                required: "Selecciona un archivo",
            }
            
        },
        submitHandler: function () {
          inserta_a2();
        }
    });
  });
  
</script>

