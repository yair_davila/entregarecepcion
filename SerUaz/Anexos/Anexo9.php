<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: ../");
}
?>
<div class="container-fluid">
	<form id="ina9">
		<h4>Anexo 9.- Corte Documental</h4>
		<label>Tipo de Documento</label>
		<select class="form-control" id="tipo_docu" name="tipo_docu">
			<option value="" selected>Selecciona una opcion...</option>
			<option value="1">Chequera</option>
			<option value="2">Recibos oficiales internos</option>
			<option value="3">Póliza de diario</option>
			<option value="4">Póliza de ingresos</option>
			<option value="5">Póliza de egresos</option>
			<option value="6">Otros</option>
		</select>
		<br>
		<label>Número de Cuenta</label>
		<input type="text" name="num_cuenta" id="num_cuenta" class="form-control" placeholder="Número de cuenta del documento">
		<br>
		<label>Institución Bancaria</label>
		<input type="text" name="in_bancaria" id="in_bancaria" class="form-control" placeholder="Institución que emite la documentación">
		<br>
		<label>Folio del Último Documento</label>
		<input type="text" class="form-control" id="folio" name="folio" placeholder="Último número de folio de documento emitido">
		<br>
		<label>Folio del Documento a Iniciar</label>
		<input type="text" class="form-control" id="folio_nuevo" name="folio_nuevo" placeholder="Primer número de folio a emitir del documento">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 140px;" name="obs_a9" id="obs_a9" placeholder="Observaciones referentes a al corte documental"></textarea>
		<br>
		<label for="archivo">Subir Archivo</label>
		<input type="file" id="archivo_a9" name="archivo_a9">
		<br>
		<div id="oculto" style="display:none;">
			<div class="loading" align="center"><img src="loader.gif"></img><br />Un momento, por favor...</div>
		</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
	$(document).ready(function() {

		$.validator.addMethod('texto', function(value, element) {
			return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
		});

		$('#ina9').validate({
			rules: {
				tipo_docu: {
					required: true
				},
				num_cuenta: {
					required: true,
					texto: true
				},
				in_bancaria: {
					required: true,
					texto: true
				},
				folio: {
					required: true,
					texto: true
				},
				folio_nuevo: {
					required: true,
					texto: true
				},
				obs_a9: {
					required: true,
					texto: true
				},
				archivo_a9: {
					required: true
				}
			},
			messages: {
				tipo_docu: {
					required: "Completa el campo por favor",
				},
				num_cuenta: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				in_bancaria: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				folio: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				folio_nuevo: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				obs_a9: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				archivo_a9: {
					required: "Selecciona un archivo",
				}

			},
			submitHandler: function() {
				inserta_a9();
			}
		});
	});
</script>