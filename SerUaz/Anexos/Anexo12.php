<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: ../");
}
?>
<div class="container-fluid">
	<form id="ina12">
		<h4>Anexo 12.- Actas de Concurso</h4>
		<label>Fecha de Aperuta Técnica</label>
		<input type="date" class="form-control" name="fecha_a" id="fecha_a">
		<br>
		<label>Fecha de Fallo</label>
		<input type="date" class="form-control" name="fecha_fallo" id="fecha_fallo">
		<br>
		<label>Nombre del Proyecto</label>
		<input type="text" name="n_proyecto" id="n_proyecto" class="form-control" placeholder="Nombre del proyecto con el cual se realiza el consurso">
		<br>
		<label>Unidad y/o Programa</label>
		<input type="text" name="unidad" id="unidad" placeholder="Nombre de la unidad que invita al conscurso" class="form-control">
		<br>
		<label>Tipo de Concurso</label>
		<select class="form-control" name="concurso" id="concurso">
			<option value="" selected>Selecciona una opción...</option>
			<option value="1">Adjudicación directa</option>
			<option value="2">Cuadro coparativo</option>
			<option value="3">Invitación restringida a 3 proveedores</option>
			<option value="4">Licitación</option>
		</select>
		<br>
		<label>Origen del Recurso</label>
		<textarea class="form-control" style="height: 90px;" name="origen_c" id="origen_c" placeholder="Origen del concurso con el cual se convoca el concurso"></textarea>
		<br>
		<label>Empresa Ganadora</label>
		<textarea class="form-control" style="height: 90px;" name="empresa_gan" id="empresa_gan" placeholder="Empresa ganadora del concurso"></textarea>
		<br>
		<label for="conte">Observaciones</label>
		<textarea class="form-control" style="height: 90px;" name="obs_a12" id="obs_a12" placeholder="Observaciones referentes del concurso aclaraciones u otros"></textarea>
		<br>
		<label for="conte">Subir archivo</label>
		<input type="file" id="archivo_a12" name="archivo_a12">
		<br>
		<div id="oculto" style="display:none;">
			<div class="loading" align="center"><img src="loader.gif"></img><br />Un momento, por favor...</div>
		</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
	$(document).ready(function() {

		$.validator.addMethod('texto', function(value, element) {
			return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
		});

		$('#ina12').validate({
			rules: {
				fecha_a: {
					required: true
				},
				fecha_fallo: {
					required: true
				},
				n_proyecto: {
					required: true,
					texto: true
				},
				unidad: {
					required: true,
					texto: true
				},
				concurso: {
					required: true
				},
				origen_c: {
					required: true,
					texto: true
				},
				empresa_gan: {
					required: true,
					texto: true
				},
				obs_a12: {
					required: true,
					texto: true
				},
				archivo_a12: {
					required: true
				}
			},
			messages: {
				fecha_a: {
					required: "Completa el campo por favor",
				},
				fecha_fallo: {
					required: "Completa el campo por favor",
				},
				n_proyecto: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				unidad: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				concurso: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				origen_c: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				empresa_gan: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				obs_a12: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				archivo_a12: {
					required: "Selecciona un archivo",
				}

			},
			submitHandler: function() {
				inserta_a12();
			}
		});
	});
</script>