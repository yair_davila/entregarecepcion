<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  } 
?>
<div class="container-fluid">
	<form id="ina7">
		<h4>Anexo 7.- Recursos Financieros - Informes</h4>
		<label>Tipo de Informe</label>
		<select class="form-control" id="tipo_info" name="tipo_info">
			<option value="" selected>Slecciona un aopción</option>
			<option value="1">Presupuesto</option>
			<option value="2">Estado de Situación Financiera</option>
			<option value="3">Estado de actividades</option>
			<option value="4">Balanza de comprobación</option>
			<option value="5">Deudores diversos</option>
			<option value="6">Acredores diversos</option>
			<option value="7">Proveedores</option>
			<option value="8">Otros</option>
		</select>
		<br>
		<label>¿Presenta Informe?</label>
		<label class="radio-inline"><input type="radio" name="repo_inf" id="repo_inf" value="Si" checked > Si</label>
		<label class="radio-inline"><input type="radio" name="repo_inf" id="repo_inf" value="No">   No</label>
		<br>
		<label>Fecha del Documento</label>
		<input type="date" name="f_doc" id="f_doc" class="form-control">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 90px;" name="obs_a7" id="obs_a7" placeholder="Observaciones referentes al documento de informe financiero"></textarea>
		<br>
      	<label for="archivo">Subir Archivo</label>
      	<input type="file" id="archivo_a7" name="archivo_a7">
      	<br>
    	<div id="oculto" style="display:none;">
    		<div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>
    	</div>    		
    	<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina7').validate({
        rules: {
            tipo_info: { required: true},
            f_doc: { required: true},
            obs_a7:{required: true, texto:true},
            archivo_a7:{required: true}   
        },
        messages: {
          tipo_info: {
                required: "Completa el campo por favor",
            },
            f_doc: {
                required: "Completa el campo por favor",
            },
            obs_a7:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a7:{
                required: "Selecciona un archivo",
            }
        },
        submitHandler: function () {
          inserta_a7();
        }
    });
  });
</script>