<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: ../");
}
?>
<div class="container-fluid">
	<form id="ina11">
		<h4>Anexo 11.- Acuerdos, Conveniso y Contratos de Servicios</h4>
		<label>Número de documento</label>
		<input type="text" name="nume_doc" id="nume_doc" placeholder="Número de documento" class="form-control">
		<br>
		<label>Descripción</label>
		<textarea class="form-control" style="height: 140px;" name="des_a11" id="des_a11" placeholder="Describa las observaciones"></textarea>
		<br>
		<label>Importe</label>
		<input type="text" name="importe" id="importe" class="form-control" placeholder="Importe">
		<br>
		<label>Fecha de inicio</label>
		<input type="date" class="form-control" name="fecha_i" id="fecha_i">
		<br>
		<label>Fecha de término:</label>
		<input type="date" class="form-control" name="fecha_f" id="fecha_f">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 140px;" name="obs_a11" id="obs_a11" placeholder="Describa las observaciones"></textarea>
		<br>
		<label>Subir Archivo</label>
		<input type="file" id="archivo_a11" name="archivo_a11">
		<br>
		<div id="oculto" style="display:none;">
			<div class="loading" align="center"><img src="loader.gif"></img><br />Un momento, por favor...</div>
		</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
	$(document).ready(function() {

		$.validator.addMethod('texto', function(value, element) {
			return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
		});

		$('#ina11').validate({
			rules: {
				nume_doc: {
					required: true,
					texto: true
				},
				des_a11: {
					required: true,
					texto: true
				},
				importe: {
					required: true,
					texto: true
				},
				fecha_i: {
					required: true
				},
				fecha_f: {
					required: true
				},
				obs_a11: {
					required: true,
					texto: true
				},
				archivo_a11: {
					required: true
				}
			},
			messages: {
				nume_doc: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				des_a11: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				importe: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				fecha_i: {
					required: "Completa el campo por favor",
				},
				fecha_f: {
					required: "Completa el campo por favor",
				},
				obs_a11: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				archivo_a11: {
					required: "Selecciona un archivo",
				}

			},
			submitHandler: function() {
				inserta_a11();
			}
		});
	});
</script>

