<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: ../");
}
?>
<div class="container-fluid">
	<form id="ina10">
		<h4>Anexo 10.- Fondo Revolvente</h4>
		<label>Fecha de Informe</label>
		<input type="date" class="form-control" name="f_informe" id="f_informe">
		<br>
		<label>Fondo Revolvente Autorizado</label>
		<input type="text" name="fondo_autorizado" id="fondo_autorizado" class="form-control" placeholder="Monto total autorizado">
		<br>
		<label>Disponible en Fondo de Caja (efectivo)</label>
		<input type="text" name="fondo_caja" id="fondo_caja" class="form-control" placeholder="Monto disponible en efectivo a la fecha de corte">
		<br>
		<label>Documentos Pagados, Pendientes de Tramite y Reposición</label>
		<input type="text" name="documentos" id="documentos" class="form-control" placeholder="Total de la cantidad de los documentos">
		<br>
		<label>Gastos Pendientes de Comprobar</label>
		<input type="text" name="gastos_p" id="gastos_p" class="form-control" placeholder="Gastos pendientes por comprobar a la fecha de corte">
		<br>
		<label for="archivo">Subir archivo</label>
		<input type="file" id="archivo_a10" name="archivo_a10">
		<br>
		<div id="oculto" style="display:none;">
			<div class="loading" align="center"><img src="loader.gif"></img><br />Un momento, por favor...</div>
		</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>
<script>
	$(document).ready(function() {

		$.validator.addMethod('texto', function(value, element) {
			return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
		});

		$('#ina10').validate({
			rules: {
				f_informe: {
					required: true
				},
				fondo_autorizado: {
					required: true,
					texto: true
				},
				fondo_caja: {
					required: true,
					texto: true
				},
				documentos: {
					required: true,
					texto: true
				},
				gastos_p: {
					required: true,
					texto: true
				},
				archivo_a10: {
					required: true
				}
			},
			messages: {
				f_informe: {
					required: "Completa el campo por favor",
				},
				fondo_autorizado: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				fondo_caja: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				documentos: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				gastos_p: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				archivo_a10: {
					required: "Selecciona un archivo",
				}

			},
			submitHandler: function() {
				inserta_a10();
			}
		});
	});
</script>