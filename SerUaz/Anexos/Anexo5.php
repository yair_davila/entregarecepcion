<?php 
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  } 
?>
<div class="container-fluid">
	<form id="ina5">
       	<h4>Anexo 5.- Libros, Boletines y Documentos de Consulta</h4>
		<label>Cantidad</label>
		<input type="text" class="form-control" id="cantidad" name="cantidad"  placeholder="Cantidad de libros, boletines u otros documentos en existencia">
		<br>
		<label>Descripción</label>
		<textarea class="form-control" style="height: 90px;" name="des_a5" id="des_a5" placeholder="Descripción de libros, boletines u otros documentos en existencia"></textarea>
		<br>
		<label>Ubicación</label>
		<input type="text" class="form-control" id="ubicacion" name="ubicacion" placeholder="Ubicación de libros, boletines u otros documentos en existencia">	
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 90px;" name="obs_a5" id="obs_a5" placeholder="Observaciones referentes a los mismos"></textarea>
		<br>
      	<label>Subir Archivo</label>
      	<input type="file" id="archivo_a5" name="archivo_a5">
      	<br>
    	<div id="oculto" style="display:none;">
    		<div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>
    	</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina5').validate({
        rules: {
            cantidad: {required: true, texto:true},
			des_a5: {required: true,texto:true},
			ubicacion:{ required: true,texto:true},
            obs_a5:{required: true, texto:true},
            archivo_a5:{required: true}   
        },
        messages: {
          cantidad: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            des_a5: {
				required: "Completa el campo por favor",
				texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            ubicacion:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
			},
			obs_a5:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a5:{
                required: "Selecciona un archivo",
            }
            
        },
        submitHandler: function () {
          inserta_a5();
        }
    });
  });
</script>