<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: ../");
}
?>
<div class="container-fluid">
	<form id="ina8">
		<h4>Anexo 8.- Recursos Financieros - Bancos </h4>
		<label>Tipo de Cuenta</label>
		<select class="form-control" name="t_cuenta" id="t_cuenta">
			<option value="" selected>Selecciona una opción...</option>
			<option value="Cheques">Cheques</option>
			<option value="Inversiones">Inversiones</option>
		</select>
		<br>
		<label>Número de Cuenta</label>
		<input type="text" name="n_cuenta" id="n_cuenta" class="form-control" placeholder="Número de cuenta bancaria">
		<br>
		<label>Institución Bancaria</label>
		<input type="text" name="i_bancaria" id="i_bancaria" class="form-control" placeholder="Institución Bancaria con la cual se celebra la cuenta">
		<br>
		<label>Saldo</label>
		<input type="text" name="saldo" id="saldo" class="form-control" placeholder="Saldo actual">
		<br>
		<label>Conciliación Bancaria</label>
		<label class="radio-inline"><input type="radio" name="conb_an8" id="conb_an8" value="Si" checked> Si</label>
		<label class="radio-inline"><input type="radio" name="conb_an8" id="conb_an8" value="No"> No</label>
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 140px;" name="obs_a8" id="obs_a8" placeholder="Observaciones respecto a lo concerniente al informe de recursos financieros"></textarea>
		<br>
		<label for="archivo">Subir Archivo</label>
		<input type="file" id="archivo_a8" name="archivo_a8">
		<br>
		<div id="oculto" style="display:none;">
			<div class="loading" align="center"><img src="loader.gif"></img><br />Un momento, por favor...</div>
		</div>
		<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
	$(document).ready(function() {

		$.validator.addMethod('texto', function(value, element) {
			return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
		});

		$('#ina8').validate({
			rules: {
				t_cuenta: {
					required: true
				},
				n_cuenta: {
					required: true,
					texto: true
				},
				i_bancaria: {
					required: true,
					texto: true
				},
				saldo: {
					required: true,
					texto: true
				},
				obs_a8: {
					required: true,
					texto: true
				},
				archivo_a8: {
					required: true
				}
			},
			messages: {
				t_cuenta: {
					required: "Completa el campo por favor",
				},
				n_cuenta: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				i_bancaria: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				saldo: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				obs_a8: {
					required: "Completa el campo por favor",
					texto: "No se aceptan caracteres especiales verificalo por favor",
				},
				archivo_a8: {
					required: "Selecciona un archivo",
				}

			},
			submitHandler: function() {
				inserta_a8();
			}
		});
	});
</script>