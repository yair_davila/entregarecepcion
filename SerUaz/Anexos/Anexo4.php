<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  } 
?>
<div class="container-fluid">
	<form id="ina4">
    	<h4>Anexo 4.- Recursos Materiales</h4>
		<label>Tipo de Inventario</label>
		<select class="form-control" name="inventario" id="inventario">
			<option value="" selected>Selecciona una opción...</option>
			<option value="1">Bienes, muebles, informáticos</option>
			<option value="2">Vehículos y transporte</option>
			<option value="3">Bienes e inmuebles</option>
			<option value="4">Existencia en almacen</option>
			<option value="5">Relación de bajas</option>
		</select>
		<br>
		<label>Fecha de Corte</label>
		<input type="date" class="form-control" name="f_cortea4" id="f_cortea4">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" style="height: 140px;" name="obs_a4" id="obs_a4" placeholder="Describa las observaciones"></textarea>
		<br>
		<label>Subir Archivo</label>
      	<input type="file" name="archivo_a4" id="archivo_a4">
		<br>
		<div  id="oculto" style="display:none;">
       		<div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>
    	</div>
    	<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>

<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina4').validate({
        rules: {
            inventario: { required: true},
            f_cortea4: { required: true},
            obs_a4:{required: true, texto:true},
            archivo_a4:{required: true}   
        },
        messages: {
          inventario: {
                required: "Completa el campo por favor",
            },
            f_cortea4: {
                required: "Completa el campo por favor",
            },
            obs_a4:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a4:{
                required: "Selecciona un archivo",
            }
            
        },
        submitHandler: function () {
          inserta_a4();
        }
    });
  });
</script>
