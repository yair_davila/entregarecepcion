<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  }
?>  
 <div class="container-fluid">
      <form id="ina3">
        <h4>Anexo 3.- Recursos Humanos</h4>
    		<label> Tipo de Nomina</label>
      	<select class="form-control" id="tipo_nomina" name="tipo_nomina">
          <option value="">Selecciona una opción ...</option>
          <option value="Ordinaria">Ordinaria</option>   
        </select>
        <br>
    		<label>Fecha de Corte</label>
        <input type="date" class="form-control" name="fecha_corte" id="fecha_corte" >
        <br>
        <label>Observaciones</label>
        <textarea class="form-control" style="height: 140px;" name="obs_a3" id="obs_a3" placeholder="Describa las observaciones"></textarea>
        <br>
			  <label>Subir archivo</label>
			  <input type="file" name="archivo_a3" id="archivo_a3">
        <br>
        <div  id="oculto" style="display:none;">
          <div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>
        </div>
  		  <button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
      </form>
</div>

<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina3').validate({
        rules: {
            tipo_nomina: { required: true, texto:true},
            fecha_corte: { required: true},
            obs_a3:{required: true, texto:true},
            archivo_a3:{required: true}   
        },
        messages: {
          tipo_nomina: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            fecha_corte: {
                required: "Completa el campo por favor",
            },
            obs_a3:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a3:{
                required: "Selecciona un archivo",
            }
            
        },
        submitHandler: function () {
          inserta_a3();
        }
    });
  });
</script>