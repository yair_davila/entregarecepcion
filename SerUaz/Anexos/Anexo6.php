<?php
session_start();

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
  } 
?>
<div class="container-fluid">
	<form id="ina6"> 
		<h4>Anexo 6.- Principales Archivos de Documentos</h4>
		<label>Área que resguarda</label>
		<input type="text" class="form-control" name="area" id="area" placeholder="Área de unidad académica o administrativa">
		<br>
		<label>Tipo de Documento</label>
		<select class="form-control" id="tipo_doc" name="tipo_doc">
			<option value="" selected>Selecciona una opción...</option>
			<option value="1">Archivos de documentos</option>
			<option value="2">Archivos electrónicos</option>
		</select>
		<br>
		<label>Tipo de Archivo</label>
		<select class="form-control" id="tipo_archivo" name="tipo_archivo">
			<option value="" selected>Selecciona una opción...</option>
			<option value="Vigente">Vigente</option>
			<option value="Muerto">Muerto</option>
		</select>
		<br>
		<label>Documentos y/o Archivos que Contiene</label>
		<textarea class="form-control" style="height: 90px;" name="documento" id="documento"></textarea>
		<br>
      	<label>Subir Archivo</label>
      	<input type="file" name="archivo_a6" id="archivo_a6">
		<br>
    	<div  id="oculto" style="display:none;">
    		<div class="loading" align="center"><img src="loader.gif"></img><br/>Un momento, por favor...</div>
    	</div>
    	<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Enviar</button>
	</form>
</div>
<script>
  $(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });

    $('#ina6').validate({
        rules: {
            area: { required: true, texto:true},
            tipo_doc: { required: true},
			tipo_archivo:{required: true},
			documento:{required: true,texto:true},
            archivo_a6:{required: true}   
        },
        messages: {
          area: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            tipo_doc: {
                required: "Completa el campo por favor",
				
			},
            tipo_archivo:{
                required:"Completa el campo por favor",
			},
			documento:{
                required:"Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            archivo_a6:{
                required: "Selecciona un archivo",
            }
            
        },
        submitHandler: function () {
          inserta_a6();
        }
    });
  });
</script>