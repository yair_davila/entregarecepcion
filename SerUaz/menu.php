<?php
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                                                                                                                            */
/*                                                                                                                                                            */
/*                                    Script donde se encuntran las funciones necesarias para el menu principal del sistema                                   */
/*                                                                                                                                                            */
/*                                                                                                                                                            */
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

session_start();
if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: ../");
}
require 'config.php';
require 'conexion.php';
include 'validacion.php';
include 'backup.php';
require __DIR__ . '/vendor/autoload.php';
use phpseclib\Net\SFTP;




switch ($_POST['accion']) {
  case 'nueva_entrega':
    if (getVal_entrega($_POST['unidad'], $_POST['resp_ci'], $_POST['espacio'], $_POST['t_unidad'], $_POST['directorio'])) {

      $directorio = $_POST['unidad'] . "/" . $_POST['directorio'];

      setEntrega($_POST['resp_ci'], $_POST['espacio'], $_POST['t_unidad'], $_POST['unidad'], $directorio);
    } else {
      echo 0;
    }
    break;
  case 'nuevo_periodo':

    if (getVal_periodo($_POST['dis_entrega'], $_POST['res_entrega'], $_POST['res_recibe'], $_POST['fe_inicio'], $_POST['fe_fin'], $_POST['anexos'], $_POST['t_periodo'])) {

      setNuevoPeriodo($_POST['dis_entrega'], $_POST['res_entrega'], $_POST['res_recibe'], $_POST['fe_inicio'], $_POST['fe_fin'], $_POST['anexos'], $_POST['t_periodo']);
    } else {
      echo 0;
    }

    break;
  case 'asignar_entrega':

    if (getVal_asig_entrega($_POST['usuario'], $_POST['usr_entrega'])) {

      set_asignar_entrega($_POST['usuario'], $_POST['usr_entrega']);
    } else {
      echo 0;
    }

    break;
  case 'seleccion':
    if (getVal_sel_entrega($_POST['sel_entrega'])) {
      set_seleccion($_POST['sel_entrega']);
    } else {
      echo 0;
    }

    break;
  case 'crear_usr':
    if (getVal_usuario($_POST['nombre'], $_POST['usr'], $_POST['pass'], $_POST['tipo_usr'])) {
      setUsuario($_POST['nombre'], $_POST['usr'], $_POST['pass'], $_POST['tipo_usr']);
    } else {
      echo 0;
    }
    break;
  case 'usuario_id':

    if (getId_usr($_POST['id_usr'])) {
      get_datosU($_POST['id_usr']);
    } else {
      echo 0;
    }
    # code...
    break;
  case 'upd_usuario':
    if (getVal_usr_upd($_POST['id'], $_POST['nombreU'], $_POST['usrU'], $_POST['passU'], $_POST['tipo_usrU'])) {
      update_usr($_POST['id'], $_POST['nombreU'], $_POST['usrU'], $_POST['passU'], $_POST['tipo_usrU']);
    } else {
      echo 0;
    }
    break;
  case 'bitacora':
    if (getVal_bitacora($_POST['id'])) {
      $_SESSION['id_bitacora'] = $_POST['id'];
    }
    break;
  case 'des_anexos':
        if(get_val_anexos($_POST['fp']))
        {
          get_desc_anexos($_POST['fp']);

        }else{
          echo 0;
        }
  break;
  case 'update_anexos':
        if(get_ins_anexos($_POST['fp'],$_POST['anexos']))
        {
          set_anexos($_POST['fp'],$_POST['anexos']);
        }else{
          echo 0;
        }
  break;

  default:
    echo 0;
    # code...
    break;
}
function set_anexos($fp,$anexos){
  global $conexion;
  $in=$conexion->prepare("UPDATE PERIODO SET ANEXOS =:anex WHERE FOLIO_PERIODO =:fp");
  $in->bindValue(":anex",$anexos);
  $in->bindValue(":fp",$fp);
  $in->execute();
  if($in)
  {
    echo 1;

  }else{
    echo 0;
  }

}

function get_desc_anexos($fp){
  global $conexion;
  $des = $conexion->prepare("SELECT DATE_FORMAT(FINICIO, '%d-%m-%Y')AS FI ,DATE_FORMAT(FFIN, '%d-%m-%Y')AS FF,ANEXOS FROM PERIODO WHERE FOLIO_PERIODO=:fp");
  $des->bindValue(":fp", $fp);
  $des->execute();

  if($des)
  {
    $desc = $des->fetch(PDO::FETCH_ASSOC);
    echo json_encode($desc);
  }
  else{
    echo 0;
  }
  $des->closeCursor();
  $conexion = null;

}
/* funcion que creea la entreg con los parametros necesarios así como el directorio en el servidor */
function setEntrega($responsable, $espacio, $t_unidad, $unidad, $directorio)
{
  global $conexion;
  $res_entrega = $conexion->prepare("SELECT  E.ID_UNIDAD, E.ESPACIO, E.DIRECTORIO FROM ENTREGA E  WHERE  E.ID_UNIDAD = :unidad  and E.ESPACIO = :espacio");
  $res_entrega->bindValue(":unidad", $unidad);
  $res_entrega->bindValue(":espacio", $espacio);
  $res_entrega->execute();

  if ($res_entrega->rowCount() == 0)
  {
    $sftp = new SFTP('localhost');

    if($sftp->login(SFTP_USER,SFTP_PASSWORD))
    {
      $sftp->chdir('files');
      $part = explode('/', $directorio);
      $lon = count($part);
      
      $directory=$sftp->nlist();
      $tam = count($directory);

      for($x=0;$x<$tam;$x++)
      {
        if($directory[$x] == $part[0])
        {
            $on=true;
            break;
        }else{
            $on=false;
        }
      }

      if($on)
      {
        $sftp->chdir($part[0]);
        for ($i = 1; $i < $lon; $i++)
        {
          $sftp->mkdir($part[$i]);
          $sftp->chdir($part[$i]);
        }

      }else{
        $sftp->mkdir($part[0]);
        $sftp->chdir($part[0]);
        for ($i = 1; $i < $lon; $i++)
        {
          $sftp->mkdir($part[$i]);
          $sftp->chdir($part[$i]);
        }
        
      }
      $insertar = $conexion->prepare("INSERT INTO ENTREGA (RFC_CI,ID_UNIDAD,ESPACIO,DIRECTORIO,TIPO_UNIDAD) VALUES (:rfc, :id_unidad, :espacio, :dir, :t_unidad)");
      $insertar->execute(array(":rfc" => $responsable, ":id_unidad" => $unidad, ":espacio" => $espacio, ":dir" => $directorio, ":t_unidad" => $t_unidad));
      $insertar->closeCursor();
      echo 1;
      
    }
  }else{
    echo 2;
  }

  $conexion = null;
}

/*funcion encargada de crear un nuevo periodo*/
function setNuevoPeriodo($id_entrega, $rese, $resr, $fi, $ff, $anexo, $tp)
{

  global $conexion;
  $resp = $conexion->prepare("SELECT COUNT(FOLIO_PERIODO) AS CONTADOR FROM PERIODO WHERE ID_ENTREGA = :id");
  $resp->bindValue(":id", $id_entrega);
  $resp->execute();
  $p = $resp->fetch(PDO::FETCH_ASSOC);
  $cont = $p['CONTADOR'];
  $resp->closeCursor();

  //Si la consulta anterior regresa un valor de = 0 es porque no existe un periodo registrado par esta entrega, por lo
  // tanto no se realizara el proceso de backup de los registros
  // Si la consulta anterior regresa un valor > 0 significa que ya hay un periodo anterior a esta entrega, por lo tanto
  //se realizara el proceso de backup de los registros. 

  if ($cont == 0) 
  {
    $inp = $conexion->prepare("INSERT INTO PERIODO(id_entrega,resp_entrega,resp_recibe,finicio,ffin,anexos,tipo_periodo,fecha) VALUES(:id_entrega ,:rese,:resr,:fi,:ffin,:anexos,:tp,:fechaC)");
    $inp->execute(array(":id_entrega" => $id_entrega, ":rese" => $rese, ":resr" => $resr, ":fi" => $fi, ":ffin" => $ff, ":anexos" => $anexo, ":tp" => $tp, ":fechaC" => $_SESSION['fecha_captura']));
      if ($inp) 
      {
        echo 1;
      } else 
      {
        echo 0;
      }
    $inp->closeCursor();
  } else 
    {
    //crear el periodo nuevo
      $inp1 = $conexion->prepare("INSERT INTO PERIODO(id_entrega,resp_entrega,resp_recibe,finicio,ffin,anexos,tipo_periodo,fecha) VALUES(:id_entrega ,:rese,:resr,:fi,:ffin,:anexos,:tp,:fechaC)");
      $inp1->execute(array(":id_entrega" => $id_entrega, ":rese" => $rese, ":resr" => $resr, ":fi" => $fi, ":ffin" => $ff, ":anexos" => $anexo, ":tp" => $tp, ":fechaC" => $_SESSION['fecha_captura']));
      if ($inp1) 
      {
        //recuperar el periodo maximo de esta entrega y los anexos
        $resp = $conexion->prepare("SELECT FOLIO_PERIODO,ANEXOS FROM PERIODO WHERE FOLIO_PERIODO=(SELECT MAX(FOLIO_PERIODO) FROM PERIODO WHERE ID_ENTREGA=:id)");
        $resp->bindValue(":id", $id_entrega);
        $resp->execute();
        $p_actual = $resp->fetch(PDO::FETCH_ASSOC);
        $pmax = $p_actual['FOLIO_PERIODO']; //periodo maximo de esta entrega
        $anex = $p_actual['ANEXOS'];//anexos de este periodo
        $resp->closeCursor();

        //recuperar el periodo anterior de esta entrega 
        $fp_ant = $conexion->prepare("SELECT FOLIO_PERIODO FROM PERIODO WHERE FOLIO_PERIODO = (SELECT MAX(FOLIO_PERIODO)FROM PERIODO WHERE FOLIO_PERIODO < :fp AND ID_ENTREGA = :id)");
        $fp_ant->bindValue(":fp", $pmax);
        $fp_ant->bindValue(":id", $id_entrega);
        $fp_ant->execute();
        $p_ant = $fp_ant->fetch(PDO::FETCH_ASSOC);
        $past = $p_ant['FOLIO_PERIODO']; //se obtiene el periodo anterior de esta entrega

        $fp_ant->closeCursor();
        $var = mainBackup($pmax,$past,$anex);
        echo $var;
      } else 
        {
        echo 0;
        }
  }
    $conexion = null;
}


function mainBackup($pmax, $pant,$anexos)
{
  $back_up = explode(",",$anexos);
  $cont = count($back_up);

  $objeto= new back($pant,$pmax);
  $x=0;
  for($x;$x<$cont;$x++){

    switch($back_up[$x]){
        case 1:
            $objeto->setbackupa1();
        break;
        case 2:
            $objeto->setbackupa2();
        break;
        case 3:
            $objeto->setbackupa3();
        break;
        case 4:
            $objeto->setbackupa4();
        break;
        case 5:
            $objeto->setbackupa5();
        break;
        case 6:
            $objeto->setbackupa6();
        break;
        case 7:
            $objeto->setbackupa7();
        break;
        case 8:
            $objeto->setbackupa8();
        break;
        case 9:
            $objeto->setbackupa9();
        break;
        case 10:
            $objeto->setbackupa10();
        break;
        case 11:
            $objeto->setbackupa11();
        break;
        case 12:
            $objeto->setbackupa12();
        break;
        case 13:
            $objeto->setbackupa13();
        break;
    }
  }
  
  $objeto->setclose();
  return 1;
}
/* funcion ecargada de asignar las entregas a los usuarios */
function set_asignar_entrega($usuario, $sel_entrega)
{
  global $conexion;
  $ins_sel_entrega = $conexion->prepare("INSERT INTO ENTREGA_USUARIOS(ID_USUARIO,ID_ENTREGA) VALUES(:usuario , :entrega)");
  $ins_sel_entrega->execute(array(":usuario" => $usuario, ":entrega" => $sel_entrega));
  if ($ins_sel_entrega->rowCount() > 0) {
    echo 1;
  } else {
    echo 0;
  }
  $ins_sel_entrega->closeCursor();
  $conexion = null;
}

/*funcion que crena nuevos usuarios -------------comprobar mensaje de existencia */
function setUsuario($nombre, $usuario, $pass, $tipo_usr)
{
  global $conexion;
  $res_usr = $conexion->prepare("SELECT USUARIO FROM USUARIOS WHERE USUARIO=:USR");
  $res_usr->bindValue(":USR", $usuario);
  $res_usr->execute();
  if ($res_usr->rowCount() == 0) {
    $encry = password_hash($pass, PASSWORD_DEFAULT);
    $res_in = $conexion->prepare("INSERT INTO USUARIOS (USUARIO,PASS,NOMBRE,TIPO_USUARIO)  VALUES (:USR, :PASS,:NOMBRE,:T_USR)");
    $res_in->execute(array(":USR" => $usuario, ":PASS" => $encry, ":NOMBRE" => $nombre, ":T_USR" => $tipo_usr));
    $res_in->closeCursor();
    echo 1;
  } else {
    echo  3;
  }
  $res_usr->closeCursor();
  $conexion = null;
}
/* funcion para obtener los datos de los usuarios para actualizarse*/
function get_datosU($id)
{
  global $conexion;
  $usr = $conexion->prepare("SELECT ID_USUARIO AS ID,USUARIO AS USR ,NOMBRE AS NAME,TIPO_USUARIO AS T_S FROM USUARIOS WHERE ID_USUARIO=:id");
  $usr->bindValue(":id", $id);
  $usr->execute();
  if ($usr) {
    $datos = $usr->fetch(PDO::FETCH_ASSOC);
    echo json_encode($datos);
  } else {
    echo 0;
  }
  $usr->closeCursor();
  $conexion = null;
}
/*funcion para actualizar los usuarios*/
function update_usr($id, $nombre, $usuario, $pass, $tipo_usr)
{
  global $conexion;
  $usr_upd = $conexion->prepare("UPDATE USUARIOS SET USUARIO = :usr, PASS = :pas, NOMBRE = :nom, TIPO_USUARIO =:ts  WHERE ID_USUARIO = :id");
  $pas_hash = password_hash($pass, PASSWORD_DEFAULT);
  $usr_upd->execute(array(":usr" => $usuario, ":pas" => $pas_hash, ":nom" => $nombre, ":ts" => $tipo_usr, ":id" => $id));
  if ($usr_upd) {
    echo 1;
  } else {
    echo  0;
  }
  $usr_upd->closeCursor();
  $conexion = null;
}

/*funcion para establecer la entrega seleccionada*/
function set_seleccion($seleccion)
{
  global $conexion;

  /*variable se session*/
  $_SESSION['id_entrega'] = $seleccion;
  $res = $conexion->prepare("SELECT C.nombre, E.espacio,E.directorio FROM CAT_UNIDADES C join  ENTREGA E on C.id_unidad = E.id_unidad
                    WHERE E.id_entrega = :id_entrega");

  $res->bindValue(":id_entrega", $_SESSION['id_entrega']);

  $res->execute();

  $usr = $res->fetch(PDO::FETCH_ASSOC);

  //------------------------------------------------------------- variables de session-----------------------------------------------------------
  if ($res) {
    $_SESSION['sel'] = $usr['nombre'] . "- " . strtoupper($usr['espacio']);
    $_SESSION['directorio'] = $usr['directorio'];
    echo 1;
  } else {
    echo 0;
  }
  $res->closeCursor();
  $conexion = null;
}
