<?php 
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require 'conexion.php';
?>
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
        		<th>Descripción</th>
        		<th>Ubicación</th>
        		<th>Monto autorizado</th>
        		<th>Monto ejercido</th>
        		<th>Monto por ejercer</th>
        		<th>Avance físico</th>
        		<th>Avance financiero</th>
        		<th>Observaciones</th>
        		<th>Fecha de captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
		$sql_a13 = "SELECT FOLIOA13, DESC_OBRA,UBICACION,MONTO_AUTORIZA,MONTO_EJERCIDO,MONTO_EJERCER,AVANCE_FISICO,AVANCE_FINANCIERO,OBSERVACIONES,F_CAPTURA FROM ANEXO13  WHERE ID_ENTREGA = :id";
      		$res=$conexion->prepare($sql_a13);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA13'].",".$fila['DESC_OBRA'].",".$fila['UBICACION'].",".$fila['MONTO_AUTORIZA'].",".$fila['MONTO_EJERCIDO'].",".$fila['MONTO_EJERCER'].",".$fila['AVANCE_FISICO'].",".$fila['AVANCE_FINANCIERO'].",".$fila['OBSERVACIONES'].",".$fila['F_CAPTURA'];
				
				$borrar = $fila['FOLIOA13'].",". 13;
        

				?>
			<tr>
				<td><?php echo $fila['DESC_OBRA']; ?></td>
				<td><?php echo $fila['UBICACION']; ?></td>
				<td><?php echo $fila['MONTO_AUTORIZA']; ?></td>
				<td><?php echo $fila['MONTO_EJERCIDO']; ?></td>
				<td><?php echo $fila['MONTO_EJERCER']; ?></td>
				<td><?php echo $fila['AVANCE_FISICO']; ?></td>
				<td><?php echo $fila['AVANCE_FINANCIERO']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A13" onclick="datos_a13('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion=null; ?> 	
		</tbody>
	</table>
</div>

<form id="a13">
<div class="modal fade" id="Edicion_A13" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Descripción</label>
	 	<textarea class="form-control" name="des_a13u" id="des_a13u" placeholder="Descripción de la obra"></textarea>
		<br>
		<label>Ubicación</label>
		<textarea class="form-control" name="ubi_a13u" id="ubi_a13u" placeholder="Ubicación de la obra"></textarea>
		<br>
		<label>Monto autorizado</label>
		<input type="text" name="mau_a13u" id="mau_a13u" class="form-control" placeholder="Cantidad autorizada">
		<br>
		<label>Monto ejercido</label>
		<input type="text"  name="mej_a13u" id="mej_a13u" class="form-control" placeholder=" Cantidad ejercida">
		<br>
		<label>Monto por ejercer</label>
		<input type="text"  name="mpe_a13u" id="mpe_a13u" class="form-control" placeholder="Cantidad por ejercer">
		<br>
		<label>Avance físico (%)</label>
		<input type="text"  name="avaf_a13u" id="avaf_a13u" class="form-control" placeholder="Cantidad %">	
		<br>
		<label>Avance financiero</label>
		<input type="text"  name="avafin_a13u" id="avafin_a13u" class="form-control" placeholder="Avance">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a13u" id="obs_a13u" placeholder="Observaciones de la obra" ></textarea>		
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>

<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a13').validate({
        rules: {
            des_a13u: {required: true,texto:true },
            ubi_a13u: {required: true,texto:true},
            mau_a13u: {required: true, number:true},
            mej_a13u: {required: true,number:true},
            mpe_a13u: {required: true,number:true},
            avaf_a13u: {required: true, number:true},
            avafin_a13u: {required: true, number:true},
            obs_a13u:{required: true, texto:true}
        },
        messages: {
            des_a13u: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor", 
            },
            ubi_a13u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor", 
            
            },
            mau_a13u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor", 
            },
            mej_a13u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            mpe_a13u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
             },
            avaf_a13u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            avafin_a13u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            obs_a13u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            
        },
        submitHandler: function () {
           actualiza_a13();
        }
    });
  });

  $('#Edicion_A13').on('hidden.bs.modal', function (e) {
      $("label.error").remove();

  });
</script>

