<?php 

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("Location: https://localhost/SerUaz/");
  }

 function getVal_fecha($fecha){
  $date = explode('-', $fecha);
  if(!checkdate($date[1], $date[2], $date[0]))return false;
  return true;
 }


 function getVal_a1($obj,$f_c,$leg){

  if(empty($obj) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obj)) return false ;
  if(empty($leg) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$leg)) return false ;
  if(empty($f_c) or !getVal_fecha($f_c))return false;
  
  return true;
}


function getVal_a2($asu,$uni,$ava,$obs){
  if(empty($asu) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$asu)) return false ;
  if(empty($uni) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$uni)) return false ;
  if(empty($ava) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/",$ava)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;

  return true;
}

function getVal_a3($tnom,$fc,$obs){
  if(empty($tnom) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tnom)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  if(empty($fc) or !getVal_fecha($fc))return false;
  return true;
}
function getVal_a4($inv,$obs,$fc){
  if(empty($inv) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$inv)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  if(empty($fc) or !getVal_fecha($fc))return false;
  return true;
}
function getVal_a5($can,$des,$ubi,$obs){
  if(empty($can) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$can)) return false ;
  if(empty($des) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$des)) return false ;
  if(empty($ubi) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$ubi)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  return true;
}
function getVal_a6($are,$tdo,$tar,$doc){
  if(empty($are) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$are)) return false ;
  if(empty($tdo) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tdo)) return false ;
  if(empty($tar) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tar)) return false ;
  if(empty($doc) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$doc)) return false ;
  return true;
}
function getVal_a7($tin,$rin,$fdo,$obs){
  if(empty($tin) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tin)) return false ;
  if(empty($rin) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$rin)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  if(empty($fdo) or !getVal_fecha($fdo))return false;
  return true;
}

function getVal_a8($tcu,$ncu,$ban,$sal,$cba,$obs){
  if(empty($tcu) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tcu)) return false ;
  if(empty($ncu) or !is_numeric($ncu)) return false ;
  if(empty($ban) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$ban)) return false ;
  if(empty($sal) or !is_numeric($sal)) return false ;
  if(empty($cba) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$cba)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  return true;
}
function getVal_a9($tdo,$ncu,$ins,$fof,$foi,$obs){
  if(empty($tdo) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tdo)) return false ;
  if(empty($ncu) or !is_numeric($ncu)) return false ;
  if(empty($ins) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$ins)) return false ;
  if(empty($fof) or !is_numeric($fof))return false;
  if(empty($foi) or !is_numeric($foi)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  return true;
}
function getVal_a10($fei,$fau,$fca,$doc,$gap){
  if(empty($fei) or !getVal_fecha($fei))return false;
  if(empty($fau) or !is_numeric($fau)) return false ;
  if(empty($fca) or !is_numeric($fca)) return false ;
  if(empty($gap) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$gap)) return false ;
  if(empty($doc) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$doc)) return false ;
  return true;
}


 function getVal_a11($ndo,$des,$imp,$fei,$fef,$obs){
    if(empty($ndo) or !is_numeric($ndo)) return false;
    if(empty($des) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$des)) return false ;
    if(empty($imp) or !is_numeric($imp)) return false;
    if(empty($fei) or !getVal_fecha($fei))return false;
    if(empty($fef) or !getVal_fecha($fef))return false;
    if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
    return true;
}

function getVal_a12($fea,$fef,$npr,$uni,$tco,$orc,$ega,$obs){
  if(empty($fea) or !getVal_fecha($fea))return false;
  if(empty($fef) or !getVal_fecha($fef))return false;
  if(empty($npr) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$npr)) return false ;
  if(empty($uni) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$uni)) return false ;
  if(empty($tco) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$tco)) return false ;
  if(empty($orc) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$orc)) return false ;
  if(empty($ega) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$ega)) return false ;
  if(empty($obs) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  return true;
}
function getVal_a13($des,$ubi,$mon,$moej,$moer,$afi,$afin,$obs){
  if(empty($des) or  !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$des)) return false ;
  if(empty($ubi) or  !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$ubi)) return false ;
  if(empty($mon) or  !is_numeric($mon))return false;
  if(empty($moej) or !is_numeric($moej))return false;
  if(empty($moer) or !is_numeric($moer))return false;
  if(empty($afi) or  !is_numeric($afi))return false;
  if(empty($afin) or !is_numeric($afin))return false;
  if(empty($obs) or  !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$obs)) return false ;
  return true;
  
}

function getVal_folio($folio){
    if(!is_numeric($folio) or ($folio <= 0) or (empty($folio)) ) return false;
    return true;

  }

/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
   /*                                            Funciones de validacion de datos de los formularios                                                  */
   /*-------------------------------------------------------------------------------------------------------------------------------------------------*/

   /*funcion para la validacion de los campos del formulario de editar usuarios*/
   function getVal_usr_upd($id,$nom,$usr,$pas,$t_usr){
    if(empty($id) or !is_numeric($id) or ($id <=0 ) )return false;
    if(empty($nom) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$nom) or strlen($nom) > 50 ) return false;
    if(empty($usr) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$usr) or strlen($usr) > 50 )return false ;
    if(empty($pas) or !preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/",$pas) or strlen($pas) > 25 )return false ;
    if(empty($t_usr) or !is_numeric($t_usr))return false;
    
    return true;
   }

   function getId_usr($id_u){
    if(empty($id_u) or !is_numeric($id_u) or ($id_u <=0 ) )return false;
    return true;
   }

   /*funcion para la validacion de los campos del formulario de crear usuarios*/
   function getVal_usuario($nom,$usr,$pas,$t_usr){
    if(empty($nom) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$nom) or strlen($nom) > 50 )return false ;
    if(empty($usr) or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$usr) or strlen($usr) > 50 )return false ;
    if(empty($pas) or !preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/",$pas) or strlen($pas) > 25 )return false ;
    if(empty($t_usr) or !is_numeric($t_usr))return false;

    return true;
   }

   /*funcion para la validacion de los campos del forulario de seleccionar entrega*/
   function getVal_sel_entrega($sel){
    if(!is_numeric($sel) or $sel == "" or $sel <=0 )return false;
    return true;

   }
   /*funcion para la validacion de los campos del forulario de asignar entrega*/
   function getVal_asig_entrega($usr,$usr_ent){
    if($usr == "" or $usr_ent == "") return false;
    if(!is_numeric($usr) or !is_numeric($usr_ent)) return false;

    return true;
   }

   /*funcion para la validacion de los campos del forulario de periodo*/
   function getVal_periodo($id_e,$res_e,$res_r,$fei,$fin,$ane,$t_p){

    if(!is_numeric($id_e))return false;
    if(!preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$res_e) or strlen($res_e) > 255)return false;
    if(!preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$res_r) or strlen($res_r) > 255)return false;
    if($fei == "" or $fin == "" )return false;
    if($ane == "" or strlen($ane)>255)return false;
    if($t_p == "" or !is_numeric($t_p))return false;

    return true;
   }

/*funcion para la validacion de los campos del forulario de nueva entrega*/
   function getVal_entrega($id,$rfc,$es,$t_unid,$dire){

    if(empty($id) or !is_numeric($id))return false;
    if(empty($rfc) or !is_numeric($rfc))return false;
    if(!preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$es) or strlen($es)>255)return false; 
    if(($t_unid == "") or !preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$t_unid) or strlen($t_unid)>50 )return false;
    if(!preg_match("/^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/",$dire) or strlen($dire)>255)return false;
    
    return true;

   }

   function getVal_bitacora($id){
    if(empty($id) or !is_numeric($id))return false;
    return true;

   } 
   function get_val_anexos($fp)
   {
    if(empty($fp) or !is_numeric($fp))return false;
    return true;
     
   }

   function get_ins_anexos($fp,$anexos)
   {
    if(empty($fp) or !is_numeric($fp) or empty($anexos)) return false;
      return true;
   }


   function resetString($string)
{
    for($i=0;$i<strlen($string);$i++){
        if($string[$i]=="\n"){
            $string[$i] = str_replace(PHP_EOL, '2', $string[$i]);
        }
    }
    return $string;
}

 

?>