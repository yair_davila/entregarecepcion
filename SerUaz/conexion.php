<?php
//Conexion a la base de datos
if(!isset($_SESSION['ver'])){
	session_destroy();
	header("Location: https://localhost/SerUaz/");
}


	try{

		$conexion=new PDO('mysql:host=localhost; dbname=seruaz', 'phpmyadmin', 'phpmyadmin.');
		
		$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conexion->exec("SET CHARACTER SET UTF8");

	}catch(Exception $e){
		$conexion=null;
		
		echo "Error en la linea " . $e->getLine() . "<br>";
		
		echo "Error:" . $e->getMessage();


}
?>