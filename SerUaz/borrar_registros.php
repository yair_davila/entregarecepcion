<?php
/*--------------------------------------------------------------------------------------------------------------------------------*/
/*                                             Eliminacion de registros y archivos del servidor                                   */
/*--------------------------------------------------------------------------------------------------------------------------------*/
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require 'conexion.php';

    if(getVal_borrar($_POST['folio'],$_POST['anexo'])){

        $sql_del = "DELETE FROM ANEXO". $_POST['anexo'] . " WHERE FOLIOA" . $_POST['anexo'] ." = :fa AND FOLIO_PERIODO =:fp ";
        $res=$conexion->prepare ($sql_del);
        $res->bindValue(":fa",$_POST['folio']);
        $res->bindValue(":fp",$_SESSION['periodo']);
        $res->execute();
        $res->closeCursor();
        if($res){
          Bitacora("Anexo ".$_POST['anexo']);
          echo 1;   
        }else {
          echo 0;
      }
    $res->closeCursor();
    $conexion = null;
    }

  function getVal_borrar($folio,$anexo){
    if(empty($folio) or !is_numeric($folio) or ($folio <= 0) )return false;
    if(empty($anexo) or !is_numeric($anexo) or ($anexo <= 0 and $anexo > 13))return false;
    return true;
  }

  function Bitacora($anexo){
  global $conexion;
  $evento="Borrar";

  $sql_bit="INSERT INTO BITACORA(ANEMOX_M,ANEXO,EVENTO,USUARIO,F_EVENTO) VALUES(:anemox,:anexo,:evento,:usuario,:fecha)";
  $bit=$conexion->prepare($sql_bit);
  $bit->execute(array(":anemox"=>$_SESSION['id_entrega'], ":anexo"=>$anexo, ":evento"=>$evento, ":usuario"=>$_SESSION['nombre'],":fecha"=>$_SESSION['fecha_captura']));
 
}

?>