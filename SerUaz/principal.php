<?php
session_start();

if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
	header("Location: https://localhost/SerUaz/");
}
require './funciones/view_usuario.php';
?>
<!DOCTYPE html>
<html>

<head>
	<title>SERUAZ 2.1</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="js/jquery.min3.3.1.js"></script>
	<script src="js/jquery.validate.min.15.0.js"></script>
	<link rel="stylesheet" href="bootstrap-3.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap-3.4.1/css/bootstrap-theme.min.css">
	<script src="bootstrap-3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="estilos/style-menu.css">
	<script src="js/funcion-menu.js"></script>
	<script src="js/insert.js"></script>
	<script src="js/val_menu.js"></script>
	<script src="js/val_update_delete.js"></script>
	<script src="CodeSeven-toastr/build/toastr.min.js"></script>
	<link rel="stylesheet" type="text/css" href="CodeSeven-toastr/build/toastr.min.css">
	<link rel="stylesheet" type="text/css" href="estilos/Seruaz.css">
	<style type="text/css">
		label.error {
			color: #FF0000;
		}
	</style>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<header class="container-fluid">
				<div class="col-md-4">
					<div>
						<img src="imagenes/LogoCI.png" class="img-responsive logo-con" width="90px">
					</div>
				</div>
				<div class="col-md-4">
					<div>
						<img src="imagenes/Seruaz1.png" class="img-responsive logo-seruaz" width="320px">
					</div>
				</div>
				<div class="col-md-4">
					<h5>Usuario: <?php echo $_SESSION['usuario']; ?></h5>
					<?php
					if ($_SESSION['id_entrega'] == "") {
						echo "<h5>No has seleccionada una entrega</h5>";
					} else {
						echo "<h5>Entrega: " . mb_strtoupper($_SESSION['sel'], 'utf-8') . "</h5>";
						getActivaAnexos();
					}
					?>
				</div>
			</header>
		</div>
		<div class="row" style="margin-top: 5px">
			<div class="container">
				<div id='cssmenu'>
					<ul>
						<li><a href='principal.php'>SERUAZ</a></li>
						<li><a href='#'>Módulo 1 </a>
							<ul>
								<?php
								$cont = 0;
								$x = 0;
								if (isset($_SESSION['anexos'])) {
									$eje = $_SESSION['anexos'];
									$aray = explode(",", $eje);
									$cont = count($aray);
									$x = 0;
								}
								?>

								<?php for ($x; $x < $cont; $x++) :
									if ($aray[$x] == 1) : ?>
										<li><a href='#'>1. Marco Legal y de Atuación</a>
											<ul>
												<li><a href="#" onclick="cargar('Anexos/Anexo1.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara1.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=1' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
									<?php endif; ?>

									<?php if ($aray[$x] == 2) : ?>
										<li><a href='#'>2. Asunto en Trámite</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo2.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara2.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=2' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
								<?php endif;
								endfor;
								$x = 0; ?>
							</ul>
						</li>
						<li><a href='#'>Módulo 2</a>
							<ul>
								<?php for ($x; $x < $cont; $x++) :
									if ($aray[$x] == 3) : ?>
										<li><a href='#'>3. Recusrsos Humanos</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo3.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara3.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=3' target="_blank">Generar</a></li>
											</ul>
										</li>
									<?php endif; ?>

									<?php if ($aray[$x] == 4) : ?>
										<li><a href='#'>4. Recursos Materiales</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo4.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara4.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=4' target="_blank">Generar</a></li>
											</ul>
										</li>
									<?php endif; ?>

									<?php if ($aray[$x] == 5) : ?>
										<li><a href='#'>5. Libros, Boletines y Libretos de Consulta</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo5.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara5.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=5' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
									<?php endif; ?>
									<?php if ($aray[$x] == 6) : ?>
										<li><a href='#'>6. Principales Archivos de Documentos</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo6.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara6.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=6' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
								<?php endif;
								endfor;
								$x = 0; ?>
							</ul>
						</li>

						<li><a href='#'>Módulo 3</a>
							<ul>
								<?php for ($x; $x < $cont; $x++) :
									if ($aray[$x] == 7) : ?>
										<li><a href='#'>7. Recsursos Financieros - Informes</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo7.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara7.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=7' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
									<?php endif; ?>
									<?php if ($aray[$x] == 8) : ?>
										<li><a href='#'>8. Recursos Financieros - Bancos</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo8.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara8.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=8' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
									<?php endif; ?>
									<?php if ($aray[$x] == 9) : ?>
										<li><a href='#'>9. Corte Documental</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo9.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara9.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=9' target="_blank">Generar</a></li>
											</ul>
										</li>
									<?php endif; ?>
									<?php if ($aray[$x] == 10) : ?>
										<li><a href='#'>10. Fondo Revolvente</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo10.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara10.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=10' target="_blank">Generar</a></li>
											</ul>
										</li>
								<?php endif;
								endfor;
								$x = 0; ?>
							</ul>
						</li>

						<li><a href='#'>Módulo 4</a>
							<ul>
								<?php for ($x; $x < $cont; $x++) :
									if ($aray[$x] == 11) : ?>
										<li><a href='#'>11. Acuerdos, Convenios y Contratos de Servicios</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo11.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara11.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=11' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
									<?php endif; ?>
									<?php if ($aray[$x] == 12) : ?>
										<li><a href='#'>12. Actas de Concurso</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo12.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara12.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=12' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
									<?php endif; ?>
									<?php if ($aray[$x] == 13) : ?>
										<li><a href='#'>13. Obras Públicas</a>
											<ul>
												<li><a href='#' onclick="cargar('Anexos/Anexo13.php')">Insertar</a></li>
												<li><a href='#' onclick="edicion('Edicion_anexos/editara13.php')">Editar</a></li>
												<li><a href='reportes/reporteanexo.php?anexo=13' target="_blank">Generar reporte</a></li>
											</ul>
										</li>
								<?php endif;
								endfor;
								$x = 0; ?>
							</ul>
						</li>

						<li><a href='#'>Panel de Control</a>
							<ul>
								<?php if ($_SESSION['tipo_usr'] == 2) : //Solo usuarios de tipo 2
								?>
									<li><a href='#'>Usuarios</a>
										<ul>
											<li><a href='#' onclick="cargar('menu/usuarios.php')">Crear</a></li>
											<li><a href='#' onclick="cargar('menu/editarUsuarios.php')">Editar</a></li>
										</ul>
									</li>
								<?php endif; ?>
								<li><a href='#'>Entrega</a>
									<ul>
										<?php if ($_SESSION['tipo_usr'] == 2) : ?>
											<li><a href="#" onclick="cargar('menu/entrega.php')">Nueva entrega</a></li>
											<li><a href='#' onclick="cargar('menu/periodo.php')">Crear periodo</a></li>
											<li><a href='#' onclick="cargar('menu/anexos.php')">Editar anexos</a></li>
											<li><a href='#' onclick="cargar('menu/AsignarEntrega.php')">Asignar entrega</a></li>
										<?php endif; ?>
										<li><a href='#' onclick="cargar('menu/SeleccionarEntrega.php')">Seleccionar entrega</a></li>
									</ul>
								</li>
								<?php if ($_SESSION['tipo_usr'] == 2) : ?>
									<li><a href='#'>Reportes</a>
										<ul>
											<li><a href='#' onclick="cargar('reportes/entregar.php')">Entrega</a></li>
											<li><a href='#' onclick="cargar('reportes/periodos.php')">Periodos</a></li>
											<li><a href='#' onclick="edicion('reportes/reporte_bitacora.php')">Bitacoras de trabajo</a></li>
											<li><a href='#' onclick="edicion('reportes/reporte_usuarios.php')">Usuarios</a></li>
										</ul>
									</li>
								<?php endif; ?>
								<li><a href="">Acerca de</a></li>
								<li><a href="cerrar.php">Cerrar Sesión</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row" id="panel1">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div id="panel" class="areat"></div>
			</div>
		</div>
		<div class="row" id="tabla1">
			<div id="tabla" class="tabla" style="display: none;"></div>
		</div>
	</div>
	<footer class="container-fluid">
		<div class="col-md-4">
			<div align="center">
				<img src="imagenes/logoUAZ.png" class="img-responsive ulogo" width="120px">
			</div>
		</div>
		<div class="col-md-4 inf">
			<br>
			Universidad Autónoma de Zacatecas<br>
			Campus UAZ Siglo XXI<br>
			Torre de Rectoría 1er Piso<br>
			Carr. Zacatecas-Guadalajara Km.6<br>
			La Escondida, Zacatecas, Zac.<br>
			CP 98160<br>
			Correo Electrónico:<br>
			coninterna@uaz.edu.mx<br>
		</div>
		<div class="col-md-4">
			<div align="center" class="log_der">
				<img src="imagenes/logoUAZ.png" class="img-responsive ulogo" width="120px">
			</div>
		</div>
	</footer>
</body>
<script type="text/javascript">
	var insert;
	var update;

	function cargar(pagina) {
		if (update) {
			$('#tabla').hide();
			$('#panel').show();
			$('#panel').load(pagina);
			insert = true;
		} else {
			$('#panel').load(pagina);
			insert = true;
			update = false;
		}
	}

	function edicion(pagina) {
		if (insert) {
			$('#panel').hide();
			$('#tabla').show();
			$('#tabla').load(pagina);
			update = true;
			insert = false;
		} else {
			$('#panel').hide();
			$('#tabla').show();
			$('#tabla').load(pagina);
			update = true;
		}
	}
</script>

</html>