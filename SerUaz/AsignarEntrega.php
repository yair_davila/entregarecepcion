<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }

  include 'conexion.php'; 
    

?>
<div class="container-fluid">
	<div class="form-group">
		<h5>Asignar un usuario a un evento de Entrega - Recepción</h5>
			<input type="hidden" id="accion" value="asignar_entrega">
			<label>Usuario</label>
			<?php

				$sql_usuario = "SELECT * FROM USUARIOS ORDER BY NOMBRE ASC";

				$resultado = $conexion -> prepare($sql_usuario);
				
				$resultado -> execute();

			?>

			<select class="form-control" id="usuario">
				<?php while($fila = $resultado -> fetch(PDO::FETCH_ASSOC)):?>

				<option value="<?php echo $fila['ID_USUARIO'] ?>"><?php echo $fila['NOMBRE']?></option>
				
				<?php endwhile; $resultado -> closeCursor();?>
			</select>
				
			<label>Entregas</label>
			<?php 


    		$sql_entrega = "SELECT E.ID_ENTREGA,E.ID_UNIDAD,E.ESPACIO,C_U.NOMBRE FROM ENTREGA E JOIN CAT_UNIDADES C_U ON C_U.ID_UNIDAD= E.ID_UNIDAD AND E.ID_ENTREGA NOT IN (SELECT E_U.ID_ENTREGA FROM ENTREGA_USUARIOS E_U ORDER BY C_U.NOMBRE)";

			$res_entrega = $conexion -> prepare($sql_entrega);

			$res_entrega -> execute();

			?>

			<?php if ($res_entrega->rowCount() == 0): ?>

				<select class="form-control" disabled>
					
					<option value="0">No hay entregas disponibles</option>
				
				</select>
			<?php endif;?>


			<?php if($res_entrega->rowCount() != 0): ?>
			<select class="form-control" id="usr_entrega">

				<?php while($fila = $res_entrega -> fetch(PDO::FETCH_ASSOC)):?>

				<option value = "<?php echo $fila['ID_ENTREGA']?>"><?php echo $fila['NOMBRE'] . " - " . strtoupper($fila['ESPACIO'])?></option>
			
			<?php endwhile; $res_entrega ->closeCursor(); ?>

			</select>
			<br>
			<button type="button" class="btn btn-primary" onclick="asignar()" ><span class="glyphicon glyphicon-send"></span> Asignar</button>
			<?php endif; $res_entrega->closeCursor(); $conexion=null; ?>
	</div>
</div> 