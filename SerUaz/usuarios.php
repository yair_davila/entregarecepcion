<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");

  }    
  require 'conexion.php';
?>

<div class="container-fluid">
	<h5>Crear nuevo usuario</h5>
			<input type="hidden" id="accion" value="crear_usr" />
			<div class="form-group">
				<label>Nombre</label>
				<input type="text"  name="0" id="nombre" class="form-control" placeholder="Nombre completo"  onChange="getValidaT(this.value,name,id)">
				<p id="aviso0" style="display:none; color: red;">Rellena el campo por favor<br></p>
          		<p id="aviso_0" style="display:none; color: red;"><br></p>
				<label>Usario</label>
				<input type="text"  name="1" id="usr" class="form-control" placeholder="Usuario asignado"  onChange="getValidaT(this.value,name,id)"  >
				<p id="aviso1" style="display:none; color: red;">Rellena el campo por favor<br></p>
          		<p id="aviso_1" style="display:none; color: red;"><br></p>
				<label>Password</label>
				<input type="password"  name="2"  id="pass" class="form-control" placeholder="Password" onChange="getValidaP(this.value,name,id)">
				<p id="aviso2" style="display:none; color: red;">Rellena el campo por favor<br></p>
          		<p id="aviso_2" style="display:none; color: red;"><br></p>
				<label>Tipo de usuario</label>
				<select class="form-control" id="tipo_usr">
					<option value="" selected>Selecciona una opción...</option>
					<option value="1">Capturista</option>
					<option value="2">Responsable de unidad</option>
				</select>
				<p id="aviso3" style="display:none; color: red;">Debes seleccionar una opción<br></p>			
			</div>
			<button type="button" class="btn btn-primary" onclick="val_usuario()"><span class="glyphicon glyphicon-send"></span> Crear</button>
	</div>
