<?php 
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require 'conexion.php';
?>
<div class="container-fluid">
	<table class="table table-condensed">
		<thead>
			<tr>
        		<th>Fecha de informe</th>
        		<th>Fondo revolvente autoriado</th>
        		<th>Fondo disponoible en caja</th>
        		<th>Documentos pagados,<br>pendientes de tramite<br>y reposición</th>
        		<th>Gastos pendiente de<br>comprobar</th>
        		<th>Fecha captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a10 = " SELECT FOLIOA10,FECHA_INFORME,MONTO_AUTORIZADO,MONTO_DISPONIBLE,GASTOS_PENDIENTES,DOCUMENTOS_PAGADOS,F_CAPTURA FROM ANEXO10  WHERE ID_ENTREGA = :id";


      		$res=$conexion->prepare($sql_a10);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA10'].",".$fila['FECHA_INFORME'].",".$fila['MONTO_AUTORIZADO'].",".$fila['MONTO_DISPONIBLE'].",".$fila['GASTOS_PENDIENTES'].",".$fila['DOCUMENTOS_PAGADOS'].",".$fila['F_CAPTURA'];
				
				$borrar = $fila['FOLIOA10'].",". 10;
        

				?>
			<tr>
				<td><?php echo $fila['FECHA_INFORME']; ?></td>
				<td><?php echo $fila['MONTO_AUTORIZADO']; ?></td>
				<td><?php echo $fila['MONTO_DISPONIBLE']; ?></td>
				<td><?php echo $fila['GASTOS_PENDIENTES']; ?></td>
				<td><?php echo $fila['DOCUMENTOS_PAGADOS']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A10" id="actualizar" onclick="datos_a10('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion=null;?> 	
		</tbody>
	</table>
</div>

<form id="a10">
<div class="modal fade" id="Edicion_A10" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label>Fecha de Informe</label>
		<input type="date" class="form-control" name="fin_a10u" id="fin_a10u" >	
		<br>
		<label>Fondo Revolvente Autorizado</label>
		<input type="text"  name="fona_a10u" id="fona_a10u" class="form-control" placeholder="Monto total autorizado">	
		<br>
		<label>Disponible en Fondo de Caja (efectivo):</label>
		<input type="text"  name="fonc_a10u" id="fonc_a10u" class="form-control" placeholder="Monto disponible en efectivo a la fecha de corte">	
		<br>
		<label>Documentos Pagados, Pendientes de Tramite y Reposición</label>
		<input type="text"  name="doc_a10u" id="doc_a10u" class="form-control" placeholder="Total de la cantidad de los documentos">	
		<br>
        <label>Gastos Pendientes de Comprobar</label>
		<input type="text"  name="gas_a10u" id="gas_a10u" class="form-control" placeholder="Gastos pendientes por comprobar a la fecha de corte" >
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>



<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a10').validate({
        rules: {
            fin_a10u: {required: true},
            fona_a10u: {required: true, number:true},
            fonc_a10u: {required: true, number:true},
            doc_a10u: {required: true, texto:true},
            gas_a10u: {required: true, texto:true}
        },
        messages: {
            fin_a10u: {
                required: "Completa el campo por favor",
            },
            fona_a10u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",

            },
            fonc_a10u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            doc_a10u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico",
            },
            gas_a10u:{
              required: "Completa el campo por favor",
				texto:"No se aceptan caracteres especiales verificalo por favor"
			},
			sum_a10u:{
				required: "Completa el campo por favor",
				texto:"No se aceptan caracteres especiales verificalo por favor",
			}
            
            
        },
        submitHandler: function () {
            actualiza_a10();
        }
    });
  });

  $('#Edicion_A10').on('hidden.bs.modal', function (e) {
      $("label.error").remove();


  });
</script>

