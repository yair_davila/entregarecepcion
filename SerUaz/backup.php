<?php
if (!isset($_SESSION["usuario"])) { //si la varible de sesion del usuario no esta establecida se manda al index
  header("location:index.php");
}
require "conexion.php";

class back{
    
    public function __construct($id_anterior,$id_maximo)
    {
        $this->id_anterior=$id_anterior;
        $this->id_maximo=$id_maximo;
        $this->fecha=date('Y-m-d');
    }


    public function setbackupa1()
    {
        global $conexion;
        $query="INSERT INTO ANEXO1(ID_ENTREGA,FOLIO_PERIODO,OBJETIVO,CREACION,LEGAL,ENLACE,F_CAPTURA)
        SELECT ID_ENTREGA,$this->id_maximo,OBJETIVO,CREACION,LEGAL,ENLACE,'$this->fecha'
        FROM ANEXO1 WHERE FOLIO_PERIODO=:id_ant";
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
    
    
    }

    public function setbackupa2()
    {
        global $conexion;

        $query="INSERT INTO ANEXO2(ID_ENTREGA,FOLIO_PERIODO,ASUNTO,UNIDAD_TRAMITE,AVANCE,OBSERVACIONES,ENLACE,F_CAPTURA) 
        SELECT ID_ENTREGA,$this->id_maximo,ASUNTO,UNIDAD_TRAMITE,AVANCE,OBSERVACIONES,ENLACE,'$this->fecha' 
        FROM ANEXO2 WHERE FOLIO_PERIODO=:id_ant";
        
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();

    }

    public function setbackupa3()
    {
        global $conexion;
        $query="INSERT INTO ANEXO3(ID_ENTREGA,FOLIO_PERIODO,NOMINA,CORTE,OBSERVACIONES,ENLACE,F_CAPTURA) 
        SELECT ID_ENTREGA,$this->id_maximo,NOMINA,CORTE,OBSERVACIONES,ENLACE,'$this->fecha'
        FROM ANEXO3 WHERE FOLIO_PERIODO=:id_ant";

        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
    }

    public function setbackupa4()
    {
        global $conexion;
        $query="INSERT INTO ANEXO4(ID_ENTREGA,FOLIO_PERIODO,INVENTARIO,FECHA,OBSERVACIONES,ENLACE,F_CAPTURA)
        SELECT ID_ENTREGA,$this->id_maximo,INVENTARIO,FECHA,OBSERVACIONES,ENLACE,'$this->fecha'
        FROM ANEXO4 WHERE FOLIO_PERIODO=:id_ant";

        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
         
    }
    public function setbackupa5()
    {
        global $conexion;
        $query="INSERT INTO ANEXO5(ID_ENTREGA,FOLIO_PERIODO,CANTIDAD,DESCRIPCION,UBICACION,OBSERVACIONES,ENLACE,F_CAPTURA)
        SELECT ID_ENTREGA,$this->id_maximo,CANTIDAD,DESCRIPCION,UBICACION,OBSERVACIONES,ENLACE,'$this->fecha'
        FROM ANEXO5 WHERE FOLIO_PERIODO=:id_ant";

        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
         
    }
    public function setbackupa6(){
        global $conexion;
        $query="INSERT INTO ANEXO6 (ID_ENTREGA,FOLIO_PERIODO,AREA_RESGUARDA,TIPO_DOC,TIPO_ARCHIVO,CONTENIDO,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,AREA_RESGUARDA,TIPO_DOC,TIPO_ARCHIVO,CONTENIDO,ENLACE,'$this->fecha'
                FROM ANEXO6 WHERE FOLIO_PERIODO=:id_ant";

        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor(); 

    }
    public function setbackupa7()
    {
        global $conexion;
        $query="INSERT INTO ANEXO7 (ID_ENTREGA,FOLIO_PERIODO,TIPO_INFORME,REPORTE_FINANCIERO,FECHA_INFORME,OBSERVACIONES,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,TIPO_INFORME,REPORTE_FINANCIERO,FECHA_INFORME,OBSERVACIONES,ENLACE,'$this->fecha'
                FROM ANEXO7 WHERE FOLIO_PERIODO=:id_ant";
                
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor(); 
    }
    public function setbackupa8()
    {
        global $conexion;
        $query="INSERT INTO ANEXO8 (ID_ENTREGA,FOLIO_PERIODO,T_CUENTA,NO_CUENTA,INSTITUCION,SALDO,CONCILIACION,OBSERVACIONES,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,T_CUENTA,NO_CUENTA,INSTITUCION,SALDO,CONCILIACION,OBSERVACIONES,ENLACE,'$this->fecha'
                FROM ANEXO8 WHERE FOLIO_PERIODO=:id_ant";

        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor(); 
         
    }
    public function setbackupa9()
    {
        global $conexion;

        $query="INSERT INTO ANEXO9(ID_ENTREGA,FOLIO_PERIODO,TIPO_DOCUMENTO,NUMERO_CUENTA,INSTITUCION,N_FINAL,N_INICIAL,OBSERVACIONES,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,TIPO_DOCUMENTO,NUMERO_CUENTA,INSTITUCION,N_FINAL,N_INICIAL,OBSERVACIONES,ENLACE,'$this->fecha' 
                FROM ANEXO9 WHERE FOLIO_PERIODO=:id_ant";

        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
         
    }
    public function setbackupa10()
    {
        global $conexion;
        $query="INSERT INTO ANEXO10 (ID_ENTREGA,FOLIO_PERIODO,FECHA_INFORME,MONTO_AUTORIZADO,MONTO_DISPONIBLE,GASTOS_PENDIENTES,DOCUMENTOS_PAGADOS,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,FECHA_INFORME,MONTO_AUTORIZADO,MONTO_DISPONIBLE,GASTOS_PENDIENTES,DOCUMENTOS_PAGADOS,ENLACE,'$this->fecha' 
                FROM ANEXO10 WHERE FOLIO_PERIODO=:id_ant";
        
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
         
    }
    public function setbackupa11()
    {
        global $conexion;

        $query="INSERT INTO ANEXO11 (ID_ENTREGA,FOLIO_PERIODO,NO_DOC,DESC_DOC,IMPORTE,F_INICIO,F_TERMINO,OBSERVACIONES,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,NO_DOC,DESC_DOC,IMPORTE,F_INICIO,F_TERMINO,OBSERVACIONES,ENLACE,'$this->fecha'
                FROM ANEXO11 WHERE FOLIO_PERIODO=:id_ant";
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
         
    }
    public function setbackupa12()
    {
        global $conexion;
        $query="INSERT INTO ANEXO12 (ID_ENTREGA,FOLIO_PERIODO,F_APERTURA,F_FALLO,PROYECTO,UNIDAD,TIPO_CONCURSO,ORIGEN_RECURSO,EMPRESA_GANADORA,OBSERVACIONES,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,F_APERTURA,F_FALLO,PROYECTO,UNIDAD,TIPO_CONCURSO,ORIGEN_RECURSO,EMPRESA_GANADORA,OBSERVACIONES,ENLACE,'$this->fecha'
                FROM ANEXO12 WHERE FOLIO_PERIODO=:id_ant";
        
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
    }
    public function setbackupa13()
    {
        global $conexion;
        $query="INSERT INTO ANEXO13 (ID_ENTREGA,FOLIO_PERIODO,DESC_OBRA,UBICACION,MONTO_AUTORIZA,MONTO_EJERCIDO,MONTO_EJERCER,AVANCE_FISICO,AVANCE_FINANCIERO,OBSERVACIONES,ENLACE,F_CAPTURA)
                SELECT ID_ENTREGA,$this->id_maximo,DESC_OBRA,UBICACION,MONTO_AUTORIZA,MONTO_EJERCIDO,MONTO_EJERCER,AVANCE_FISICO,AVANCE_FINANCIERO,OBSERVACIONES,ENLACE,'$this->fecha'
                FROM ANEXO13 WHERE FOLIO_PERIODO=:id_ant";
        
        $resp1=$conexion->prepare($query);
        $resp1->bindValue(":id_ant",$this->id_anterior);
        $resp1->execute();
        $resp1->closeCursor();
    }

    public function setclose()
    {
        global $conexion;
        $conexion=null;
    }

    public function getValores(){
        return $this->fecha;
        //return "{$this->id_anterior} {$this->id_maximo}";
    

    }
    private $id_anterior;
    private $id_maximo;
    private $fecha;




}

?>