<?php

session_start();

include 'conexion.php';

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }       
     

?>
<div class="container-fluid"> 
      <h5>Crear nuevo registro de Entrega - Recepción</h5>
      <input type="hidden" id="accion" value="nueva_entrega"/>
        <div class="form-group" id="ver">
          <label>Unidad</label>
          <?php 
            $sql_unidad = "SELECT DISTINCT ID_UNIDAD, NOMBRE FROM CAT_UNIDADES ORDER BY  NOMBRE ASC"; //consulta a realizar 

            $resultado =  $conexion->prepare($sql_unidad);  //preparacion de la consulta

            $resultado->execute();
          ?>

          <select class="form-control" name="unidad" id="unidad">;
            <?php while($fila = $resultado -> fetch(PDO::FETCH_ASSOC)):?>

                <option value="<?php echo  $fila['ID_UNIDAD']?>"> <?php echo $fila["NOMBRE"] ?></option>
                
                <?php endwhile;  $resultado -> closeCursor();?>
              </select>
              <br>
              <button type="button" class="btn btn-primary" onclick="mostrar()"><span class="glyphicon glyphicon-ok"></span> Seleccionar</button>

        </div>

        <div  class="container-fluid" id="oculto" style="display:none;">
            <select class="form-control" disabled>
              <option value="0">Unidad seleccionada</option>
            </select>
            <label>Tipo unidad</label><br>
            <select name="0" id="t_unidad" class="form-control">
              <option value="" selected>Slecciona una opción...</option>
              <option value="0">Académica</option>
              <option value="1">Administrativa</option>
            </select>
            <p id="aviso0" style="display:none; color: red;">Debes seleccionar una opción<br></p>      
            <label>Espacio (en su caso)</label><br>
            <input type="text"  class="form-control" name="1" id="espacio" onChange="getValidaT(this.value,name,id)" placeholder="Departamento" >
            <p id="aviso1" style="display:none; color: red;">Rellena el campo por favor<br></p>
            <p id="aviso_1" style="display:none; color: red;"><br></p>
            <label>Responsable de contraloria</label>
              <?php 
                 $sql_resp = "SELECT * FROM RESP_CI ORDER BY NOMBRE ASC";
                                 
                 $resp_c = $conexion -> prepare($sql_resp);  //preparacion de la consulta

                 $resp_c -> execute();  //ejecución de la consulta
      
              ?>
            <select id="resp_ci" class="form-control">

              <?php  while($rci = $resp_c -> fetch(PDO::FETCH_ASSOC)):?>
            
            <option value="<?php echo $rci['RFC_CI'] ?>"><?php echo $rci['NOMBRE'] ?></option>
            
            <?php endwhile;  $resp_c ->closeCursor(); $conexion=null; ?>
            </select>
            <label>Directorio de la entrega</label><br>
            <input type="text"  class="form-control" name="directorio" id="directorio" >
            <p id="aviso2" style="display:none; color: red;">Debes especificar el subdirectorio<br></p>      
            <br>
            <button type="button" class="btn btn-primary" onclick="val_entrega(event)"><span class="glyphicon glyphicon-send"></span> Aceptar</button>
          </div>
</div>

<script>

  function mostrar(){

    $('#ver').hide(); //oculta el primer div

    $('#oculto').show(); //muestra el segundo div

    var dir = $('#unidad').val(); //alamcena el directorio

    $('#directorio').val(dir) ; //mustra el directorio

  }

  
</script>

