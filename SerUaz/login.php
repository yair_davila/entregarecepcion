<?php
	
	$capchat = $_POST['g-recaptcha-response'];//se captura el valor de recaptcha

	if($capchat!=""){//si el valor es diferente de vacio se procede
	
		$secret = "6LfH7swUAAAAALpnjuBVdIFT3fhGsLdd4_D-wj7B";//clave
	
		$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$capchat");//solicitud
	
		$arr=json_decode($response,true);
	
		if($arr['success']){
	
			cont_login();
	
		}else{
			echo 0;
			//header("Location:index.php");
		}
	}else{
		echo 0;
		//header("Location:index.php");
	}
	
	
//cont_login();



/**
 * Descripcion : esta funcion filtra los valores que llegan como variables POST del index en este caso el usuario y la contraseña, a las cuales se les aplica una serie de reglas que deben cumplir
 * @return bool
 */

function val_login(){

	$usr = htmlentities(addslashes($_POST['usuario']));  //se escapan los caracteres
			
	$pas = htmlentities(addslashes($_POST['password'])); //se escapan los caractere

	if(empty($usr) or !preg_match("/^([a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s\d])*$/",$usr) or strlen($usr) > 10 ) return false;
	if(empty($pas) or !preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/",$pas) or strlen($pas) > 20 )return false ;
   
	return true;
}

/**
 * Desceipcion: esta funcion se encarga de dirigir el flujo del programa, en donde se evalua el valor retornado por la funcion "val_login" si este valor es true se procese a la siguente funcion sí no lo es se redirige al index
 * 
 */

function cont_login(){

	if(!val_login()){
		//header("location:index.php");
		echo 0;

	}else
		logeo();//llamada
	}

/**
 * Descripción: esta funcion realiza el proceso de verificar el usuario y la contraseña, así como de verificar el acceso a el servidor ftp. Si ambas verificaciones son correctas se dara acceso al sistema con sus varibles de session correspondientes. Si por alguna razón alguna de las dos verificaciones falla se redirigira al index principal.
 */

function logeo(){

			session_start();

			$_SESSION['ver']="ok";

			$usr = $_POST['usuario']; 
			
			$pas = $_POST['password']; 


			require 'conexion.php';


			$sql = "SELECT * FROM USUARIOS WHERE USUARIO = :usr"; //consulta a realizar
			
			$resultado = $conexion->prepare($sql);		//se prepara la consulta
			
			$resultado->bindValue(":usr",$usr);		//se enlaza el valor de referencia
			
			$resultado->execute();				//se ejecuta la consulta sql
			
			$usua= $resultado ->fetch(PDO::FETCH_ASSOC);

			
			if(password_verify($pas, $usua['PASS'])){//si los registros son mayores a cero se encontro el usuario correspondiente y el password 
				
				$ftp_server = "localhost";
				$ftp_user = "seruaz";
				$ftp_password = "xampp";
				/*------------------------------------------------------------------------------------------------------*/	
				/*                                      Verificar conexion al servidor ftp                              */
				/*------------------------------------------------------------------------------------------------------*/

				$ftp_conexion = ftp_ssl_connect($ftp_server) or die("No se pudo conectar");
				
				if(ftp_login($ftp_conexion, $ftp_user, $ftp_password)){
					
						
					ftp_close($ftp_conexion);

					
				/*-------------------------------------------------------------------------------------------------------*/
				/*                                     Variables de sesion de usuarios                                   */
				/*-------------------------------------------------------------------------------------------------------*/

				
					
				$_SESSION['nombre'] = $usua['NOMBRE'];
				$_SESSION['id_usuario'] = $usua['ID_USUARIO'];
				$_SESSION['tipo_usr']  = $usua['TIPO_USUARIO'];
				$_SESSION["usuario"] = $usua['USUARIO'];
				$_SESSION['id_entrega']="";
				$_SESSION['sel']="";
				$_SESSION['fecha_captura'] = date("Y-m-d");


				/*------------------------------------------------------------------------------------------------------*/
				/*                                   Variables de session para el servidor ftp                          */
                /*------------------------------------------------------------------------------------------------------*/
				
				$_SESSION['ftp_server'] = $ftp_server;
				$_SESSION['ftp_user'] = $ftp_user;
				$_SESSION['ftp_password'] = $ftp_password;
				echo 1;
				//header("location:principal.php");	
				
				}else{
					
					ftp_close($ftp_conexion);
					session_destroy();
					echo 0;
					//header("location:index.php");
				}
				/*------------------------------------------------------------------------------------------------------*/
			}else{
				$resultado->closeCursor();
				$conexion = null;
				session_destroy();
				echo 0;
				//header("location:index.php"); //de otra manera si no hubo exito en el logeo se redirecciona al login
			}
	}
	
?>

