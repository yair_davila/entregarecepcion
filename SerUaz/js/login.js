function callback() { if (grecaptcha.getResponse().length != 0) { $("#send").prop('disabled', false); } } $(document).ready(function () {
  $.validator.addMethod('texto', function (value, element) { return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value); });
  $('#index_login').validate({
    rules: { usuario: { required: true, texto: true }, password: { required: true } }, messages: {
      usuario: {
        required: "Completa el campo por favor",
        texto: "No se aceptan caracteres especiales verificalo por favor",
      }, password: { required: "Completa el campo por favor", }
    },
    submitHandler: function () { let datos = $('#index_login').serialize(); $(".alert").remove(); inicio(datos); }
  });
}); function inicio(datos) {
  $.ajax({
    type: "POST", url: "./funciones/login.php",
    data: datos,beforeSend:function(){
      $('#oculto').show();$("#send").prop('disabled', true);}, success: function (answer) {
      console.log(answer); if (answer == 0) {
        $('#oculto').hide();
        console.log(answer); $('#message_con').append('<div class="alert alert-danger alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
          + '<strong>Error usuario o password incorectos</strong></div>'); grecaptcha.reset(); $("#send").prop('disabled', true); $("#usuario").focus();
      } else if (answer == 1) {
        $('#oculto').hide();
        $('#message_con').append('<div class="alert alert-success"><strong>!!!Login con exito!!!</strong></div>'); setTimeout(function () { window.location.href = 'https://localhost/SerUaz/principal.php'; }, 2100);
      }
    }, error: function () { console.log('Error'); }
  });
}