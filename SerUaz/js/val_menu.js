var nombre;
var usr;
var id_usr;

function desc_enexos() {
  let data = "fp=" + $('#per_entr').val() +
    "&accion=" + "des_anexos";
  get_desc_anexos(data);

}
function set_update_anexos() {
  anexos = get_anexos();
  envio_menu("fp=" + $('#per_entr').val() + "&anexos=" + anexos + "&accion=update_anexos", "anexos");
}

function create_entrega() {
  envio_menu($('#cr_entrega').serialize() + "&accion=nueva_entrega", "entrega");
}

function get_anexos() {
  var valor = $('[name="anexos[]"]:checked').map(function () {
    return this.value;
  }).get();
  return valor;
}

function create_periodo() {
  anexos = get_anexos();
  envio_menu($('#cr_periodo').serialize() + "&anexos=" + anexos + "&accion=nuevo_periodo", "periodo");

}

function asignar() {
  envio_menu($('#asig_entrega').serialize() + "&accion=asignar_entrega", "AsignarEntrega");
}

function sel() {
  let data = "sel_entrega=" + $('#sel_entrega').val() +
    "&accion=" + $('#accion').val();
  envio_menu1(data);

}

function create_usuario() {
  envio_menu($('#usr_create').serialize() + "&accion=crear_usr", "usuarios");
}

function set_id() {
  id_usr = $('#id_usr').val();
  let data = "id_usr=" + $('#id_usr').val() +
    "&accion=" + "usuario_id";
  envio_usr(data);
}

function update_usr() {
  envio_menu($('#upd_usr').serialize() + "&id=" + id_usr + "&accion=upd_usuario", "editarUsuarios");
}

function ok() {
  let data = "id=" + $('#entrega1').val() +
    "&accion=" + "bitacora";
  envio_bitacora(data);
}

function get_desc_anexos(valor) {
  $.ajax({
    type: "POST",
    url: "menu.php",
    data: valor,
    dataType: 'json',
    success: function (answ) {
      $('#anexos_act').val(answ['ANEXOS']);
      $('#anexos_per').val(answ['FI'] + " - " + answ['FF']);
    },
    error: function () {
      mensajes_e("Ocurrio algun problema intentalo nuevamente");
    }

  });
}

function envio_usr(datos) {
  $.ajax({
    type: "POST",
    url: "menu.php",
    data: datos,
    dataType: 'json',
    success: function (data) {
      if (data != "") {
        $('#ver').hide();
        $('#oculto').show();
        $('#nombreU').val(data['NAME']);
        nombre = data['NAME'];
        $('#accion').val(data['ID']);
        $('#usrU').val(data['USR']);
        usr = data['USR'];
        let t_u = data['T_S'];

        if (t_u == 1) {
          $('#tipo_usrU option[value=1]').attr('selected', 'selected');
        } else {
          $('#tipo_usrU option[value=2]').attr('selected', 'selected');
        }
      } else {
        mensajes_e("Ocurrio algun problema intentalo nuevamente");
      }
    }, error: function () {
      mensajes_e("Ocurrio algun problema intentalo nuevamente");

    }
  });
}

function envio_menu(datos, pagina) {

  $.ajax({
    type: "POST",
    url: "menu.php",
    data: datos,
    success: function (res) {
      if (res == 1) {
        console.log(res);
        switch (pagina) {
          case 'entrega':
            mensajes("Entrega creada correctamente");
            break;
          case 'AsignarEntrega':
            mensajes("Entrega asignada correctamente ");
            break;
          case 'periodo':
            mensajes("Periodo creado correctamente");
            break;
          case 'usuarios':
            mensajes("Usuario creado correctamente");
            break;
          case 'editarUsuarios':
            mensajes("Usuario actualizado correctamente");
            break;
          case 'anexos':
            mensajes("Anexos actualizados correctamente")
            break;
        }
        $('input[type="text"]').val('');
        $('textarea').val('');
        $('#panel').load('menu/' + pagina + '.php');
      } else if (res == 2) {
        console.log(res);
        mensajes_w('Entrega existente, verifivalo e intentalo nuevamente');
      } else if (res == 3) {
        mensajes_w('Este usuario ya existente en el sistema, verifivalo e intentalo nuevamente');

      }
      else {
        console.log(res);
        mensajes_e("Ocurrio algun problema intentalo nuevamente");
      }

    },
    error: function () {
      mensajes_e("Ocurrio algun problema intentalo nuevamente");
    }
  });
}

function envio_menu1(datos) {

  $.ajax({
    type: "POST",
    url: "menu.php",
    data: datos,
    success: function (res) {
      if (res == 1) {
        $('input[type="text"]').val('');
        $('textarea').val('');
        mensajes("Entrega seleccionada");
        setTimeout(function () { window.location.href = 'principal.php'; }, 2000);

      } else {
        console.log(res);
        mensajes_e("Ocurrio algun problema intentalo nuevamente");
      }

    }, error: function () {
      mensajes_e("Ocurrio algun problema intentalo nuevamente");
    }
  });
}

function envio_bitacora(datos) {

  $.ajax({
    type: "POST",
    url: "menu.php",
    data: datos,
    success: function (res) {
      edicion("reportes/reporte_bitacora.php");
    }, 
    error: function () {
      mensajes_e("Ocurrio algun problema intentalo nuevamente");
    }
  });
}


function mensajes(texto) {
  toastr.success(texto, 'Exito', {
    "positionClass": "toast-bottom-right"
  });

}
function mensajes_w(texto) {
  toastr.warning(texto, 'Precaución', {
    "positionClass": "toast-bottom-right"
  });
}

function mensajes_e(texto) {
  toastr.error(texto, 'Error', {
    "positionClass": "toast-bottom-right"
  });
}