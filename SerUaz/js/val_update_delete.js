
var numFolio;

function datos_a1(data){
  
  let datos = data.split('|');
  console.log(datos);
  numFolio = datos[0];
  
  $('#objA1').val(datos[1]);
  $('#feA1').val(datos[2]);
  $('#decA1').val(datos[3]);
  
}

function actualiza_a1() {
  update_envio($('#a1').serialize() + "&accion=upd_a1" + "&folio=" + numFolio, "a1");
}

function datos_a2(data){
  console.log(data);
  let datosa2 = data.split('|');
  numFolio = datosa2[0];
  $('#asunto_a2u').val(datosa2[1]);
  $('#unidad_a2u').val(datosa2[2]);
  $('#avance_a2u').val(datosa2[3]);
  $('#obs_a2u').val(datosa2[4]);
}
function actualiza_a2() {
  update_envio($('#a2').serialize() + "&accion=upd_a2" + "&folio_a2=" + numFolio, "a2");
}

function datos_a3(data) {
  datosa3 = data.split('|');

  numFolio = datosa3[0];
  $('#t_nominau').val(datosa3[1]);
  $('#f_corteu').val(datosa3[2]);
  $('#obs_a3u').val(datosa3[3]);

}

function actualiza_a3() {
  update_envio($('#a3').serialize() + "&accion=upd_a3" + "&folio_a3=" + numFolio, "a3");
}

function datos_a4(data) {
  datosa4 = data.split('|');
  numFolio = datosa4[0];
  let inv = datosa4[1];
  $('#inv_a4u').val(datosa4[1]);
  $('#f_cortea4u').val(datosa4[2]);
  $('#obs_a4u').val(datosa4[3]);

  switch (inv) {
    case 'Bienes, muebles, informáticos':
      $("#inv_a4u option[value=1]").prop("selected", true);
      break;
    case 'Vehiculos y transportes':
      $("#inv_a4u option[value=2]").prop("selected", true);
      break;
    case 'Bienes e inmuebles':
      $("#inv_a4u option[value=3]").prop("selected", true);
      break;
    case 'Existencia en almacen':
      $("#inv_a4u option[value=4]").prop("selected", true);
      break;
    case 'Relacion de bajas':
      $("#inv_a4u option[value=5]").prop("selected", true);
      break;
    default:
      break;
  }
}

function actualiza_a4() {
  update_envio($('#a4').serialize() + "&accion=upd_a4" + "&folio_a4=" + numFolio, "a4");
}

function datos_a5(datos) {
  datosa5 = datos.split("|");
  numFolio = datosa5[0];
  $('#can_a5u').val(datosa5[1]);
  $('#des_a5u').val(datosa5[2]);
  $('#ubi_a5u').val(datosa5[3]);
  $('#obs_a5u').val(datosa5[4]);
}

function actualiza_a5() {
  update_envio($('#a5').serialize() + "&accion=upd_a5" + "&folio_a5=" + numFolio, "a5");
}

function datos_a6(datos) {
  datosa6 = datos.split("|");
  numFolio = datosa6[0];
  $('#area_a6u').val(datosa6[1]);
  $('#doc_a6u').val(datosa6[4]);
  tdoc = datosa6[2];
  tarc = datosa6[3];

  switch (tdoc) {
    case 'Archivos de documentos':
      $("#tdoc_a6u option[value=1]").prop("selected", true);
      break;
    case 'Archivos electrónicos':
      $("#tdoc_a6u option[value=2]").prop("selected", true);
      break;
    default:
      break;
  }
  switch (tarc) {
    case 'Vigente':
      $("#tarc_a6u option[value=Vigente]").prop("selected", true);
      break;
    case 'Muerto':
      $("#tarc_a6u option[value=Muerto]").prop("selected", true);
      break;
    default:
      break;
  }
}

function actualiza_a6() {
  update_envio($('#a6').serialize() + "&accion=upd_a6" + "&folio_a6=" + numFolio, "a6");
}

function datos_a7(datos) {
  datosa7 = datos.split("|");
  numFolio = datosa7[0];
  tinf = datosa7[1];
  pinf = datosa7[2];
  $('#fdoc_a7u').val(datosa7[3]);
  $('#obs_a7u').val(datosa7[4]);

  switch (tinf) {
    case 'Presupuesto':
      $("#tipi_a7u option[value=1]").prop("selected", true);
      break;
    case 'Estado de situación financiera':
      $("#tipi_a7u option[value=2]").prop("selected", true);
      break;
    case 'Estado de actividades':
      $("#tipi_a7u option[value=3]").prop("selected", true);
      break;
    case 'Balanza de comprobación':
      $("#tipi_a7u option[value=4]").prop("selected", true);
      break;
    case 'Deudores diversos':
      $("#tipi_a7u option[value=5]").prop("selected", true);
      break;
    case 'Acredores diversos':
      $("#tipi_a7u option[value=6]").prop("selected", true);
      break;
    case 'Proveedores':
      $("#tipi_a7u option[value=7]").prop("selected", true);
      break;
    case 'Otros':
      $("#tipi_a7u option[value=8]").prop("selected", true);
      break;
    default:
      break;
  }
  switch (pinf) {
    case 'Si':
      $("#si_a7u").prop("checked", true);
      break;
    case 'No':
      $("#no_a7u").prop("checked", true);
      break;
    default:
      // statements_def
      break;
  }
}

function actualiza_a7() {
  update_envio($('#a7').serialize() + "&accion=upd_a7" + "&folio_a7=" + numFolio, "a7");
}

function datos_a8(datos) {
  datosa8 = datos.split("|");
  numFolio = datosa8[0];
  tcue = datosa8[1];
  cons = datosa8[5];
  $('#ncu_a8u').val(datosa8[2]);
  $('#iba_a8u').val(datosa8[3]);
  $('#sal_a8u').val(datosa8[4]);
  $('#obs_a8u').val(datosa8[6])

  switch (tcue) {
    case 'Cheques':
      $("#tcu_a8u option[value=Cheques]").prop("selected", true);
      break;
    case 'Inversiones':
      $("#tcu_a8u option[value=Inversiones]").prop("selected", true);
      break;
    default:
      break;
  }

  switch (cons) {
    case 'Si':
      $("#si_a8u").prop("checked", true);
      break;
    case 'No':
      $("#no_a8u").prop("checked", true);
      break;
    default:
      break;
  }
}

function actualiza_a8() {
  update_envio($('#a8').serialize() + "&accion=upd_a8" + "&folio_a8=" + numFolio, "a8");
}

function datos_a9(datos) {
  datosa9 = datos.split("|");
  numFolio = datosa9[0];
  tdoc = datosa9[1];
  $('#ncu_a9u').val(datosa9[2]);
  $('#iba_a9u').val(datosa9[3]);
  $('#foud_a9u').val(datosa9[4]);
  $('#foid_a9u').val(datosa9[5]);
  $('#obs_a9u').val(datosa9[6]);

  switch (tdoc) {
    case 'Chequera':
      $("#tdoc_a9u option[value=1]").prop("selected", true);
      break;
    case 'Recibos oficiales internos':
      $("#tdoc_a9u option[value=2]").prop("selected", true);
      break;
    case 'Poliza de diario':
      $("#tdoc_a9u option[value=3]").prop("selected", true);
      break;
    case 'Póliza de ingreso':
      $("#tdoc_a9u option[value=4]").prop("selected", true);
      break;
    case 'Póliza de egresos':
      $("#tdoc_a9u option[value=5]").prop("selected", true);
      break;
    case 'Otros':
      $("#tdoc_a9u option[value=6]").prop("selected", true);
      break;
    default:
      break;
  }
}

function actualiza_a9() {
  update_envio($('#a9').serialize() + "&accion=upd_a9" + "&folio_a9=" + numFolio, "a9");
}

function datos_a10(datos) {
  datosa10 = datos.split("|");
  numFolio = datosa10[0];
  $('#fin_a10u').val(datosa10[1]);
  $('#fona_a10u').val(datosa10[2]);
  $('#fonc_a10u').val(datosa10[3]);
  $('#doc_a10u').val(datosa10[4]);
  $('#gas_a10u').val(datosa10[5]);

}

function actualiza_a10() {
  update_envio($('#a10').serialize() + "&accion=upd_a10" + "&folio_a10=" + numFolio, "a10");
}

function datos_a11(datos) {
  datosa11 = datos.split("|");
  numFolio = datosa11[0];
  $('#ndoc_a11u').val(datosa11[1]);
  $('#des_a11u').val(datosa11[2]);
  $('#imp_a11u').val(datosa11[3]);
  $('#fi_a11u').val(datosa11[4]);
  $('#ft_a11u').val(datosa11[5]);
  $('#obs_a11u').val(datosa11[6]);

}

function actualiza_a11() {
  update_envio($('#a11').serialize() + "&accion=upd_a11" + "&folio_a11=" + numFolio, "a11");
}

function datos_a12(datos) {
  datosa12 = datos.split("|");
  numFolio = datosa12[0];
  concurso = datosa12[5];
  $('#fape_a12u').val(datosa12[1]);
  $('#ffa_a12u').val(datosa12[2]);
  $('#npro_a12u').val(datosa12[3]);
  $('#uni_a12u').val(datosa12[4]);
  $('#oricon_a12u').val(datosa12[6]);
  $('#empg_a12u').val(datosa12[7]);
  $('#obs_a12u').val(datosa12[8]);

  switch (concurso) {
    case 'Adjudicación directa':
      $("#tcon_a12u option[value=1]").prop("selected", true);
      break;
    case 'Cuadro corporativo':
      $("#tcon_a12u option[value=2]").prop("selected", true);
      break;
    case 'Investigación restringida a tres proveedores':
      $("#tcon_a12u option[value=3]").prop("selected", true);
      break;
    case 'Licitación':
      $("#tcon_a12u option[value=4]").prop("selected", true);
      break;
    default:
      break;
  }
}

function actualiza_a12() {
  update_envio($('#a12').serialize() + "&accion=upd_a12" + "&folio_a12=" + numFolio, "a12");
}

function datos_a13(datos) {
  datosa13 = datos.split('|');
  numFolio = datosa13[0];
  $('#des_a13u').val(datosa13[1]);
  $('#ubi_a13u').val(datosa13[2]);
  $('#mau_a13u').val(datosa13[3]);
  $('#mej_a13u').val(datosa13[4]);
  $('#mpe_a13u').val(datosa13[5]);
  $('#avaf_a13u').val(datosa13[6]);
  $('#avafin_a13u').val(datosa13[7]);
  $('#obs_a13u').val(datosa13[8]);
}

function actualiza_a13() {
  update_envio($('#a13').serialize() + "&accion=upd_a13" + "&folio_a13=" + numFolio, "a13");
}

function update_envio(datos, anexo) {

  $.ajax({
    type: "POST",
    url: "actualizar_anexos.php",
    data: datos,
    success: function (res) {
      if (res == 1) {
        console.log(res);
        //alert('Registro actualizado');
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
        $('#tabla').load('Edicion_anexos/editar' + anexo + '.php');
        toastr.success('Registro actualizado correctamente', 'Exito', {
          "positionClass": "toast-bottom-right"
        });
        numFolio = "";
        datos="";
      } else {
        console.log("valor:" + res);
        alert('Petición no procesada: ocurrio algun problema intentalo nuevamente');
        numFolio = "";
        datos="";
      }

    }, error: function () {
      alert('Petición no procesada: ocurrio algun problema intentalo nuevamente');
    }
  });
}

/*---------------------------------------------------------------Funcion para borrar anexos-------------------------------------------------------------*/
function borrar_reg(datos) {
  //console.log(datos);
  if (confirmar()) {

    file_delete = datos.split('|');
    let data_b = "folio=" + file_delete[0] + "&anexo=" + file_delete[1];
    $.ajax({
      type: "POST",
      url: "borrar_registros.php",
      data: data_b,
      success: function (resp) {
        console.log(resp);
        $('#tabla').load('Edicion_anexos/editara' + file_delete[1] + '.php');
      }

    });
  }
}

function confirmar() {
  if (confirm('Se eliminara este registro. ¿Estas seguro?')) {
    return true;
  }
  else {
    return false;
  }
}
