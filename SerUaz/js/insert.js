
var control = false;

$(document).on('change','input[type="file"]',function(){
  let tam = this.files[0].size;
  let archivo=$('input[type="file"]').val();
  let extensiones = archivo.substring(archivo.lastIndexOf("."));
  
  if(extensiones != ".xls" && extensiones != ".xml" && extensiones != ".ods" && extensiones != ".pdf" && extensiones != ".xlsx" && extensiones !=".jpg" && extensiones !=".doc" && extensiones !=".docx") {
    toastr.warning('El archivo que estas inntentando enviar no tiene la extención permitida, verificalo e intentalo nuevamente. Se aceptan PDF y XLS','Archivo no permitido',{
      "positionClass": "toast-top-right",
      "showDuration": "400",
      "timeOut": "7500",
      "extendedTimeOut": "2000"
    });
    control=false;
    this.value='';
  }

  if(!control){
    if(tam < 80000000 ){
          control=true;
    }else{
      toastr.warning('El archivo que intentas enviar supera el limite establecido de 80Mb','Tamaño de archivo superado',{
        "positionClass": "toast-top-right",
        "showDuration": "400",
        "timeOut": "7500",
        "extendedTimeOut": "2000"
      });
      control=false;
      this.value='';
  
    }
  }
});


  function inserta_a1(){
      if(control){
      let datos = new FormData();
      datos.append('archivo',$('#archivo_a1')[0].files[0]);
      datos.append('objetivo',$('#objetivo').val());
      datos.append('creacion',$('#f_creacion').val());
      datos.append('legal',$('#decreto').val());
      datos.append('callfun',"in_a1");
      setEnvio(datos);
      }
}
function inserta_a2(){
  if(control){
      let datos_a2 = new FormData();
      datos_a2.append('archivo_a2',$('#archivo_a2')[0].files[0]);
      datos_a2.append('asunto_a2',$('#asunto_a2').val());
      datos_a2.append('unidad_a2',$('#unidad_a2').val());
      datos_a2.append('avance_a2',$('#avance_a2').val());
      datos_a2.append('obs_a2',$('#obs_a2').val());
      datos_a2.append('callfun',"in_a2");
      setEnvio(datos_a2);   
  }
}
function inserta_a3(){
    if(control){
      let datos_a3 = new FormData(); //variable donde se almacenara los datos a enviar al servidor
      datos_a3.append('archivo_a3',$('#archivo_a3')[0].files[0]);
      datos_a3.append('t_nomina',$('#tipo_nomina').val());
      datos_a3.append('f_corte',$('#fecha_corte').val());
      datos_a3.append('obs_a3',$('#obs_a3').val());
      datos_a3.append('callfun',"in_a3");
      setEnvio(datos_a3);
    }
}
function inserta_a4(){
    if(control){
      let datos_a4 = new FormData();
      datos_a4.append('archivo_a4',$('#archivo_a4')[0].files[0]);
      datos_a4.append('inventario',$('#inventario').val());
      datos_a4.append('f_cortea4',$('#f_cortea4').val());
      datos_a4.append('obs_a4',$('#obs_a4').val());
      datos_a4.append('callfun',"in_a4");
      setEnvio(datos_a4);
    }
}
function inserta_a5(){
    if(control){
      let datos_a5 = new FormData();
      datos_a5.append('archivo_a5',$('#archivo_a5')[0].files[0]);
      datos_a5.append('cantidad',$('#cantidad').val());
      datos_a5.append('des_a5',$('#des_a5').val());
      datos_a5.append('ubicacion',$('#ubicacion').val());
      datos_a5.append('obs_a5',$('#obs_a5').val());
      datos_a5.append('callfun',"in_a5");
      setEnvio(datos_a5);
    }
}
function inserta_a6(){
    if(control){
      let datos_a6 = new FormData();
      datos_a6.append('archivo_a6',$('#archivo_a6')[0].files[0]);
      datos_a6.append('area',$('#area').val());
      datos_a6.append('tipo_doc',$('#tipo_doc').val());
      datos_a6.append('tipo_archivo',$('#tipo_archivo').val());
      datos_a6.append('documento',$('#documento').val());
      datos_a6.append('callfun',"in_a6");
      setEnvio(datos_a6);
    }
}
function inserta_a7(){
    if(control){      
      let datos_a7 = new FormData();
      datos_a7.append('archivo_a7',$('#archivo_a7')[0].files[0]);
      datos_a7.append('tipo_info',$('#tipo_info').val());
      datos_a7.append('repo_info',$('input:radio[name=repo_inf]:checked').val());
      datos_a7.append('f_doc',$('#f_doc').val());
      datos_a7.append('obs_a7',$('#obs_a7').val());
      datos_a7.append('callfun',"in_a7");
      setEnvio(datos_a7);
    }
}

function inserta_a8(){
    if(control){  
      let datos_a8 = new FormData();
      datos_a8.append('archivo_a8',$('#archivo_a8')[0].files[0]);
      datos_a8.append('t_cuenta',$('#t_cuenta').val());
      datos_a8.append('numero_cuenta',$('#n_cuenta').val());
      datos_a8.append('bancaria',$('#i_bancaria').val());
      datos_a8.append('saldo',$('#saldo').val());
      datos_a8.append('con_banco',$('input:radio[name=conb_an8]:checked').val());
      datos_a8.append('obs_a8',$('#obs_a8').val());
      datos_a8.append('callfun',"in_a8");
      setEnvio(datos_a8);
    }
}
function inserta_a9(){
    if(control){      
      let datos_a9 = new FormData();
      datos_a9.append('archivo_a9',$('#archivo_a9')[0].files[0]);
      datos_a9.append('tipo_docu',$('#tipo_docu').val());
      datos_a9.append('num_cuenta',$('#num_cuenta').val());
      datos_a9.append('institucion',$('#in_bancaria').val());
      datos_a9.append('folio_f',$('#folio').val());
      datos_a9.append('folio_n',$('#folio_nuevo').val());
      datos_a9.append('obs_a9',$('#obs_a9').val());
      datos_a9.append('callfun',"in_a9");
      setEnvio(datos_a9); 
    }
}
function inserta_a10(){
    if(control){
      let datos_a10 = new FormData();
      datos_a10.append('archivo_a10',$('#archivo_a10')[0].files[0]);
      datos_a10.append('fecha_info',$('#f_informe').val());
      datos_a10.append('f_autorizado',$('#fondo_autorizado').val());
      datos_a10.append('fondo_caja',$('#fondo_caja').val());
      datos_a10.append('documentos',$('#documentos').val());
      datos_a10.append('gastos_p',$('#gastos_p').val());
      datos_a10.append('callfun',"in_a10");
      setEnvio(datos_a10);  
    }
}


function inserta_a11(){
    if(control){
      let datos_a11 = new FormData();
      datos_a11.append('archivo_a11',$('#archivo_a11')[0].files[0]);
      datos_a11.append('num_doc',$('#nume_doc').val());
      datos_a11.append('des_a11',$('#des_a11').val());
      datos_a11.append('importe',$('#importe').val());
      datos_a11.append('fecha_i',$('#fecha_i').val());
      datos_a11.append('fecha_f',$('#fecha_f').val());
      datos_a11.append('obs_a11',$('#obs_a11').val());
      datos_a11.append('callfun',"in_a11");
      setEnvio(datos_a11); 
    }
}

function inserta_a12(){
    if(control){
      let datos_a12 = new FormData();
      datos_a12.append('archivo_a12',$('#archivo_a12')[0].files[0]);
      datos_a12.append('fecha_a',$('#fecha_a').val());
      datos_a12.append('fecha_fallo',$('#fecha_fallo').val());
      datos_a12.append('n_proyecto',$('#n_proyecto').val());
      datos_a12.append('unidad',$('#unidad').val());
      datos_a12.append('tipo_concurso',$('#concurso').val());
      datos_a12.append('origen_concurso',$('#origen_c').val());
      datos_a12.append('empresa_ganadora',$('#empresa_gan').val());
      datos_a12.append('obs_a12',$('#obs_a12').val());
      datos_a12.append('callfun',"in_a12");
      setEnvio(datos_a12); 
    }
}
function inserta_a13(){
    if(control){
      let datos_a13 = new FormData();
      datos_a13.append('archivo_a13',$('#archivo_a13')[0].files[0]);
      datos_a13.append('des_a13',$('#des_a13').val());
      datos_a13.append('ubicacion',$('#ubicacion').val());
      datos_a13.append('monto_a13',$('#monto_a').val());
      datos_a13.append('monto_ejercido',$('#monto_e').val());
      datos_a13.append('monto_ejercer',$('#monto_pe').val());
      datos_a13.append('a_fisico',$('#avance_f').val());
      datos_a13.append('avance_fin',$('#avance_fin').val());
      datos_a13.append('obs_a13',$('#obs_a13').val());
      datos_a13.append('callfun',"in_a13");
      setEnvio(datos_a13);
    }
}


 function setEnvio(datos){

     $.ajax({
        url: 'all_anexos.php',
        type: 'POST',
        contentType:false,
        data: datos,
        beforeSend:function(){
          $('#oculto').show();},
        processData: false,
        cache: false,
        success:function(res){
                   
          if(res == 1){
              console.log(res);
              $('#oculto').hide();
              $('input[type="file"]').val('');
              $('input[type="date"]').val('');
              $('input[type="text"]').val('');
              $('textarea').val('');
              toastr.success('Registro agregado correctamente', 'Exito',{
                "positionClass": "toast-bottom-right"
               });
            }else if(res == 2){
              console.log(res);
              $('#oculto').hide();
              $('input[type="file"]').val('');
              toastr.warning('El archivo que estas inntentando enviar no tiene la extención permitida, verificalo e intentalo nuevamente. Se aceptan PDF y XLS','Precaución: Archivo no permitido',{
                "positionClass": "toast-top-right",
                "showDuration": "400",
                "timeOut": "7500",
                "extendedTimeOut": "2000"
              });
            }else if(res == 3){
              console.log(res);
              $('#oculto').hide();
              $('input[type="file"]').val('');
              toastr.warning('Este archivo ya existe en el directorio actula, verificalo e intentalo nuevamente','Archivo existente',{
                "positionClass": "toast-top-right",
                "showDuration": "400",
                "timeOut": "7500",
                "extendedTimeOut": "2000"
              });

            }else if(res == 4){
              console.log(res);
              $('#oculto').hide();
              $('input[type="file"]').val('');
              toastr.warning('El archivo que intentas enviar supera el limite establecido de 80Mb','Precaución: Tamaño de archivo superado',{
                "positionClass": "toast-top-right",
                "showDuration": "400",
                "timeOut": "7500",
                "extendedTimeOut": "2000"
              });
            }else{
              console.log(res);
              $('#oculto').hide();
              $('input[type="file"]').val('');
              toastr.error('Hubo un problema al subir el archivo, intentalo nuevamente.','Error',{
                "positionClass": "toast-top-right",
                "showDuration": "400",
                "timeOut": "7500",
                "extendedTimeOut": "2000"
              });
            }
        },
        error:function(){
          alert('error');
        }
      });
  }

  
