
/*---------------------------------------------------------funcion global para todos los anexos--------------------------------------------------------------*/


//evaluar caracteres especiales
function getValidaT(texto,campo,id){
          $('#aviso' + campo).hide();//ocultar el aviso del aviso de espacio en blanco
          if(!/^([a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s\d])*$/.test(texto)){
            $('#aviso_' + campo).html('Este campo no admite caracteres especiales');
            $('#aviso_' + campo).show();
             setTimeout(function(){$('#'+id).val("");},1000);
          }else{
          $('#aviso_' + campo).hide();
          }
        }


function getValidaN(numero,campo,id){
      var num=$('#'+id).val();
          $('#aviso' + campo).hide();
        if (isNaN(num)){
          $('#aviso' + campo).html('Este campo es solo númerico');
          $('#aviso' + campo).show();
           setTimeout(function(){$('#'+id).val("");},1000); 
          }else if( num < 0){
          $('#aviso' + campo).html('Este campo no pude tener valores negativos');
          $('#aviso' + campo).show();
           setTimeout(function(){$('#'+id).val("");},1000); 
           

          }
        }


  var control = true;//varible de control para el envio de los datos al servidor 

//verificar el tamaño del archivo
$(document).on('change','input[type="file"]',function(){
  let tam = this.files[0].size;

  if(tam >800000000){
          alert('Archivo demasiado grande');
          control=false;
          this.value='';
        }

});
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------funciones necesarias para el anexo 1------------------------------------------------------------*/


  //envia los datos al servidor siempre y cuando no haya algun error

  function validar_a1(event){

  	if(getCampos_a1()){//llamada a la función para evitar campos vacios 

      if(control){//se evalua el valor de la variable de cotrol
      	
      event.preventDefault();
      
      let datos = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos.append('archivo',$('#archivo_a1')[0].files[0]);
      datos.append('objetivo',$('#objetivo').val());
      datos.append('creacion',$('#f_creacion').val());
      datos.append('legal',$('#decreto').val());
      datos.append('callfun',$('#in_anexos').val());


      document.getElementById('oculto').style.display='block';

      setEnvio(datos);
      

      }
  }
  	
}

/*-----------------------------------------------------------------Funciones para el anexo2-------------------------------------------------------------------*/


function validar_a2(event){

  if(getCampos_a2()){
    if(control){

      event.preventDefault();
      
      let datos_a2 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a2.append('archivo_a2',$('#archivo_a2')[0].files[0]);
      datos_a2.append('asunto_a2',$('#asunto_a2').val());
      datos_a2.append('unidad_a2',$('#unidad_a2').val());
      datos_a2.append('avance_a2',$('#avance_a2').val());
      datos_a2.append('obs_a2',$('#obs_a2').val());
      datos_a2.append('callfun',$('#in_anexos').val());

      $('#oculto').show();

      setEnvio(datos_a2);
      
    }
  }

}

/*-----------------------------------------------------------------Funciones para el anexo3-------------------------------------------------------------------*/


function validar_a3(event){
  if(getCampos_a3()){
    if(control){

      event.preventDefault();
      
      let datos_a3 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a3.append('archivo_a3',$('#archivo_a3')[0].files[0]);
      datos_a3.append('t_nomina',$('#tipo_nomina').val());
      datos_a3.append('f_corte',$('#fecha_corte').val());
      datos_a3.append('obs_a3',$('#obs_a3').val());
      datos_a3.append('callfun',$('#in_anexos').val());

      document.getElementById('oculto').style.display='block';

      setEnvio(datos_a3);

    }
  }


}
/*---------------------------------------------------------------Funciones para el anexo 4--------------------------------------------------------------------*/

function validar_a4(event){
  if(getCampos_a4()){
    if(control){

       event.preventDefault();
      
      let datos_a4 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a4.append('archivo_a4',$('#archivo_a4')[0].files[0]);
      datos_a4.append('inventario',$('#inventario').val());
      datos_a4.append('f_cortea4',$('#f_cortea4').val());
      datos_a4.append('obs_a4',$('#obs_a4').val());
      datos_a4.append('callfun',$('#in_anexos').val());

      document.getElementById('oculto').style.display='block';

      setEnvio(datos_a4);
    
    }
  }
}


function valida_a5(event){

  if(getCampos_a5()){
    if(control){
      
      event.preventDefault();
      
      let datos_a5 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a5.append('archivo_a5',$('#archivo_a5')[0].files[0]);
      datos_a5.append('cantidad',$('#cantidad').val());
      datos_a5.append('des_a5',$('#des_a5').val());
      datos_a5.append('ubicacion',$('#ubicacion').val());
      datos_a5.append('obs_a5',$('#obs_a5').val());
      datos_a5.append('callfun',$('#in_anexos').val());

      document.getElementById('oculto').style.display='block';

      setEnvio(datos_a5);
    }
  }


}

function valida_a6(event){
  if(getCampos_a6()){
    if(control){

        event.preventDefault();
      
      let datos_a6 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a6.append('archivo_a6',$('#archivo_a6')[0].files[0]);
      datos_a6.append('area',$('#area').val());
      datos_a6.append('tipo_doc',$('#tipo_doc').val());
      datos_a6.append('tipo_archivo',$('#tipo_archivo').val());
      datos_a6.append('documento',$('#documento').val());
      datos_a6.append('callfun',$('#in_anexos').val());

      document.getElementById('oculto').style.display='block';

      setEnvio(datos_a6);
    


    }
  }
}
function valida_a7(event){
  if(getCampos_a7()){
    if(control){

      event.preventDefault();
      
      let datos_a7 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a7.append('archivo_a7',$('#archivo_a7')[0].files[0]);
      datos_a7.append('tipo_info',$('#tipo_info').val());
      datos_a7.append('repo_info',$('#repo_inf').val());
      datos_a7.append('f_doc',$('#f_doc').val());
      datos_a7.append('obs_a7',$('#obs_a7').val());
      datos_a7.append('callfun',$('#in_anexos').val());

      document.getElementById('oculto').style.display='block';

      setEnvio(datos_a7);
    

    }
  }

}

function valida_a8(event){

  if(getCampos_a8()){
    if(control){
      
      event.preventDefault();
      
      let datos_a8 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a8.append('archivo_a8',$('#archivo_a8')[0].files[0]);
      datos_a8.append('t_cuenta',$('#t_cuenta').val());
      datos_a8.append('numero_cuenta',$('#n_cuenta').val());
      datos_a8.append('bancaria',$('#i_bancaria').val());
      datos_a8.append('saldo',$('#saldo').val());
      datos_a8.append('con_banco',$('#con_banco').val());
      datos_a8.append('obs_a8',$('#obs_a8').val());
      datos_a8.append('callfun',$('#in_anexos').val());

      
      $('#oculto').show();

      setEnvio(datos_a8);
    }
  }

}

function valida_a9(event){
  if(getCampos_a9()){
    if(control){

      event.preventDefault();
      
      let datos_a9 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a9.append('archivo_a9',$('#archivo_a9')[0].files[0]);
      datos_a9.append('tipo_docu',$('#tipo_docu').val());
      datos_a9.append('num_cuenta',$('#num_cuenta').val());
      datos_a9.append('institucion',$('#in_bancaria').val());
      datos_a9.append('folio_f',$('#folio').val());
      datos_a9.append('folio_n',$('#folio_nuevo').val());
      datos_a9.append('obs_a9',$('#obs_a9').val());
      datos_a9.append('callfun',$('#in_anexos').val());

      
      $('#oculto').show();

      setEnvio(datos_a9);
      
    }
  }

}
function valida_a10(event){
  if(getCampos_a10()){
    if(control){
      
      event.preventDefault();
      
      let datos_a10 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a10.append('archivo_a10',$('#archivo_a10')[0].files[0]);
      datos_a10.append('fecha_info',$('#f_informe').val());
      datos_a10.append('f_autorizado',$('#fondo_autorizado').val());
      datos_a10.append('fondo_caja',$('#fondo_caja').val());
      datos_a10.append('documentos',$('#documentos').val());
      datos_a10.append('gastos_p',$('#gastos_p').val());
      datos_a10.append('sumas',$('#sumas').val());
      datos_a10.append('callfun',$('#in_anexos').val());

      
      $('#oculto').show();

      setEnvio(datos_a10);  
    }
  }
}


function valida_a11(event){

  if(getCampos_a11()){
    if(control){

      event.preventDefault();
      
      let datos_a11 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a11.append('archivo_a11',$('#archivo_a11')[0].files[0]);
      datos_a11.append('num_doc',$('#nume_doc').val());
      datos_a11.append('des_a11',$('#des_a11').val());
      datos_a11.append('importe',$('#importe').val());
      datos_a11.append('fecha_i',$('#fecha_i').val());
      datos_a11.append('fecha_f',$('#fecha_f').val());
      datos_a11.append('obs_a11',$('#obs_a11').val());
      datos_a11.append('callfun',$('#in_anexos').val());

      $('#oculto').show();
      setEnvio(datos_a11); 

    }
  }
}

function valida_a12(event){
  if(getCampos_a12()){
    if(control){
      event.preventDefault();
      
      let datos_a12 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a12.append('archivo_a12',$('#archivo_a12')[0].files[0]);
      datos_a12.append('fecha_a',$('#fecha_a').val());
      datos_a12.append('fecha_fallo',$('#fecha_fallo').val());
      datos_a12.append('n_proyecto',$('#n_proyecto').val());
      datos_a12.append('unidad',$('#unidad').val());
      datos_a12.append('tipo_concurso',$('#concurso').val());
      datos_a12.append('origen_concurso',$('#origen_c').val());
      datos_a12.append('empresa_ganadora',$('#empresa_gan').val());
      datos_a12.append('obs_a12',$('#obs_a12').val());
      datos_a12.append('callfun',$('#in_anexos').val());

      $('#oculto').show();
      setEnvio(datos_a12);
      
    }
  }
}
function valida_a13(event){
  if(getCampos_a13()){
    if(control){
       event.preventDefault();
      
      let datos_a13 = new FormData(); //variable donde se almacenara los datos a enviar al servidor


      datos_a13.append('archivo_a13',$('#archivo_a13')[0].files[0]);
      datos_a13.append('des_a13',$('#des_a13').val());
      datos_a13.append('ubicacion',$('#ubicacion').val());
      datos_a13.append('monto_a13',$('#monto_a').val());
      datos_a13.append('monto_ejercido',$('#monto_e').val());
      datos_a13.append('monto_ejercer',$('#monto_pe').val());
      datos_a13.append('a_fisico',$('#avance_f').val());
      datos_a13.append('avance_fin',$('#avance_fin').val());
      datos_a13.append('obs_a13',$('#obs_a13').val());
      datos_a13.append('callfun',$('#in_anexos').val());

      $('#oculto').show();
      setEnvio(datos_a13);
     
    }
  }

}

/*----------------------------------------------------------------Funcion general para el envio de los datos al servidor--------------------------------------*/

 function setEnvio(datos){

     $.ajax({
        url: 'all_anexos.php',
        type: 'POST',
        contentType:false,
        data: datos,
        processData: false,
        cache: false,
        success:function(res){
          console.log("valor de respuesta:" + res);
           

          if(res == 1){
              //alert("Petición procesada: Registro agregado");
              console.log(res);
              $('#oculto').hide();
              // document.getElementById('oculto').style.display='none';
               $('input[type="file"]').val('');
                $('input[type="date"]').val('');
                $('input[type="text"]').val('');
                $('textarea').val('');
               setTimeout(function(){ alert("Petición procesada: Registro agregado"); }, 350);
            
            }else{
              console.log(res);
              $('#oculto').hide();
              $('input[type="file"]').val('');
              $('input[type="date"]').val('');
              $('textarea').val('');
               setTimeout(function(){ alert("Petició no procesada: Hubo algun problema intentalo nuevamente"); }, 350);    
            }    
        },
        error:function(){
          alert('error');
        }
      });
  }
