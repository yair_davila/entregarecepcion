
function getCampos_a1(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso3').hide();
    $('#aviso_0').hide();
    $('#aviso_1').hide();
    $('#aviso_2').hide();
    $('#aviso_3').hide();

    if($('#objetivo').val()==""){
      $('#aviso0').show();
      $('#objetivo').focus();
      return false;
    
    }
    if($('#f_creacion').val()==""){
      $('#aviso1').show();
      $('#f_creacion').focus();
      return false;
    }
    if($('#decreto').val()==""){
      $('#aviso2').show();
      $('#decreto').focus();
      return false;

    }
    if($('#archivo_a1').val()==""){
      $('#aviso3').show();
      $('#archivo_a1').focus();
      return false;
    }
    return true;
    
  }


  function getCampos_a2(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso3').hide();
    $('#aviso4').hide();
    $('#aviso_0').hide();
    $('#aviso_1').hide();
    $('#aviso_2').hide();
    $('#aviso_3').hide();

  if($('#asunto_a2').val()==""){
      $('#aviso0').show();
      $('#asunto_a2').focus();
      return false;
    
    }
  if($('#unidad_a2').val()==""){
      $('#aviso1').show();
      $('#unidad_a2').focus();
      return false;
    
    }
   if($('#avance_a2').val()==""){
      $('#aviso2').show();
      $('#avance_a2').focus();
      return false;
    
    } 
  if($('#obs_a2').val()==""){
      $('#aviso3').show();
      $('#obs_a2').focus();
      return false;
    
    }

  if($('#archivo_a2').val()==""){
      $('#aviso4').show();
      $('#archivo_a2').focus();
      return false;
    
    }
  return true;
}


function getCampos_a3(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso_1').hide();
  if($('#tipo_nomina').val()==""){
      $('#tipo_nomina').focus();
      return false;
    }
  if($('#fecha_corte').val()==""){
      $('#aviso0').show();
      $('#fecha_corte').focus();
      return false;
    }
  if($('#obs_a3').val()==""){
      $('#aviso1').show();
      $('#obs_a3').focus();
      return false;
    }
  if($('#archivo_a3').val()==""){
      $('#aviso2').show();
      $('#archivo_a3').focus();
      return false;
    }
    return true;
}

function getCampos_a4(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso3').hide();
    $('#aviso_2').hide();
  if($('#inventario').val().trim()==""){
      $('#aviso0').show();
      $('#inventario').focus();
      return false;
    }
  if($('#f_cortea4').val()==""){
      $('#aviso1').show();
      $('#f_cortea4').focus();
      return false;
    }
  if($('#obs_a4').val()==""){
      $('#aviso2').show();
      $('#obs_a4').focus();
      return false;
    }
  if($('#archivo_a4').val()==""){
      $('#aviso3').show();
      $('#archivo_a4').focus();
      return false;
    }
    return true;
}


function getCampos_a5(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso3').hide();
    $('#aviso4').hide();
    $('#aviso_0').hide();
    $('#aviso_1').hide();
    $('#aviso_2').hide();
    $('#aviso_3').hide();

  if($('#cantidad').val()==""){
      $('#aviso0').show();
      $('#cantidad').focus();
      return false;
  }
  if($('#des_a5').val()==""){
      $('#aviso1').show();
      $('#des_a5').focus();
      return false;
  }
  if($('#ubicacion').val()==""){
      $('#aviso2').show();
      $('#ubicacion').focus();
      return false;
  }
  if($('#obs_a5').val()==""){
      $('#aviso3').show();
      $('#obs_a5').focus();
      return false;
  }
  if($('#archivo_a5').val()==""){
      $('#aviso4').show();
      $('#archivo_a5').focus();
      return false;
  }
  return true;

}


function getCampos_a6(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso3').hide();
    $('#aviso4').hide();
    $('#aviso4').hide();
    $('#aviso_0').hide();
    $('#aviso_4').hide();
  if($('#area').val()==""){
      $('#aviso0').show();
      $('#area').focus();
      return false;
  }
  if($('#tipo_doc').val().trim()==""){
    $('#aviso1').show();
      $('#tipo_doc').focus();
      return false;
  }
  if($('#tipo_archivo').val().trim()==""){
      $('#aviso2').show();
      $('#tipo_archivo').focus();
      return false;
  }
  if($('#documento').val()==""){
      $('#aviso3').show();
      $('#documento').focus();
      return false;
  }
  if($('#archivo_a6').val()==""){
      $('#aviso4').show();
      $('#archivo_a6').focus();
      return false;
  }
  return true;

}


function getCampos_a7(){
    $('#aviso0').hide();
    $('#aviso1').hide();
    $('#aviso2').hide();
    $('#aviso3').hide();
    $('#aviso_2').hide();

if($('#tipo_info').val().trim()==""){
      $('#aviso0').show();
      $('#tipo_info').focus();
      return false;
  }

  if($('#f_doc').val()==""){
      $('#aviso1').show();
      $('#f_doc').focus();
      return false;
  }

  if($('#obs_a7').val()==""){
      $('#aviso2').show();
      $('#obs_a7').focus();
      return false;
  }
  if($('#archivo_a7').val()==""){
      $('#aviso3').show();
      $('#archivo_a7').focus();
      return false;
  }
  return true;

}


function getCampos_a8(){
  $('#aviso0').hide();
  $('#aviso1').hide();
  $('#aviso2').hide();
  $('#aviso3').hide();
  $('#aviso4').hide();
  $('#aviso5').hide();
  if($('#t_cuenta').val().trim() == ""){
      $('#aviso0').html('Debes seleccionar una opcion');
      $('#aviso0').show();
      $('#t_cuenta').focus();
      return false;
  }
  if($('#n_cuenta').val()==""){
      $('#aviso1').show();
      $('#n_cuenta').focus();
      return false;
  }
  if($('#i_bancaria').val()==""){
      $('#aviso2').show();
      $('#i_bancaria').focus();
      return false;
  }
  if($('#saldo').val()==""){
      $('#aviso3').show();
      $('#saldo').focus();
      return false;
  }
  if($('#obs_a8').val()==""){
      $('#aviso4').show();
      $('#obs_a8').focus();
      return false;
  }
  if($('#archivo_a8').val()==""){
      $('#aviso5').show();
      $('#archivo_a8').focus();
      return false;
  }
  return true;
}


function getCampos_a9(){
  $('#aviso0').hide();
  $('#aviso1').hide();
  $('#aviso2').hide();
  $('#aviso3').hide();
  $('#aviso4').hide();
  $('#aviso5').hide();
  $('#aviso6').hide();


  if($('#tipo_docu').val().trim() == ""){
      $('#aviso0').html('Debes seleccionar una opcion');
      $('#aviso0').show();
      $('#tipo_docu').focus();
      return false;
  }
   if($('#num_cuenta').val()==""){
      $('#aviso1').show();
      $('#num_cuenta').focus();
      return false;
  }
   if($('#in_bancaria').val()==""){
      $('#aviso2').show();
      $('#in_bancaria').focus();
      return false;
  }
   if($('#folio').val()==""){
      $('#aviso3').show();
      $('#folio').focus();
      return false;
  }
   if($('#folio_nuevo').val()==""){
      $('#aviso4').show();
      $('#folio_nuevo').focus();
      return false;
  }
   if($('#obs_a9').val()==""){
      $('#aviso5').show();
      $('#obs_a9').focus();
      return false;
  }
   if($('#archivo_a9').val()==""){
      $('#aviso6').show();
      $('#archivo_a9').focus();
      return false;
  }
  return true;

}


function getCampos_a10(){
  $('#aviso0').hide();
  $('#aviso1').hide();
  $('#aviso2').hide();
  $('#aviso3').hide();
  $('#aviso4').hide();
  $('#aviso6').hide();

  if($('#f_informe').val()==""){
      $('#aviso0').show();
      $('#f_informe').focus();
      return false;
  }
  if($('#fondo_autorizado').val()==""){
      $('#aviso1').show();
      $('#fondo_autorizado').focus();
      return false;
  }
  if($('#fondo_caja').val()==""){
      $('#aviso2').show();
      $('#fondo_caja').focus();
      return false;
  }
  if($('#documentos').val()==""){
      $('#aviso3').show();
      $('#documentos').focus();
      return false;
  }
  if($('#gastos_p').val()==""){
      $('#aviso4').show();
      $('#gastos_p').focus();
      return false;
  }
  
  if($('#archivo_a10').val()==""){
      $('#aviso6').show();
      $('#archivo_a10').focus();
      return false;
  }
  return true;

}

function getCampos_a11(){
  $('#aviso0').hide();
  $('#aviso1').hide();
  $('#aviso2').hide();
  $('#aviso3').hide();
  $('#aviso4').hide();
  $('#aviso5').hide();
  $('#aviso6').hide();

  if($('#nume_doc').val()==""){
      $('#aviso0').show();
      $('#nume_doc').focus();
      return false;
  }
  if($('#des_a11').val()==""){
      $('#aviso1').show();
      $('#des_a11').focus();
      return false;
  }
  if($('#importe').val()==""){
      $('#aviso2').show();
      $('#importe').focus();
      return false;
  }
  if($('#fecha_i').val()==""){
      $('#aviso3').show();
      $('#fecha_i').focus();
      return false;
  }
  if($('#fecha_f').val()==""){
      $('#aviso4').show();
      $('#fecha_f').focus();
      return false;
  }
  if($('#obs_a11').val()==""){
      $('#aviso5').show();
      $('#obs_a11').focus();
      return false;
  }
  if($('#archivo_a11').val()==""){
      $('#aviso6').show();
      $('#archivo_a11').focus();
      return false;
  }
  return true;
}


function getCampos_a12(){
  $('#aviso0').hide();
  $('#aviso1').hide();
  $('#aviso2').hide();
  $('#aviso3').hide();
  $('#aviso4').hide();
  $('#aviso5').hide();
  $('#aviso6').hide();
  $('#aviso7').hide();
  $('#aviso8').hide();

  if($('#fecha_a').val()==""){
      $('#aviso0').show();
      $('#fecha_a').focus();
      return false;
  }

  if($('#fecha_fallo').val()==""){
      $('#aviso1').show();
      $('#fecha_fallo').focus();
      return false;
  }

  if($('#n_proyecto').val()==""){
      $('#aviso2').show();
      $('#n_proyecto').focus();
      return false;
  }

  if($('#unidad').val()==""){
      $('#aviso3').show();
      $('#unidad').focus();
      return false;
  }

  if($('#concurso').val().trim() ==""){
      $('#aviso4').show();
      $('#concurso').focus();
      return false;
  }

  if($('#origen_c').val()==""){
      $('#aviso5').show();
      $('#origen_c').focus();
      return false;
  }

  if($('#empresa_gan').val()==""){
      $('#aviso6').show();
      $('#empresa_g').focus();
      return false;
  }

  if($('#obs_a12').val()==""){
      $('#aviso7').show();
      $('#obs_a12').focus();
      return false;
  }

  if($('#archivo_a12').val()==""){
      $('#aviso8').show();
      $('#archivo_a12').focus();
      return false;
  }
  return true;

}


function getCampos_a13(){
  $('#aviso0').hide();
  $('#aviso1').hide();
  $('#aviso2').hide();
  $('#aviso3').hide();
  $('#aviso4').hide();
  $('#aviso5').hide();
  $('#aviso6').hide();
  $('#aviso7').hide();
  $('#aviso8').hide();
  if($('#des_a13').val()==""){
      $('#aviso0').show();
      $('#des_a13').focus();
      return false;
  }
  if($('#ubicacion').val()==""){
      $('#aviso1').show();
      $('#ubicacion').focus();
      return false;
  }
  if($('#monto_a').val()==""){
      $('#aviso2').show();
      $('#monto_a').focus();
      return false;
  }
  if($('#monto_e').val()==""){
      $('#aviso3').show();
      $('#monto_e').focus();
      return false;
  }
  if($('#monto_pe').val()==""){
      $('#aviso4').show();
      $('#monto_pe').focus();
      return false;
  }
  if($('#avance_f').val()==""){
      $('#aviso5').show();
      $('#avance_f').focus();
      return false;
  }
  if($('#avance_fin').val()==""){
      $('#aviso6').show();
      $('#avance_fin').focus();
      return false;
  }
  if($('#obs_a13').val()==""){
      $('#aviso7').show();
      $('#obs_a13').focus();
      return false;
  }
  if($('#archivo_a13').val()==""){
      $('#aviso8').show();
      $('#archivo_a12').focus();
      return false;
  }
  return true;

}




