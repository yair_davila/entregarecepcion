<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  }

  require '../conexion.php'; 
     
    
?>
<div class="container-fluid">
  <div class="form-group">
    <h4>Crear nuevo periodo <span class="glyphicon glyphicon-list-alt"></h4>
      <form id="cr_periodo">
        <label>Entregas disponibles</label>
        <?php
          $entregas_dis= $conexion-> prepare(" SELECT E.id_entrega,C.nombre, E.espacio FROM CAT_UNIDADES C join ENTREGA E on C.id_unidad = E.id_unidad");
          $entregas_dis->execute();
        if ($entregas_dis -> rowCount() == 0): ?>
          <select class="form-control" disabled>
            <option value="0">No hay entregas disponibles</option>
          </select>
        <?php endif;?>


      <?php if($entregas_dis -> rowCount() != 0): ?>      
        <select name="dis_entrega" id="dis_entrega" class="form-control">
          <?php while($fila = $entregas_dis ->fetch(PDO::FETCH_ASSOC)):?>
          <option value= "<?php echo $fila['id_entrega']?>" > <?php echo $fila['nombre'] . "- " . mb_strtoupper($fila['espacio'],'utf-8') ;?> </option>
          <?php endwhile; $entregas_dis -> closeCursor();?>      
        </select>

        <label>Responsable que entrega</label>
        <input type="text" class="form-control" name="res_entrega" id="res_entrega" placeholder="Responsable que entrega">  
        <br>
        <label>Responsable que recibe</label>
        <input type="text" class="form-control" name="res_recibe" id="res_recibe" placeholder="Responsable que recibe">
        <br>
        <label>Fecha inicio</label>
        <input type="date" class="form-control" name="fe_inicio" id="fe_inicio">
        <br>
        <label>Fecha termino</label>
        <input type="date" class="form-control" name="fe_fin" id="fe_fin">
        <br>
        <label>Anexos aplicables</label><br>
        <div class="row" style="background-color: #F4F6F7;">
          <div class="col-md-6">
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="1">1. Marco legal y de Actuación</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="2">2. Asuntos en Trámite</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="3">3. Recursos Humanos</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="4">4. Recursos Materiales</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="5">5. Libros, Boletines y Libretos de Consulta</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="6">6. Principales Archivos de Documentos</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="7">7. Recsursos Financieros - Informes</label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="8" >8. Recursos Financieros - Bancos</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="9">9. Corte Documental</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="10">10. Fondo Revolvente</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="11">11. Acuerdos, Convenios y Contratos de Servicios</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="12">12. Actas de Concurso</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="13">13. Obras Públicas</label>
            </div>
          </div>
          <br>
          <label for="anexos[]" class="error">
      </div>
        <label>Tipo periodo</label>
        <select name="t_periodo" id="t_periodo" class="form-control">
          <option value="" selected>Selecciona una opción...</option>
          <option value="0">Ordianrio</option>
          <option value="1">Extraordinario</option>
        </select>
        <br>
        <button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-send"></span> Crear</button>
    <?php endif; $entregas_dis->closeCursor(); $conexion=null; ?>
    </form>
  </div>

</div>
<script type="text/javascript">
  $(document).ready(function () {

      $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });
  
  $('#cr_periodo').validate({
        rules: {
            res_entrega: { required: true, texto:true},
            res_recibe:{required: true,texto:true},
            fe_inicio:{required:true},
            fe_fin:{required:true},
            t_periodo:{required:true},
            "anexos[]":{required:true}

        },
        messages: {
          res_entrega: {
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
          res_recibe:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
          fe_fin:{
              required: "Completa el campo por favor",
            
           },
           fe_inicio:{
              required: "Completa el campo por favor",
            
           },
           t_periodo:{
              required: "Completa el campo por favor",
            
           },
           "anexos[]":{
              required: "Debes seleccionar como mínimo un anexo",
           },           
            
        },
        submitHandler: function (){
            create_periodo();
        }
    });

    });
</script>
