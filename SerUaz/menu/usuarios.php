<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");

  }    
  
?>
<form id="usr_create">
<div class="container-fluid">
	<h4>Nuevo usuario <span class="glyphicon glyphicon-user"></span></h4>
			<div class="form-group">
				<label>Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre completo">
				<br>
				<label>Usuario</label>
				<input type="text" name="usr" id="usr" class="form-control" placeholder="Usuario asignado">
				<br>
				<label>Password</label>
				<input type="password" name="pass" id="pass" class="form-control" placeholder="Password">
				<br>
				<label>Tipo de usuario</label>
				<select class="form-control" name="tipo_usr" id="tipo_usr">
					<option value="" selected>Selecciona una opción...</option>
					<option value="1">Capturista</option>
					<option value="2">Responsable de unidad</option>
				</select>			
			</div>
			<button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-floppy-saved"></span> Crear</button>
	</div>
<form>

<script type="text/javascript">
	$(document).ready(function () {

	$.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });
	$.validator.addMethod('passwor',function(value,element){
        return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(value);
      });
	

	$('#usr_create').validate({
        rules: {
            nombre: { required: true, texto:true},
            usr:{required: true,texto:true},
            tipo_usr:{required:true},
            pass:{required:true,passwor:true}

        },
        messages: {
            nombre: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            usr:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
            tipo_usr:{
            	required:"Selecciona una opcion",
            },
            pass:{
            	required: "Completa el campo por favor",
            	passwor:"El password tiene almenos 1 digito,1 mayuscula,1 minúscula, 1 caracter especial y una loguitud mínima de 8 caracteres",
            }
           
            
        },
        submitHandler: function () {
            create_usuario();
        }
    });


	});
</script>
