<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location: https://localhost/SerUaz/");
  
  }

  require '../conexion.php'; 
    
?>
<div class="container-fluid">
	<div class="form-group">
		<h4>Asignacion de usuarios a entrega - recepción <span class="glyphicon glyphicon-tags"></h4>
		<form id="asig_entrega">
			<label>Usuario</label>
			<?php

				$sql_usuario = "SELECT * FROM USUARIOS ORDER BY NOMBRE ASC";

				$resultado = $conexion -> prepare($sql_usuario);
				
				$resultado -> execute();

			?>

			<select class="form-control" id="usuario" name="usuario">
				<?php while($fila = $resultado -> fetch(PDO::FETCH_ASSOC)):?>

				<option value="<?php echo $fila['ID_USUARIO'] ?>"><?php echo $fila['NOMBRE']?></option>
				
				<?php endwhile; $resultado -> closeCursor();?>
			</select>
				
			<label>Entregas</label>
			<?php 
			$res_entrega = $conexion -> prepare("SELECT E.ID_ENTREGA,E.ID_UNIDAD,E.ESPACIO,C_U.NOMBRE FROM ENTREGA E JOIN CAT_UNIDADES C_U ON C_U.ID_UNIDAD= E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA=P.ID_ENTREGA AND E.ID_ENTREGA NOT IN (SELECT E_U.ID_ENTREGA FROM ENTREGA_USUARIOS E_U ORDER BY C_U.NOMBRE)");
			$res_entrega -> execute();
			?>

			<?php if ($res_entrega->rowCount() == 0): ?>

				<select class="form-control" disabled>
					
					<option value="0">No hay entregas disponibles</option>
				
				</select>
			<?php endif;?>


			<?php if($res_entrega->rowCount() != 0): ?>
			<select class="form-control" id="usr_entrega" name="usr_entrega">

				<?php while($fila = $res_entrega -> fetch(PDO::FETCH_ASSOC)):?>

				<option value = "<?php echo $fila['ID_ENTREGA']?>"><?php echo $fila['NOMBRE'] . " - " . mb_strtoupper($fila['ESPACIO'],'utf-8')?></option>
			
			<?php endwhile; $res_entrega ->closeCursor(); ?>

			</select>
			<br>
			<button type="button" class="btn btn-primary" style="width:150px;" onclick="asignar()" ><span class="glyphicon glyphicon-floppy-saved"></span> Asignar</button>
			<?php endif; $res_entrega->closeCursor(); $conexion=null; ?>
		</form>
	</div>
</div> 