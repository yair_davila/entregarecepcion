<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }

require '../conexion.php';       
?>
<div class="container-fluid"> 
  <h4>Crear nueva entrega - recepción <span class="glyphicon glyphicon-book"></h4>
  <form id="cr_entrega">
  <div class="form-group" id="ver">
    <label>Selecciona la unidad para continuar</label>
          <?php 
            $resultado =  $conexion->prepare("SELECT DISTINCT ID_UNIDAD, NOMBRE FROM CAT_UNIDADES ORDER BY  NOMBRE ASC");  //preparacion de la consulta
            $resultado->execute();
          ?>

    <select class="form-control" name="unidad" id="unidad">
            <?php while($fila = $resultado -> fetch(PDO::FETCH_ASSOC)):?>

      <option value="<?php echo  $fila['ID_UNIDAD']?>"> <?php echo $fila["NOMBRE"] ?></option>
                
                <?php endwhile;  $resultado -> closeCursor();?>
    </select>
    <br>
    <button type="button" class="btn btn-primary" onclick="mostrar()"><span class="glyphicon glyphicon-ok"></span> Seleccionar</button>
  </div>
  
    <div  class="container-fluid" id="oculto" style="display:none;">
      <select class="form-control" disabled id="seleccion" name="seleccion">
        <option></option>
      </select>
      <label>Tipo unidad</label><br>
      <select name="t_unidad" id="t_unidad" class="form-control">
        <option value="" selected>Slecciona una opción...</option>
        <option value="académica">Académica</option>
        <option value="administrativa">Administrativa</option>
      </select>
      <br>      
      <label>Espacio (en su caso)</label><br>
      <input type="text"  class="form-control" name="espacio" id="espacio" placeholder="Departamento" >
      <br>
      <label>Responsable de contraloria</label>
              <?php              
                 $resp_c = $conexion -> prepare("SELECT * FROM RESP_CI ORDER BY NOMBRE ASC");  //preparacion de la consulta
                 $resp_c -> execute();  //ejecución de la consulta
      
              ?>
      <select name="resp_ci" id="resp_ci" class="form-control">
        <?php  while($rci = $resp_c -> fetch(PDO::FETCH_ASSOC)):?>
        <option value="<?php echo $rci['RFC_CI'] ?>"><?php echo $rci['NOMBRE'] ?></option>    
        <?php endwhile;  $resp_c ->closeCursor(); $conexion=null; ?>
      </select>
      <label>Directorio de la entrega</label><br>
      <input type="text"  class="form-control" name="directorio" id="directorio" placeholder="Directorio donde se guardaran los archivos">  
      <br>
      <button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-floppy-saved"></span> Aceptar</button>
    </div>
  </form>
</div>

<script>

  function mostrar(){
    var opcion_seleccionada = $('#unidad option:selected').text();
    $('#ver').hide(); //oculta el primer div
    $('#oculto').show(); //muestra el segundo div
    $("#seleccion option:selected").text(opcion_seleccionada);
  }

  $(document).ready(function () {

  $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });
  
  $('#cr_entrega').validate({
        rules: {
            t_unidad: { required: true, texto:true},
            espacio:{required: true,texto:true},
            directorio:{required:true,texto:true}

        },
        messages: {
            t_unidad: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            espacio:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
            directorio:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
           }
           
            
        },
        submitHandler: function (){
            create_entrega();
        }
    });
  });



  
</script>

