<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  
  include "../conexion.php";

?>
<div class="container-fluid">
	<h4>Edicion de usuarios <span class="glyphicon glyphicon-user"></span></h4>
			<div class="from-group" id="ver">
				<label>Selecciona un usuario para editarlo</label>
				<?php 
				$sql_u="SELECT ID_USUARIO,NOMBRE FROM USUARIOS";
				$res_u= $conexion -> prepare($sql_u);
				$res_u->execute();
				?>
				<select class="form-control"  id="id_usr">
					<?php while($fila = $res_u -> fetch(PDO::FETCH_ASSOC)):?>
					<option value="<?php echo $fila['ID_USUARIO']; ?>"><?php echo $fila['NOMBRE'];?></option>
					<?php endwhile; $res_u -> closeCursor(); $conexion=null; ?> 
				</select>
				<br>
              	<button type="button" class="btn btn-primary" onclick="set_id()"><span class="glyphicon glyphicon-ok"></span> Seleccionar</button>
	
			</div>
			<form id="upd_usr">
			<div class="form-group" id="oculto" style="display:none;">
				<label>Nombre</label>
				<input type="text"  name="nombreU" id="nombreU" class="form-control" required placeholder="Nombre completo"> 
				<br>
				<label>Usuario</label>
				<input type="text"  name="usrU" id="usrU" class="form-control" placeholder="Usuario asignado">
				<br>
				<label>Password</label>
				<input type="password"  name="passU"  id="passU" class="form-control" placeholder="Password">
				<br>
				<label>Tipo de usuario</label>
				<select class="form-control" name="tipo_usrU" id="tipo_usrU">
					<option value="">Selecciona una opcion...</option>
					<option value="1">Capturista</option>
					<option value="2">Responsable de unidad</option>
				</select>
				<br>
				<button type="submit" class="btn btn-primary" ><span class="glyphicon glyphicon-floppy-saved"></span>Actualizar</button>				
			</div>
			</form>
</div>

<script type="text/javascript">
	$(document).ready(function () {

	$.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;%\s])*$/.test(value);
      });
	$.validator.addMethod('passwor',function(value,element){
        return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(value);
      });
	

	$('#upd_usr').validate({
        rules: {
            nombreU: { required: true, texto:true},
            usrU:{required: true,texto:true},
            tipo_usrU:{required:true},
            passU:{required:true,passwor:true}

        },
        messages: {
            nombreU: {
                required: "Completa el campo por favor",
                texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            usrU:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",

            },
            tipo_usrU:{
            	required:"Selecciona una opcion",
            },
            passU:{
            	required: "Completa el campo por favor",
            	passwor:"El password tiene almenos 1 digito,1 mayuscula,1 minúscula, 1 caracter especial y una loguitud mínima de 8 caracteres",
            }
           
            
        },
        submitHandler: function () {
            update_usr();
        }
    });


	});
</script>