<?php
session_start();

 if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location: https://localhost/SerUaz/");
  
  }
require '../conexion.php';

$per=$conexion->prepare("SELECT FOLIO_PERIODO,NOMBRE,ESPACIO,ANEXOS FROM CAT_UNIDADES C JOIN ENTREGA E ON C.ID_UNIDAD=E.ID_UNIDAD JOIN PERIODO P ON E.ID_ENTREGA = P.ID_ENTREGA WHERE FOLIO_PERIODO IN( SELECT MAX(FOLIO_PERIODO) FROM PERIODO GROUP BY ID_ENTREGA)");
$per->execute();
?>

<div class="container-fluid"> 
  <h4>Editar anexos <span class="glyphicon glyphicon-book"></h4>
  <form id="send_anexos"> 
    <div class="form-group" id="show">
      <label>Selecciona una entrega para editar sus anexos correspondientes.</label>
      <select class="form-control" name="per_entr" id="per_entr">
        <?php while($fila = $per -> fetch(PDO::FETCH_ASSOC)):?>
        <option value="<?php echo  $fila['FOLIO_PERIODO']?>"> <?php echo $fila["NOMBRE"]." - ". mb_strtoupper($fila["ESPACIO"],'utf-8')." ANEXOS: ".$fila["ANEXOS"] ?></option>  
        <?php endwhile;  $per -> closeCursor();?>
      </select>
      <br>
      <button type="button" class="btn btn-primary" onclick="view()"><span class="glyphicon glyphicon-ok"></span> Aceptar</button>
    </div>
    <div class="form-group" id="unseen" style="display:none;">
      <label>Descripción de la entrega</label>
      <select class="form-control" disabled id="esay" name="seleccion">
        <option></option>
      </select>
      <label>Periodo</label>
      <input type="text"  class="form-control" name="anexos_per" id="anexos_per" disabled>
      <label>Anexos actualmente aplicables</label>
      <input type="text"  class="form-control" name="anexos_act" id="anexos_act" disabled>
      <label>Anexos aplicables</label>
      <div class="row" style="background-color: #F4F6F7;">
          <div class="col-md-6">
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="1">1. Marco legal y de Actuación</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="2">2. Asuntos en Trámite</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="3">3. Recursos Humanos</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="4">4. Recursos Materiales</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="5">5. Libros, Boletines y Libretos de Consulta</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="6">6. Principales Archivos de Documentos</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="7">7. Recsursos Financieros - Informes</label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="8" >8. Recursos Financieros - Bancos</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="9">9. Corte Documental</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="10">10. Fondo Revolvente</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="11">11. Acuerdos, Convenios y Contratos de Servicios</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="12">12. Actas de Concurso</label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" name="anexos[]" value="13">13. Obras Públicas</label>
            </div>
          </div>
          <label for="anexos[]" class="error">
      </div><br>
      <button type="submit" class="btn btn-primary" style="width:150px;"><span class="glyphicon glyphicon-floppy-saved"></span> Actualizar</button>
    </div>
  </form>
</div>


<script>

function view(){
  seleccion= $('#per_entr option:selected').text();
  $('#show').hide();
  $('#unseen').show();
  $("#esay option:selected").text(seleccion);
  desc_enexos();

}

$(document).ready(function () {
  $('#send_anexos').validate({
    rules:{
      "anexos[]":{required:true}
    },
    messages: {
      "anexos[]":{
              required: "Debes seleccionar como mínimo un anexo",
      }
    },
    submitHandler: function (){
            set_update_anexos();
        }

  });




});



</script>
