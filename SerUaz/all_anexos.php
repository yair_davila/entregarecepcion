<?php
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                                                                                                                             */
/*                                                                                                                                                             */
/*                                               SCRIPT ENCARGADO DE INSERTAR LOS DATOS EN LA BDD                                                           */ 
/*                                                                                                                                                             */
/*                                                                                                                                                             */
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/

session_start();
/**
 * Comprobacion de la sesion de el usuario
 */

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  header("location:index.php");
  }

 require 'conexion.php'; 
 require 'config.php';
 include 'validacion.php'; 
 require __DIR__ . '/vendor/autoload.php';
 use phpseclib\Net\SFTP;

/**
 *LLAMADA A LAS FUNCIONES DE CADA ANEXO. EN DONDE PERIMENRAMENTE SE REALIZA UN FILTRO DE LOS DATOS QUE LLEGAN EN LAS VARIABLES POST SI SE CUMPLE LAS REGLAS  
 *DE LOS FILTROS SE LLAMA LA FUNCIÓN ENCARADA DE INSERTAR LOS DATOS, DESPUÉS SE FORMATEA EL NOMBRE DEL ARCHIVO QUE CORRESPONDE A CADA ANEXO. DEPENDIENDO DEL VALOR DEVUELTO POR LOS FILTROS SERA EL MENSAJE QUE SE MUESTRE CON AJAX  
 *               NOTA: UN CERO 0 = ERROR                                                                                                                      
 *               UN UNO  1 = EXITO                                                                                                                            
 
 */

 switch ($_POST['callfun']) {
   case 'in_a1':
          if(getVal_a1($_POST['objetivo'],$_POST['creacion'],$_POST['legal'])){
            $name=getReset($_FILES['archivo']['name']);
            setA1($_POST['objetivo'],$_POST['creacion'],$_POST['legal'],$name,$_FILES['archivo']['tmp_name'],$_FILES['archivo']['size']);  
          }else{
            sleep(2);
            echo 0;
          }
          
     break;
  case 'in_a2':
          if(getVal_a2($_POST['asunto_a2'],$_POST['unidad_a2'],$_POST['avance_a2'],$_POST['obs_a2'])){
              $name=getReset($_FILES['archivo_a2']['name']);
              setA2($_POST['asunto_a2'],$_POST['unidad_a2'],$_POST['avance_a2'],$_POST['obs_a2'],$name,$_FILES['archivo_a2']['tmp_name'],$_FILES['archivo_a2']['size']);            
          }else {
            sleep(2);
            echo 0;
          }

      break;
  case 'in_a3':
        if(getVal_a3($_POST['t_nomina'],$_POST['f_corte'],$_POST['obs_a3'])){
          $name=getReset($_FILES['archivo_a3']['name']);
          setA3($_POST['t_nomina'],$_POST['f_corte'],$_POST['obs_a3'],$name,$_FILES['archivo_a3']['tmp_name'],$_FILES['archivo_a3']['size']);
        }else{
          sleep(2);
          echo 0;
        }
        # code...
    break;
  case 'in_a4':
        if(getVal_a4($_POST['inventario'],$_POST['obs_a4'],$_POST['f_cortea4'])){
          $name=getReset($_FILES['archivo_a4']['name']);
          setA4($_POST['inventario'],$_POST['obs_a4'],$_POST['f_cortea4'],$name,$_FILES['archivo_a4']['tmp_name'],$_FILES['archivo_a4']['size']);
        }else{
          sleep(2);
          echo 0;
        }
    break;
  case 'in_a5':
          if(getVal_a5($_POST['cantidad'],$_POST['des_a5'],$_POST['ubicacion'],$_POST['obs_a5'])){
            $name=getReset($_FILES['archivo_a5']['name']);
            setA5($_POST['cantidad'],$_POST['des_a5'],$_POST['ubicacion'],$_POST['obs_a5'],$name,$_FILES['archivo_a5']['tmp_name'],$_FILES['archivo_a5']['size']);
        }else{
          sleep(2);
          echo 0 ;
        }
    break;
  case 'in_a6':
          if(getVal_a6($_POST['area'],$_POST['tipo_doc'],$_POST['tipo_archivo'],$_POST['documento'])){
            $name=getReset($_FILES['archivo_a6']['name']);
            setA6($_POST['area'],$_POST['tipo_doc'],$_POST['tipo_archivo'],$_POST['documento'],$name,$_FILES['archivo_a6']['tmp_name'],$_FILES['archivo_a6']['size']);
          }else{
            sleep(2);
            echo 0;
          }
    break;
   case 'in_a7':
          if(getVal_a7($_POST['tipo_info'],$_POST['repo_info'],$_POST['f_doc'],$_POST['obs_a7'])){
            $name=getReset($_FILES['archivo_a7']['name']);
            setA7($_POST['tipo_info'],$_POST['repo_info'],$_POST['f_doc'],$_POST['obs_a7'],$name,$_FILES['archivo_a7']['tmp_name'],$_FILES['archivo_a7']['size']);
          }else{
            sleep(2);
            echo 0;
          }
    break;
   case 'in_a8':
          if(getVal_a8($_POST['t_cuenta'],$_POST['numero_cuenta'],$_POST['bancaria'],$_POST['saldo'],$_POST['con_banco'],$_POST['obs_a8'])){
            $name=getReset($_FILES['archivo_a8']['name']);
            setA8($_POST['t_cuenta'],$_POST['numero_cuenta'],$_POST['bancaria'],$_POST['saldo'],$_POST['con_banco'],$_POST['obs_a8'],$name,$_FILES['archivo_a8']['tmp_name'],$_FILES['archivo_a8']['size']);
          }else{
            sleep(2);
            echo 0;
          }
    break;
  case 'in_a9':
          if(getVal_a9($_POST['tipo_docu'],$_POST['num_cuenta'],$_POST['institucion'],$_POST['folio_f'],$_POST['folio_n'],$_POST['obs_a9'])){
            $name=getReset($_FILES['archivo_a9']['name']);
            setA9($_POST['tipo_docu'],$_POST['num_cuenta'],$_POST['institucion'],$_POST['folio_f'],$_POST['folio_n'],$_POST['obs_a9'],$name,$_FILES['archivo_a9']['tmp_name'],$_FILES['archivo_a9']['size']);
          }else{
            sleep(2);
            echo 0 ;
          }
    break;
   case 'in_a10':
         if(getVal_a10($_POST['fecha_info'],$_POST['f_autorizado'],$_POST['fondo_caja'],$_POST['documentos'],$_POST['gastos_p'])){
            $name=getReset($_FILES['archivo_a10']['name']);
            setA10($_POST['fecha_info'],$_POST['f_autorizado'],$_POST['fondo_caja'],$_POST['gastos_p'],$_POST['documentos'],$name,$_FILES['archivo_a10']['tmp_name'],$_FILES['archivo_a10']['size']);
        }else {
          sleep(2);
          echo 0;
        }
      break;
    case 'in_a11':
          if(getVal_a11($_POST['num_doc'],$_POST['des_a11'],$_POST['importe'],$_POST['fecha_i'],$_POST['fecha_f'],$_POST['obs_a11'])){
            $name=getReset($_FILES['archivo_a11']['name']);
            setA11($_POST['num_doc'],$_POST['des_a11'],$_POST['importe'],$_POST['fecha_i'],$_POST['fecha_f'],$_POST['obs_a11'],$_FILES['archivo_a11']['name'],$_FILES['archivo_a11']['tmp_name'],$_FILES['archivo_a11']['size']);
          }else {
            sleep(2);
            echo 0;
          }

     break;
    case 'in_a12':
          if(getVal_a12($_POST['fecha_a'],$_POST['fecha_fallo'],$_POST['n_proyecto'],$_POST['unidad'],$_POST['tipo_concurso'],$_POST['origen_concurso'],$_POST['empresa_ganadora'],$_POST['obs_a12'])){
            $name=getReset($_FILES['archivo_a12']['name']);
            setA12($_POST['fecha_a'],$_POST['fecha_fallo'],$_POST['n_proyecto'],$_POST['unidad'],$_POST['tipo_concurso'],$_POST['origen_concurso'],$_POST['empresa_ganadora'],$_POST['obs_a12'],$name,$_FILES['archivo_a12']['tmp_name'],$_FILES['archivo_a12']['size']);
          }else{
            sleep(2);
            echo 0;
          }
      break;
    case 'in_a13':
          if(getVal_a13($_POST['des_a13'],$_POST['ubicacion'],$_POST['monto_a13'],$_POST['monto_ejercido'],$_POST['monto_ejercer'],$_POST['a_fisico'],$_POST['avance_fin'],$_POST['obs_a13'])){
            $name=getReset($_FILES['archivo_a13']['name']);
            setA13($_POST['des_a13'],$_POST['ubicacion'],$_POST['monto_a13'],$_POST['monto_ejercido'],$_POST['monto_ejercer'],$_POST['a_fisico'],$_POST['avance_fin'],$_POST['obs_a13'],$name,$_FILES['archivo_a13']['tmp_name'],$_FILES['archivo_a13']['size']);
          }else {
            sleep(2);
            echo 0;
          }
      break;
   default:
   sleep(2);
   echo 0;
     break;
 }
/*****************************************************************************************************************************************************************
 * 
 * FUNCIONES PARA INSERTAR EN LOS ANEXOS, EN DONDE PRIMERAMENTE SE REALIZA LA LLAMADA A LA FUNCION "setfile" ENCARGADA DE INSERTAR EL ARCHIVO CORRESPONDIENTE A CADA ANEXO EN EL SERVIDOR FTP. SI SE REALIZA CON EXITO LA INSERCIÓN DEL ARCHIVO SE OBTINE UN STRING CON EL URL DE LA DIRECCION DEL ARCHIVO. SI TODO ESTO ES CORRECTO SE PROCEDE A REALIZAR LA INSERCION EN LA BASE DE DATOS Y EN LA SECCIO DE LA BITACORA DE TRABAJO. HAY DOS POSILES MENSAJES QUE SE PUEDEN OBTENER DE UNA INSERCION DE UN REGISTRO EN LA BASE DE DATOS ERROR = 0 Ó EXITO = 1. SE ASIGNA UN TIEMPO DE 2 SEG PARA LA PARTE GRÁFICA DE LOS MENSAJES MOSTRADOS AL USUARIO. 
 *
 *
 ****************************************************************************************************************************************************************/

/**
 * Descripción: función para insertar en el anexo 1
 * @param [string]
 * @param [date]
 * @param [string]
 * @param [file]
 * @param [file]
 * @param [file]
 */

function setA1($obj,$f_cr,$decr,$arc_local,$arc_temp,$size){
  global $conexion;
  $url = setFile($arc_local,$arc_temp,$size);
  if(!is_numeric($url)){
        $res_an1 = $conexion -> prepare("INSERT INTO ANEXO1 (ID_ENTREGA,FOLIO_PERIODO,OBJETIVO,CREACION,LEGAL,ENLACE,F_CAPTURA) VALUES(:id,:fp,:ob,:cr,:le,:enlace,:fc)");
        $res_an1 -> execute(array(":id"=>$_SESSION['id_entrega'], ":fp"=>$_SESSION['periodo'], ":ob"=>$obj=resetString($obj), ":cr"=>$f_cr, ":le"=>$decr=resetString($decr), ":enlace"=>$url, ":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 1");
        $res_an1 -> closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo $url;
    }
  $conexion = null;
}

/**
 * Descripcion: función para insertar en el anexo 2
 * @param [string]
 * @param [string]
 * @param [string]
 * @param [string]
 * @param [file]
 * @param [file]
 * @param [file]
 */
function setA2($asunto,$unidad,$avance,$obs,$arc_local,$arc_temp,$size){
  global $conexion;
  $url = setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a2 = $conexion -> prepare("INSERT INTO ANEXO2 (ID_ENTREGA,FOLIO_PERIODO,ASUNTO,UNIDAD_TRAMITE,AVANCE,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES(:id,:per,:asu, :uni, :ava, :obs, :en,:fe)");  
        $res_a2 -> execute(array("id"=>$_SESSION['id_entrega'],"per"=>$_SESSION['periodo'],":asu"=>$asunto=resetString($asunto),":uni"=>$unidad=resetString($unidad),":ava"=>$avance=resetString($avance),":obs"=>$obs=resetString($obs),":en"=>$url,":fe"=>$_SESSION['fecha_captura'])); 
        Bitacora("Anexo 2");
        $res_a2->closeCursor();
        sleep(2);
        echo 1;         
    }else {
        sleep(2);
        echo $url;
    }
  $conexion = null ;
}
/**
 * @param [string]
 * @param [string]
 * @param [string]
 * @param [file]
 * @param [file]
 * @param [file]
 */
function setA3($nomina,$corte,$obse,$arc_local,$arc_temp,$size){
  global $conexion; 
  $url = setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $sql_a3 = "INSERT INTO ANEXO3 (ID_ENTREGA,FOLIO_PERIODO,NOMINA,CORTE,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES(:id,:fp,:no,:co,:ob,:en,:fc)";
        $res_a3 = $conexion->prepare($sql_a3);
        $res_a3->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":no"=>$nomina,":co"=>$corte,":ob"=>$obse=resetString($obse),":en"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 3");
        $res_a3 -> closeCursor();
        sleep(2);
        echo 1;
    }else {
        sleep(2);
        echo $url;        
    }
  $conexion = null;
}
/* función que inserta en anexo 4 */
function setA4($inv,$obs_a4,$fc_a4,$arc_local,$arc_temp,$size){
  global $conexion;
    switch($inv){
      case '1':
      	 	$inv="Bienes, muebles, informáticos";
      	break;
      case '2':
      		$inv="Vehiculos y transportes";
      	break;
      case '3':
    			$inv="Bienes e inmuebles";
      	break;
      case '4':
      		$inv="Existencia en almacen";
      	break;
      case '5':
     		   $inv="Relacion de bajas";
      	break;
      }
  $url = setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){ 
        $sql_a4 = "INSERT INTO ANEXO4 (ID_ENTREGA,FOLIO_PERIODO,INVENTARIO,FECHA,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES(:id,:fp,:inv,:f,:ob,:en,:fc)";
        $res_a4 = $conexion-> prepare($sql_a4);
        $res_a4->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":inv"=>$inv,":f"=>$fc_a4,":ob"=>$obs_a4=resetString($obs_a4),":en"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 4");
        $res_a4->closeCursor();
        sleep(2);
        echo 1;
      }else{
        sleep(2);
        echo  $url;
      }  
  $conexion = null;
}

/* función que inserta en anexo 5*/
function setA5($cantidad,$des_a5,$ubi,$obs_a5,$arc_local,$arc_temp,$size){
  global $conexion;
  $url= setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a5 = $conexion -> prepare("INSERT INTO ANEXO5(ID_ENTREGA,FOLIO_PERIODO,CANTIDAD,DESCRIPCION,UBICACION,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES (:id,:fp,:ca,:des,:ub,:ob,:en,:fc)");
        $res_a5 ->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":ca"=>$cantidad,":des"=>$des_a5=resetString($des_a5),":ub"=>$ubi,":ob"=>$obs_a5=resetString($obs_a5),":en"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 5");
        $res_a5->closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo $url; 
    }
  $conexion = null ;   
}

/* función que inserta en anexo 6*/
function setA6($area,$tdoc,$tarch,$doc,$arc_local,$arc_temp,$size){
  global $conexion;
  $url =setFile($arc_local,$arc_temp,$size); 
    if($tdoc == "1"){
        $tdoc = "Archivos de documentos";
    }else{
        $tdoc="Archivos electrónicos";
    }
    if(!is_numeric($url)){
        $res_a6= $conexion->prepare("INSERT INTO ANEXO6 (ID_ENTREGA,FOLIO_PERIODO,AREA_RESGUARDA,TIPO_DOC,TIPO_ARCHIVO,CONTENIDO,ENLACE,F_CAPTURA) VALUES(:id,:fp,:ar,:td,:ta,:con,:en,:fc)");
        $res_a6->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":ar"=>$area,":td"=>$tdoc,":ta"=>$tarch,":con"=>$doc=resetString($doc),":en"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 6");
        $res_a6 ->closeCursor();
        sleep(2);
        echo 1;
    }else {
        sleep(2);
        echo $url;
    }
  $conexion = null;
}
/* función que inserta en anexo 7*/
function setA7($tinf,$rfin,$fe_info,$obs,$arc_local,$arc_temp,$size){
  global $conexion;
    switch($tinf){
      case '1':
        $tinf="Presupuesto";
        break;
      case '2':
        $tinf="Estado de situación financiera";
        break;
      case '3':
        $tinf="Estado de actividades";
        break;
      case '4':
        $tinf="Balanza de comprobación";
        break;
      case '5':
        $tinf="Deudores diversos";
        break;
      case '6':
        $tinf="Acredores diversos";
        break;
      case '7':
        $tinf="Proveedores";
        break;
      case '8':
           $tinf="Otros";
        break; 
      }
  $url =setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a7=$conexion->prepare("INSERT INTO ANEXO7 (ID_ENTREGA,FOLIO_PERIODO,TIPO_INFORME,REPORTE_FINANCIERO,FECHA_INFORME,OBSERVACIONES,ENLACE,F_CAPTURA)  VALUES (:id,:fp,:ti,:rf,:fi,:ob,:en,:fc)");
        $res_a7->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":ti"=>$tinf,":rf"=>$rfin,":fi"=>$fe_info,":ob"=>$obs=resetString($obs),":en"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 7");
        $res_a7->closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo 0;
    }
  $conexion= null;
}
/* función que inserta en anexo 8*/
function setA8($t_cuenta,$num_cuenta,$bancaria,$saldo,$con_banco,$obs_a8,$arc_local,$arc_temp,$size){
  global $conexion;
  $url =setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a8 = $conexion->prepare("INSERT INTO ANEXO8 (ID_ENTREGA,FOLIO_PERIODO,T_CUENTA,NO_CUENTA,INSTITUCION,SALDO,CONCILIACION,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES (:id,:fp,:tc,:nc,:ban,:sal,:cb,:obs,:enlace,:fc)");
        $res_a8->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":tc"=>$t_cuenta,":nc"=>$num_cuenta,":ban"=>$bancaria,":sal"=>$saldo,":cb"=>$con_banco,":obs"=>$obs_a8=resetString($obs_a8),":enlace"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 8");
        $res_a8->closeCursor();
        sleep(2);
        echo 1;
    }else {
        sleep(2);
        echo $url;
    }    
  $conexion= null;
}
/* función que inserta en anexo 9*/
function setA9($tdoc,$num_cuenta,$inst,$fdf,$fdi,$obs_a9,$arc_local,$arc_temp,$size){
  global $conexion;
    switch ($tdoc) {
      case '1':
        $tdoc="Chequera";
        break;
      case '2':
        $tdoc="Recibos oficiales internos";
        break;
      case '3':
          $tdoc="Póliza de diario";
        break;
      case '4';
          $tdoc="Póliza de ingresos";
        break;
      case '5':
          $tdoc="Póliza de egresos";
          break;
      case '6':
          $tdoc="Otros";
          break;
      }
  $url = setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a9=$conexion->prepare("INSERT INTO ANEXO9(ID_ENTREGA,FOLIO_PERIODO,TIPO_DOCUMENTO,NUMERO_CUENTA,INSTITUCION,N_FINAL,N_INICIAL,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES (:id,:fp,:td,:nc,:ins,:nf,:ni,:obs,:enlace,:fc)");
        $res_a9->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":td"=>$tdoc,":nc"=>$num_cuenta,":ins"=>$inst,":nf"=>$fdf,":ni"=>$fdi,":obs"=>$obs_a9=resetString($obs_a9),":enlace"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 9");
        $res_a9->closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo $url;
    }
  $conexion=null;
}
/* función que inserta en anexo 10*/
function setA10($fecha_info,$fondo,$caja,$pendientes,$doc_pag,$arc_local,$arc_temp,$size){
  global $conexion;
  $url = setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a10 = $conexion->prepare("INSERT INTO ANEXO10 (ID_ENTREGA,FOLIO_PERIODO,FECHA_INFORME,MONTO_AUTORIZADO,MONTO_DISPONIBLE,GASTOS_PENDIENTES,DOCUMENTOS_PAGADOS,ENLACE,F_CAPTURA)VALUES(:id,:fp,:finfo,:ma,:md,:pen,:doc_pag,:enlace,:fc)");
        $res_a10->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":finfo"=>$fecha_info,":ma"=>$fondo,":md"=>$caja,":pen"=>$pendientes,":doc_pag"=>$doc_pag,":enlace"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 10");
        $res_a10->closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo $url;
    }
  $conexion=null;
}
/* función que inserta en anexo 11*/
function setA11($num_doc,$des_a11,$importe,$fecha_i,$fecha_f,$obs_a11,$arc_local,$arc_temp,$size){
  global $conexion;
  $url = setFile($arc_local,$arc_temp,$size);
    if(!is_numeric($url)){
        $res_a11=$conexion->prepare("INSERT INTO ANEXO11 (ID_ENTREGA,FOLIO_PERIODO,NO_DOC,DESC_DOC,IMPORTE,F_INICIO,F_TERMINO,OBSERVACIONES,ENLACE,F_CAPTURA)VALUES(:id,:fp,:doc,:des,:imp,:fi,:ft,:obs,:enlace,:fc)");
        $res_a11->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":doc"=>$num_doc,":des"=>$des_a11=resetString($des_a11),":imp"=>$importe,":fi"=>$fecha_i,":ft"=>$fecha_f,":obs"=>$obs_a11=resetString($obs_a11),":enlace"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 11");
        $res_a11->closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo $url;
    }
  $conexion=null;
}
/* función que inserta en anexo 12*/
function setA12($fecha_a,$fecha_f,$nombre_p,$unidad,$tipo_c,$origen_c,$empresa_g,$obs_a12,$arc_local,$arc_temp,$size){
  global $conexion;
  $url = setFile($arc_local,$arc_temp,$size);
    switch ($tipo_c) {
      case '1':
          $tipo_c="Adjudicación directa";
        break;
      case '2':
          $tipo_c="Cuadro corporativo";
        break;
      case '3':
            $tipo_c="Investigación restringida a tres proveedores";
        # code...
        break;
      case '4':
            $tipo_c="Licitación";
        # code...
        break;
    }

  if(!is_numeric($url)){
      $res_a12=$conexion->prepare("INSERT INTO ANEXO12 (ID_ENTREGA,FOLIO_PERIODO,F_APERTURA,F_FALLO,PROYECTO,UNIDAD,TIPO_CONCURSO,ORIGEN_RECURSO,EMPRESA_GANADORA,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES(:id,:fp,:fa,:ff,:pr,:un,:tc,:oc,:eg,:obs,:enlace,:fc)");
      $res_a12->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":fa"=>$fecha_a,":ff"=>$fecha_f,":pr"=>$nombre_p,":un"=>$unidad,":tc"=>$tipo_c,":oc"=>$origen_c=resetString($origen_c),":eg"=>$empresa_g=resetString($empresa_g),":obs"=>$obs_a12=resetString($obs_a12),":enlace"=>$url,":fc"=>$_SESSION['fecha_captura']));
      Bitacora("Anexo 12");
      $res_a12->closeCursor();
      sleep(2);
      echo 1;
  }else {
      sleep(2);
      echo $url;
  }
  $conexion=null;
}
/* función que inserta en anexo 13*/
function setA13($des_a13,$ubicacion,$monto_a13,$monto_e,$monto_ej,$avance_f,$avance_fis,$obs_a13,$arc_local,$arc_temp,$size){
  global $conexion;
  $url = setFile($arc_local,$arc_temp,$size);

    if(!is_numeric($url)){
        $res_a13=$conexion->prepare("INSERT INTO ANEXO13 (ID_ENTREGA,FOLIO_PERIODO,DESC_OBRA,UBICACION,MONTO_AUTORIZA,MONTO_EJERCIDO,MONTO_EJERCER,AVANCE_FISICO,AVANCE_FINANCIERO,OBSERVACIONES,ENLACE,F_CAPTURA) VALUES (:id,:fp,:des,:ub,:ma,:me,:mej,:af,:afi,:obs,:enlace,:fc)");
        $res_a13->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":des"=>$des_a13=resetString($des_a13),":ub"=>$ubicacion=resetString($ubicacion),":ma"=>$monto_a13,":me"=>$monto_e,":mej"=>$monto_ej,":af"=>$avance_f,":afi"=>$avance_fis,":obs"=>$obs_a13=resetString($obs_a13),":enlace"=>$url,":fc"=>$_SESSION['fecha_captura']));
        Bitacora("Anexo 13");
        $res_a13->closeCursor();
        sleep(2);
        echo 1;
    }else{
        sleep(2);
        echo $url;
    }
  $conexion=null;
}

/**
 * @param [file]
 * @param [file]
 * @param [file]
 * @return [string]
 */
function setFile($local,$temp,$tam){

  $sftp = new SFTP(SFTP_SERVER);

    if($sftp->login(SFTP_USER,SFTP_PASSWORD))
    {
      $sftp->chdir('files');
      $sftp->chdir($_SESSION['directorio']);
      $dir_entrega = $sftp->pwd();

      $info = new SplFileInfo($local);
      $tipo_file=$info->getExtension();
      $url="";

      if($tipo_file == "pdf" || $tipo_file == "xml" || $tipo_file == "xls" || $tipo_file == "ods" || $tipo_file == "xlsx" || $tipo_file == "jpg" || $tipo_file == "doc" || $tipo_file == "docx")
      {
        if(!file_exists("sftpseruaz".$dir_entrega."/".$local))
        {
          if($tam < 80000000)
          {
            if($sftp->put($local,$temp,SFTP::SOURCE_LOCAL_FILE))
            {
              $url = "https://" . SFTP_SERVER . "/SerUaz/sftpseruaz". $dir_entrega . "/" . $local;
              return $url;
            }return $url = 0;//error de servidor
          }return $url=4;//limete de tamaño de archivo superado
        }return $url=3;//archivo exixtente en el servidor
      }return $url=2;//2 = a extencion de archivo no permitida

    }
}

function getReset($f_local){
  
  $subs= array('Á'=>'A','Ç'=>'C','É'=>'E','Í'=>'I','Ñ'=>'N','Ó'=>'O','Ú'=>'U',
             'á'=>'a','é'=>'e', 'í'=>'i','ñ'=>'n','ó'=>'o','ú'=>'u'
            ,'?'=>'_','-'=>'_','+'=>'_','/'=>'_','*'=>'_',';'=>'_',':'=>'_','@'=>'_','!'=>'_','¿'=>'_','¡'=>'_',' '=>'_');
  $y=0;
  while ($y<strlen($f_local)){
      if($f_local[$y]==" "){
          $y++;
      }else{
          $reset[$y]=$f_local[$y];
          $y++;
      }
  }
  $res= implode($reset);
  return strtr($res,$subs);
}

function Bitacora($anexo){
  global $conexion;
  $evento="Insertar";
  $bit=$conexion->prepare("INSERT INTO BITACORA(ANEMOX_M,ANEXO,EVENTO,USUARIO,F_EVENTO) VALUES(:anemox,:anexo,:evento,:usuario,:fecha)");
  $bit->execute(array(":anemox"=>$_SESSION['id_entrega'], ":anexo"=>$anexo, ":evento"=>$evento, ":usuario"=>$_SESSION['nombre'],":fecha"=>$_SESSION['fecha_captura'])); 
}

?>