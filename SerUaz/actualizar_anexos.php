<?php
session_start();

  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }

  require 'conexion.php';
  include 'validacion.php';
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                                                                                                                             */
/*                                                                                                                                                             */
/*                                                 LLAMADA A LAS FUNCIONES PARA INSERTAR EN LOS ANEXOS                                                         */ 
/*                                                                                                                                                             */
/*                                                                                                                                                             */
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  switch ($_POST['accion']) {
    case 'upd_a1':
          if(getVal_a1($_POST['objA1'],$_POST['feA1'],$_POST['decA1']) and (getVal_folio($_POST['folio']))){
             updA1($_POST['objA1'],$_POST['feA1'],$_POST['decA1'],$_POST['folio']);
          }else
            echo 0;
      break;
    case 'upd_a2':
          if(getVal_a2($_POST['asunto_a2u'],$_POST['unidad_a2u'],$_POST['avance_a2u'],$_POST['obs_a2u']) and (getVal_folio($_POST['folio_a2'] ))){
             updA2($_POST['asunto_a2u'],$_POST['unidad_a2u'],$_POST['avance_a2u'],$_POST['obs_a2u'],$_POST['folio_a2']);
          }else
            echo 0;
      break;
    case 'upd_a3':
          if(getVal_a3($_POST['t_nominau'],$_POST['f_corteu'],$_POST['obs_a3u']) and (getVal_folio($_POST['folio_a3'] ))){
              updA3($_POST['t_nominau'],$_POST['f_corteu'],$_POST['obs_a3u'],$_POST['folio_a3']);
          }else
          echo 0;
          
      break;
    case 'upd_a4':
          if(getVal_a4($_POST['inv_a4u'],$_POST['obs_a4u'],$_POST['f_cortea4u']) and (getVal_folio($_POST['folio_a4'] ))){
                updA4($_POST['inv_a4u'],$_POST['obs_a4u'],$_POST['f_cortea4u'],$_POST['folio_a4']);  
          }else 
              echo 0;
      break;
    case 'upd_a5':
          if(getval_a5($_POST['can_a5u'],$_POST['des_a5u'],$_POST['ubi_a5u'],$_POST['obs_a5u']) and (getVal_folio($_POST['folio_a5'] )) ){
             updA5($_POST['can_a5u'],$_POST['des_a5u'],$_POST['ubi_a5u'],$_POST['obs_a5u'],$_POST['folio_a5']);
          }else
            echo 0 ;
      break;
    case 'upd_a6':
          if(getval_a6($_POST['area_a6u'],$_POST['tdoc_a6u'],$_POST['tarc_a6u'],$_POST['doc_a6u']) and (getVal_folio($_POST['folio_a6'] ))){
              updA6($_POST['area_a6u'],$_POST['tdoc_a6u'],$_POST['tarc_a6u'],$_POST['doc_a6u'],$_POST['folio_a6']);
          }else 
            echo 0;
          
      break;
    case 'upd_a7':
      if (getVal_a7($_POST['tipi_a7u'],$_POST['repi_a7u'],$_POST['fdoc_a7u'],$_POST['obs_a7u']) and (getVal_folio($_POST['folio_a7'] ))){
            updA7($_POST['tipi_a7u'],$_POST['repi_a7u'],$_POST['fdoc_a7u'],$_POST['obs_a7u'],$_POST['folio_a7']);
        }else{
          echo 0;
        }
      break;
    case 'upd_a8':
        if(getVal_a8($_POST['tcu_a8u'],$_POST['ncu_a8u'],$_POST['iba_a8u'],$_POST['sal_a8u'],$_POST['cban_a8u'],$_POST['obs_a8u']) and (getVal_folio($_POST['folio_a8'] ))){
          updA8($_POST['tcu_a8u'],$_POST['ncu_a8u'],$_POST['iba_a8u'],$_POST['sal_a8u'],$_POST['cban_a8u'],$_POST['obs_a8u'],$_POST['folio_a8']);

        }else{
          echo 0;
        }
      break;
    case 'upd_a9':
        if(getVal_a9($_POST['tdoc_a9u'],$_POST['ncu_a9u'],$_POST['iba_a9u'],$_POST['foud_a9u'],$_POST['foid_a9u'],$_POST['obs_a9u']) and (getVal_folio($_POST['folio_a9'] ))){
            updA9($_POST['tdoc_a9u'],$_POST['ncu_a9u'],$_POST['iba_a9u'],$_POST['foud_a9u'],$_POST['foid_a9u'],$_POST['obs_a9u'],$_POST['folio_a9']);
        }else{
          echo 0;
        }
      break;
    case 'upd_a10':
          if(getVal_a10($_POST['fin_a10u'],$_POST['fona_a10u'],$_POST['fonc_a10u'],$_POST['doc_a10u'],$_POST['gas_a10u']) and (getVal_folio($_POST['folio_a10'] ))){
              updA10($_POST['fin_a10u'],$_POST['fona_a10u'],$_POST['fonc_a10u'],$_POST['doc_a10u'],$_POST['gas_a10u'],$_POST['folio_a10']);
          }else{
            echo 0;
          }
       break;
    case 'upd_a11':
        if(getVal_a11($_POST['ndoc_a11u'],$_POST['des_a11u'],$_POST['imp_a11u'],$_POST['fi_a11u'],$_POST['ft_a11u'],$_POST['obs_a11u']) and (getVal_folio($_POST['folio_a11']))){
            updA11($_POST['ndoc_a11u'],$_POST['des_a11u'],$_POST['imp_a11u'],$_POST['fi_a11u'],$_POST['ft_a11u'],$_POST['obs_a11u'],$_POST['folio_a11']);
        }else {
          echo 0;
        }
      break;
    case 'upd_a12':
          if(getVal_a12($_POST['fape_a12u'],$_POST['ffa_a12u'],$_POST['npro_a12u'],$_POST['uni_a12u'],$_POST['tcon_a12u'],$_POST['oricon_a12u'],$_POST['empg_a12u'],$_POST['obs_a12u']) and (getVal_folio($_POST['folio_a12'])) ){
              updA12($_POST['fape_a12u'],$_POST['ffa_a12u'],$_POST['npro_a12u'],$_POST['uni_a12u'],$_POST['tcon_a12u'],$_POST['oricon_a12u'],$_POST['empg_a12u'],$_POST['obs_a12u'],$_POST['folio_a12']);

          }else{
            echo 0;
          }
        break;
    case 'upd_a13':
              if(getVal_a13($_POST['des_a13u'],$_POST['ubi_a13u'],$_POST['mau_a13u'],$_POST['mej_a13u'],$_POST['mpe_a13u'],$_POST['avaf_a13u'],$_POST['avafin_a13u'],$_POST['obs_a13u']) and (getVal_folio($_POST['folio_a13']))) {
                  updA13($_POST['des_a13u'],$_POST['ubi_a13u'],$_POST['mau_a13u'],$_POST['mej_a13u'],$_POST['mpe_a13u'],$_POST['avaf_a13u'],$_POST['avafin_a13u'],$_POST['obs_a13u'],$_POST['folio_a13']);
                }else{
                  echo 0;
                }
        break;  
    default:
    echo 0;
      break;
  }

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                                                                                                                              */
/*                                                                                                                                                              */
/*                                                       Funciones para actualizar en los anexos                                                                */
/*                                                                                                                                                              */
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*funcion que actualiza el anexo 1*/
function updA1($obj,$f_cr,$decr,$num){
  global $conexion;
  $res_an1 = $conexion -> prepare("UPDATE ANEXO1 SET ID_ENTREGA = :id ,FOLIO_PERIODO =:fp ,OBJETIVO = :ob ,CREACION = :cr ,LEGAL = :le ,F_CAPTURA= :fc WHERE FOLIOA1 = :n_a");
  $res_an1 -> execute(array(":id"=>$_SESSION['id_entrega'], ":fp"=>$_SESSION['periodo'], ":ob"=>$obj=resetString($obj), ":cr"=>$f_cr, ":le"=>$decr=resetString($decr), ":fc"=>$_SESSION['fecha_captura'],":n_a"=>$num));

    if($res_an1){
        Bitacora("Anexo 1");
        echo 1;
    }else{
        echo 0;
    }
  $res_an1 -> closeCursor();
  $conexion = null ;

}

/*función que actualiza el anexo 2*/
function updA2($asunto,$unidad,$avance,$obs,$n_a2){
  global $conexion;
  $res_a2 = $conexion -> prepare("UPDATE ANEXO2 SET ID_ENTREGA = :id, FOLIO_PERIODO = :fp, ASUNTO = :asu, UNIDAD_TRAMITE = :uni, AVANCE = :ava, OBSERVACIONES = :obs, F_CAPTURA =:fe WHERE FOLIOA2 = :n_a2");
          
  $res_a2 -> execute(array("id"=>$_SESSION['id_entrega'],"fp"=>$_SESSION['periodo'],":asu"=>$asunto,":uni"=>$unidad,":ava"=>$avance,":obs"=>$obs,":fe"=>$_SESSION['fecha_captura'],"n_a2"=>$n_a2)); 
    if($res_a2){
        Bitacora("Anexo 2");
        echo 1;
    }else {
      echo 0;
    }

  $res_a2->closeCursor();
  $conexion = null ;
}
/* función que actualiza el anexo 3*/
function updA3($nomina,$corte,$obse,$num_a){
  global $conexion;
  $res_a3 = $conexion->prepare("UPDATE ANEXO3 SET ID_ENTREGA =:id, FOLIO_PERIODO=:fp, NOMINA=:nom, CORTE=:co, OBSERVACIONES =:ob, F_CAPTURA =:fc WHERE FOLIOA3 = :n_a3");
  $res_a3->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":nom"=>$nomina,":co"=>$corte,":ob"=>$obse,":fc"=>$_SESSION['fecha_captura'],":n_a3"=>$num_a));
    if($res_a3){
        Bitacora("Anexo 3");
        echo 1;
    }else {
       	 echo 0;
    }
  $res_a3->closeCursor();
  $conexion = null ;
}
/* función que actualiza el anexo 4*/
function updA4($inv,$obs_a4,$fc_a4,$num_a){
  global $conexion;
  switch ($inv) {
    case '1':
    	 	$inv="Bienes, muebles, informáticos";
    	break;
    case '2':
    		$inv="Vehiculos y transportes";
    	break;
    case '3':
      	$inv="Bienes e inmuebles";
      break;
    case '4':
      	$inv="Existencia en almacen";
      break;
    case '5':
     		$inv="Relacion de bajas";
      break;
      }
      
  $res_a4 = $conexion-> prepare("UPDATE ANEXO4 SET ID_ENTREGA=:id, FOLIO_PERIODO=:fp, INVENTARIO=:inv, FECHA=:f, OBSERVACIONES=:ob, F_CAPTURA=:fc WHERE FOLIOA4 = :num_a");
  $res_a4->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":inv"=>$inv,":f"=>$fc_a4,":ob"=>$obs_a4,":fc"=>$_SESSION['fecha_captura'],":num_a"=>$num_a));  
    if($res_a4){
        Bitacora("Anexo 4");
        echo 1;
    }else{
        echo 0;
    }
  $res_a4->closeCursor();
  $conexion = null;
}

/* función que actualiza el anexo 5*/
function updA5($cantidad,$des_a5,$ubi,$obs_a5,$num_a){
  global $conexion;  
  $res_a5 = $conexion -> prepare("UPDATE ANEXO5 SET ID_ENTREGA=:id, FOLIO_PERIODO=:fp, CANTIDAD=:ca, DESCRIPCION=:des, UBICACION=:ub, OBSERVACIONES=:ob,F_CAPTURA=:fc WHERE FOLIOA5 = :folio");
  $res_a5 ->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":ca"=>$cantidad,":des"=>$des_a5,":ub"=>$ubi,":ob"=>$obs_a5,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a5){
        Bitacora("Anexo 5");
        echo 1;
    }else{
        echo 0;
    }
  $res_a5->closeCursor();
  $conexion = null; 
}

/* función que actualiza el anexo 6*/
function updA6($area,$tdoc,$tarch,$doc,$num_a){
  global $conexion;
  
  if($tdoc == "1"){
      $tdoc = "Archivos de documentos";
  }else{
      $tdoc="Archivos electrónicos";
  }

  $res_a6= $conexion->prepare("UPDATE ANEXO6 SET ID_ENTREGA=:id, FOLIO_PERIODO=:fp, AREA_RESGUARDA=:ar, TIPO_DOC=:td, TIPO_ARCHIVO=:ta,CONTENIDO=:con,F_CAPTURA=:fc WHERE FOLIOA6 = :folio");
  $res_a6->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":ar"=>$area,":td"=>$tdoc,":ta"=>$tarch,":con"=>$doc,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a6){
        Bitacora("Anexo 6");
        echo 1;
    }else {
        echo 0;
    }
  $res_a6 ->closeCursor();
  $conexion = null;
}
/* función que actualiza el anexo 7*/
function updA7($tinf,$rfin,$fe_info,$obs,$num_a){
  global $conexion;
    switch ($tinf){
      case '1':
        $tinf="Presupuesto";
        break;
      case '2':
         $tinf="Estado de situación financiera";
        break;
      case '3':
         $tinf="Estado de actividades";
       break;
      case '4':
         $tinf="Balanza de comprobación";
       break;
      case '5':
         $tinf="Deudores diversos";
        break;
      case '6':
         $tinf="Acredores diversos";
        break;
      case '7':
         $tinf="Proveedores";
        break;
      case '8':
         $tinf="Otros";
        break; 
      }

  $res_a7=$conexion->prepare("UPDATE ANEXO7 SET ID_ENTREGA=:id,FOLIO_PERIODO=:fp,TIPO_INFORME=:ti,REPORTE_FINANCIERO=:rf,FECHA_INFORME=:fi,OBSERVACIONES=:ob,F_CAPTURA=:fc WHERE FOLIOA7 = :folio");
  $res_a7->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":ti"=>$tinf,":rf"=>$rfin,":fi"=>$fe_info,":ob"=>$obs,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a7){
        Bitacora("Anexo 7");
        echo 1;
    }else{
        echo 0;
    }

  $res_a7->closeCursor();
  $conexion= null;
}
/* función que actualiza el anexo 8*/
function updA8($t_cuenta,$num_cuenta,$bancaria,$saldo,$con_banco,$obs_a8,$num_a){
  global $conexion;
  $res_a8 = $conexion->prepare("UPDATE ANEXO8 SET ID_ENTREGA=:id,FOLIO_PERIODO=:fp,T_CUENTA=:tc,NO_CUENTA=:nc,INSTITUCION=:ins,SALDO=:sal,CONCILIACION=:con,OBSERVACIONES=:obs,F_CAPTURA=:fc WHERE FOLIOA8 =:folio");
  $res_a8->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":tc"=>$t_cuenta,":nc"=>$num_cuenta,":ins"=>$bancaria,":sal"=>$saldo,":con"=>$con_banco,":obs"=>$obs_a8,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a8){
        Bitacora("Anexo 8");
        echo 1;
    }else {
        echo 0;
    }
  $res_a8->closeCursor();
  $conexion= null;
}
/* función que actualiza el anexo 9*/
function updA9($t_cuenta,$n_cuenta,$ins_ban,$foliof,$folioi,$obs,$num_a){
  global $conexion;
    switch ($t_cuenta) {
      case '1':
            $t_cuenta = "Chequera";
        break;
      case '2':
            $t_cuenta = "Recibos oficiales internos";
        break;
      case '3':
            $t_cuenta = "Póliza de diario";
        break;
      case '4':
            $t_cuenta = "Póliza de ingresos";
        break;
      case '5':
            $t_cuenta = "Póliza de egresoso";
        break;
      case '6':
            $t_cuenta = "Otros";
        break;
    }

  $res_a9=$conexion->prepare("UPDATE ANEXO9 SET ID_ENTREGA =:id,FOLIO_PERIODO=:fp,TIPO_DOCUMENTO=:td,NUMERO_CUENTA=:nc,INSTITUCION=:ins,N_FINAL=:nf,N_INICIAL=:ni,OBSERVACIONES=:obs,F_CAPTURA=:fc WHERE FOLIOA9 =:folio");
  $res_a9->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":td"=>$t_cuenta,":nc"=>$n_cuenta,":ins"=>$ins_ban,":nf"=>$foliof,":ni"=>$folioi,":obs"=>$obs,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a9){
        Bitacora("Anexo 9");
        echo 1;
    }else{
        echo 0;
    }      
  $res_a9->closeCursor();
  $conexion=null;
}

/* función que actualiza el anexo 10*/
function updA10($f_inicio,$f_auto,$f_caja,$d_pagados,$g_pend,$num_a){
  global $conexion;
  $res_a10=$conexion->prepare("UPDATE ANEXO10 SET ID_ENTREGA = :id,FOLIO_PERIODO = :fp,FECHA_INFORME = :fi, MONTO_AUTORIZADO = :ma, MONTO_DISPONIBLE = :md, GASTOS_PENDIENTES =:gp, DOCUMENTOS_PAGADOS =:dp ,F_CAPTURA =:fc WHERE FOLIOA10 =:num_a");
  $res_a10->execute(array(":id"=>$_SESSION['id_entrega'], ":fp"=>$_SESSION['periodo'], ":fi"=>$f_inicio, ":ma"=>$f_auto, ":md"=>$f_caja, ":gp"=>$g_pend, ":dp"=>$d_pagados, ":fc"=>$_SESSION['fecha_captura'], ":num_a"=>$num_a));
    if($res_a10){
        Bitacora("Anexo 10");
        echo 1;
    }else{
        echo 0;
    }
  $res_a10->closeCursor();
  $conexion=null;
}
/* función que actualiza el anexo 11*/
function updA11($num_doc,$des,$imp,$f_ini,$f_ter,$obs,$num_a){
  global $conexion;
  $res_a11 = $conexion -> prepare("UPDATE ANEXO11 SET ID_ENTREGA =:id, FOLIO_PERIODO=:fp, NO_DOC=:nd, DESC_DOC=:des, IMPORTE=:im, F_INICIO=:fi, F_TERMINO=:ft, OBSERVACIONES=:obs, F_CAPTURA=:fc WHERE FOLIOA11 =:folio");
  $res_a11->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":nd"=>$num_doc,":des"=>$des,":im"=>$imp,":fi"=>$f_ini,":ft"=>$f_ter,":obs"=>$obs,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a11){
        Bitacora("Anexo 11");
        echo 1;
    }else{
        echo 0;
    }
  $res_a11->closeCursor();
  $conexion=null;
}
/* función que actualiza el anexo 12*/
function updA12($f_aper,$f_falla,$proyecto,$unidad,$concurso,$origen,$empresa,$obs,$num_a){
  global $conexion;
    switch ($concurso) {
      case '1':
        $concurso="Adjudicación directa";
        break;
      case '2':
        $concurso="Cuadro corporativo";
        break;
      case '3':
        $concurso="Investigación restringida a tres proveedores";
        break;
      case '4':
        $concurso="Licitación";
        break;
      default:
        # code...
        break;
    }
    $res_a12=$conexion->prepare("UPDATE ANEXO12 SET ID_ENTREGA =:id, FOLIO_PERIODO=:fp, F_APERTURA=:fa, F_FALLO=:ff, PROYECTO=:pr, UNIDAD=:uni, TIPO_CONCURSO=:tc, ORIGEN_RECURSO=:ore, EMPRESA_GANADORA=:ega, OBSERVACIONES=:obs, F_CAPTURA=:fc WHERE FOLIOA12=:folio");
    $res_a12->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":fa"=>$f_falla,":ff"=>$f_falla,":pr"=>$proyecto,":uni"=>$unidad,":tc"=>$concurso,":ore"=>$origen,":ega"=>$empresa,":obs"=>$obs,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a12){
      Bitacora("Anexo 12");
      echo 1;
    }else{
      echo 0;
    }
    $res_a12->closeCursor();
    $concurso=null;
  }
/* función que actualiza el anexo 13*/
function updA13($desc,$ubica,$m_auto,$m_eje,$m_ejer,$a_fis,$a_fina,$obs,$num_a){
  global $conexion;
  $res_a13=$conexion->prepare("UPDATE ANEXO13 SET ID_ENTREGA=:id, FOLIO_PERIODO=:fp, DESC_OBRA=:des, UBICACION=:ubi, MONTO_AUTORIZA=:ma, MONTO_EJERCIDO=:me, MONTO_EJERCER=:mej, AVANCE_FISICO=:af, AVANCE_FINANCIERO=:afi, OBSERVACIONES=:obs, F_CAPTURA=:fc WHERE FOLIOA13 = :folio");
  $res_a13->execute(array(":id"=>$_SESSION['id_entrega'],":fp"=>$_SESSION['periodo'],":des"=>$desc,":ubi"=>$ubica,":ma"=>$m_auto,":me"=>$m_eje,":mej"=>$m_ejer,":af"=>$a_fis,":afi"=>$a_fina,":obs"=>$obs,":fc"=>$_SESSION['fecha_captura'],":folio"=>$num_a));
    if($res_a13){
      Bitacora("Anexo 13");
      echo 1;
    }else {
      echo 0;
    }
    $res_a13->closeCursor();
    $conexion=null;
  }

function Bitacora($anexo){
  global $conexion;
  $evento="Editar";
  $bit=$conexion->prepare("INSERT INTO BITACORA(ANEMOX_M,ANEXO,EVENTO,USUARIO,F_EVENTO) VALUES(:anemox,:anexo,:evento,:usuario,:fecha)");
  $bit->execute(array(":anemox"=>$_SESSION['id_entrega'], ":anexo"=>$anexo, ":evento"=>$evento, ":usuario"=>$_SESSION['nombre'],":fecha"=>$_SESSION['fecha_captura'])); 
}

?>