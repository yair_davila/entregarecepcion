<!DOCTYPE html>
<html>
<head>
  <title></title>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">           
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Entrega</th>
        <th>Espacio</th>
        <th>Objetivo</th>
        <th>Usuario</th>
        <th>Fecha captura</th>
        <th>    Acción</th>
      </tr>
    </thead>
    <tbody>
      <?php
      include 'conexion.php';
      $sql_selec = " SELECT C.nombre as name, E.espacio,R.NOMBRE, A.OBJETIVO, A.F_CAPTURA FROM CAT_UNIDADES C join ENTREGA E on C.id_unidad = E.id_unidad join RESP_CI R ON R.RFC_CI=E.RFC_CI JOIN ANEXO1 A on E.id_entrega = A.id_entrega WHERE E.id_entrega = :id";

      $res=$conexion->prepare($sql_selec);
      $res->bindValue(":id",19);
      $res->execute();
    while($fila = $res -> fetch(PDO::FETCH_ASSOC)):?>
      <tr>
        <td><?php echo $fila['name']; ?></td>
        <td><?php echo strtoupper($fila['espacio']) ?></td>
        <td><?php echo $fila['OBJETIVO']; ?></td>
        <td><?php echo $fila['NOMBRE']; ?></td>
        <td><?php echo $fila['F_CAPTURA']; ?></td>
        
        
        <td>
          <button type="button" class="btn btn-warning">Editar</button>
          <button type="button" class="btn btn-danger">Borrar</button>
        </t>
      </tr>
    <?php endwhile; $res->closeCursor(); ?>
      
    </tbody>
  </table>
</div>

</body>
</html>