<?php 
session_start();
  if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("location:index.php");
  
  }
  require 'conexion.php';
?>
<div class="container-fluid">
	<table class="table table-condensed">
		<thead>
			<tr>
        		<th>Número de documento</th>
        		<th>Descripción</th>
        		<th>Importe</th>
        		<th>Fecha de inicio</th>
        		<th>Fecha de término</th>
        		<th>Observaciones</th>
        		<th>Fecha captura</th>
        		<th>Acción</th>
      		</tr>
		</thead>
		<tbody>
			<?php 
			$sql_a11 = "SELECT FOLIOA11,NO_DOC,DESC_DOC,IMPORTE,F_INICIO,F_TERMINO,OBSERVACIONES,F_CAPTURA FROM ANEXO11  WHERE ID_ENTREGA = :id";
      		$res=$conexion->prepare($sql_a11);
      		$res->bindValue(":id",$_SESSION['id_entrega']);
      		$res->execute();
			while($fila = $res -> fetch(PDO::FETCH_ASSOC)):
				$datos = $fila['FOLIOA11'].",".$fila['NO_DOC'].",".$fila['DESC_DOC'].",".$fila['IMPORTE'].",".$fila['F_INICIO'].",".$fila['F_TERMINO'].",".$fila['OBSERVACIONES'].",".$fila['F_CAPTURA'];
				
				$borrar = $fila['FOLIOA11'].",". 11;
        

				?>
			<tr>
				<td><?php echo $fila['NO_DOC']; ?></td>
				<td><?php echo $fila['DESC_DOC']; ?></td>
				<td><?php echo $fila['IMPORTE']; ?></td>
				<td><?php echo $fila['F_INICIO']; ?></td>
				<td><?php echo $fila['F_TERMINO']; ?></td>
				<td><?php echo $fila['OBSERVACIONES']; ?></td>
				<td><?php echo $fila['F_CAPTURA']; ?></td>
				<td>
					<button class="btn btn-warning" data-toggle="modal" data-target="#Edicion_A11" onclick="datos_a11('<?php echo $datos ?>')" title="Actualizar"><samp class="glyphicon glyphicon-pencil"></samp></button>
					<button class="btn btn-danger" onclick="borrar_reg('<?php echo $borrar ?>')" title="Borrar"><span class="glyphicon glyphicon-trash" ></span></button>
				
				</td>
			</tr>
			<?php endwhile; $res->closeCursor(); $conexion=null; ?> 	
		</tbody>
	</table>
</div>


<form id="a11">
<div class="modal fade" id="Edicion_A11" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar anexo</h4>
      </div>
      <div class="modal-body">
      	<label >Número de documento</label>
		<input type="text"  name="ndoc_a11u" id="ndoc_a11u" placeholder="Número de documento" class="form-control">
		<br>
		<label>Descripción</label>
		<textarea class="form-control" name="des_a11u" id="des_a11u" placeholder="Describa las observaciones"></textarea>		
		<br>
		<label>Importe</label>
		<input type="text"  name="imp_a11u" id="imp_a11u" class="form-control" placeholder="Importe">
		<br>
		<label>Fecha de inicio</label>
		<input type="date" class="form-control"  name="fi_a11u" id="fi_a11u">
        <br>
        <label>Fecha de término:</label>
		<input type="date" class="form-control"  name="ft_a11u" id="ft_a11u">
		<br>
		<label>Observaciones</label>
		<textarea class="form-control" name="obs_a11u" id="obs_a11u" placeholder="Describa las observaciones"></textarea>
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </div>
</div>
</form>


<script>
$(document).ready(function () {

    $.validator.addMethod('texto',function(value,element){
        return this.optional(element) || /^([a-zA-Z0-9áÁéÉíÍóÓúÚñÑüÜ\-,.;\s])*$/.test(value);
      });

    $('#a11').validate({
        rules: {
            ndoc_a11u: {required: true, number:true},
            des_a11u: {required: true, texto:true},
            imp_a11u: {required: true, number:true},
            fi_a11u: {required: true,},
            ft_a11u: {required: true,},
            obs_a11u: {required: true, texto:true},
        },
        messages: {
            ndoc_a11u: {
                required: "Completa el campo por favor",
                number:"Campo solo numérico",
            },
            des_a11u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            imp_a11u:{
              required: "Completa el campo por favor",
              number:"Campo solo numérico", 
            },
            fi_a11u:{
              required: "Completa el campo por favor",
            },
            ft_a11u:{
              required: "Completa el campo por favor",              
            },
            obs_a11u:{
              required: "Completa el campo por favor",
              texto:"No se aceptan caracteres especiales verificalo por favor",
            },
            
        },
        submitHandler: function () {
           actualiza_a11();
        }
    });
  });

  $('#Edicion_A11').on('hidden.bs.modal', function (e) {
      $("label.error").remove();

  });
</script>
