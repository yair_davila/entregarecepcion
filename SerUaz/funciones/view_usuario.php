<?php
/**
 * Script donde se encuentran algunas funciones que se encargan de mostrar la descripcion de la entrega, periodo y anexos 
 */

if (!isset($_SESSION["usuario"])) {//si la varible de sesion del usuario no esta establecida se manda al index
  
  header("Location: https://localhost/SerUaz/");
  }

require '../conexion.php';

/**
 * Descripción:  
 */

/*funcion que obtiene los anexos relacionados con el el id_entrega*/
function getActivaAnexos(){
	idMax();
	global $conexion;
	$resulset = $conexion ->prepare("SELECT ANEXOS,FINICIO,FFIN FROM PERIODO WHERE id_entrega = :id and FOLIO_PERIODO =:pa");
	$resulset->bindValue(":id",$_SESSION['id_entrega']);
	$resulset->bindValue(":pa",$_SESSION['per_actual']);
	$resulset -> execute();
	$anexos =$resulset->fetch(PDO::FETCH_ASSOC);
	$_SESSION['anexos'] = $anexos['ANEXOS'];
	$_SESSION['periodo'] = $_SESSION['per_actual']; //se establece el periodo actual
    $pinicio=$anexos['FINICIO'];
    $pfin=$anexos['FFIN'];
    $resulset->closeCursor();
    $conexion=null;
    $pi=invdate($pinicio);
    $pf=invdate($pfin);
    $_SESSION['fper_actual']=$pi." - ".$pf;
    echo "<h5>Periodo: ". $_SESSION['fper_actual'] ."</h5>";
    echo "<h5>Anexos aplicables: ". $_SESSION['anexos'] ."</h5>";
}

function idMax(){
 	global $conexion;
	$resp = $conexion-> prepare("SELECT MAX(FOLIO_PERIODO) AS FOLIO FROM PERIODO WHERE ID_ENTREGA =:id");
	$resp->bindValue(":id",$_SESSION['id_entrega']);
	$resp->execute();
	$p_actual = $resp -> fetch (PDO::FETCH_ASSOC);
	$_SESSION['per_actual']= $p_actual['FOLIO'];
	$resp->closeCursor();
}

function invdate($fecha){
	$date = explode('-', $fecha);
    $finv = $date[2]."-".$date[1]."-".$date[0];
    return $finv;
}
          


?>