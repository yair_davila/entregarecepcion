function actualizarTabla(data) {
	
	if(data[0]  ==  undefined){
		$("#tabla_personas").children().remove();
		$("#tabla_personas").append("<thead>"+
				"<th>Nombre</th>"+
				"<th>Apaterno</th>"+
				"<th>Amaterno</th>"
		);
		$("#tabla_personas").append("</thead>");
	}

	if (data[0]['error'] != undefined) {
		document.getElementById("alerta_administracion").className = "alert alert-block alert-danger fade in";
		$("#alerta_administracion").children().remove();
		$("#alerta_administracion").append(data[0]['error']);
	} else {
		//mostrarAlertas();
		//limpiado();
		
		$("#tabla_personas").children().remove();
		
		$("#tabla_personas").append("<thead><tr>"+
				"<th>Nombre</th>"+
				"<th>Apaterno</th>"+
				"<th>Amaterno</th>"+
				"<th>Opciones</th>"
		);
		$("#tabla_personas").append("<tr></thead><tbody>");

		for (i = data.length-1; i >= 0 ; i--) {
			$("#tabla_personas").append(
				"<tr id='info_personas_"+ data[i]['id'] +"'>\n" +
						
						"<td>" + data[i]['nombre'] + "</td>"+
						"<td>" + data[i]['apaterno'] + "</td>"+
						"<td>" + data[i]['amaterno'] + "</td>"+

					"<td>"+
		                "<button class='btn btn-warning' onclick='obtenerUno("+data[i]['id']+")'>"+
		                  "Editar"+
		                "</button>"+
		                "<label>_</label>"+
		                "<button class='btn btn-danger' onclick='obtenerUno("+data[i]['id']+")'>" +
		                  "Eliminar"+
		                "</button>"+
		            "</td>"+
				"</tr>"
			);
		}
		$("#tabla_persona").append("</tbody>");
		$('#id_elemento').val(0);
	}
}
function actualizarPersona() {
	var id=0
	$.ajax(
		{
        url : "localhost:8000/entrega/persona/",
        type : "POST",
        data : {
        	id : $('#id_elemento').val(),
			nombre:$('#id_nombre').val().toUpperCase(),
			apaterno:$('#id_apaterno').val().toUpperCase(),
			amaterno:$('#id_amaterno').val().toUpperCase(),
		},
        success : function(data) {
			actualizarTabla(data);
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
			console.log(err);
        }
    });
};
function registrarPersona() {
	var id=0
	$.ajax(
		{
        url : "localhost:8000/entrega/persona/",
        type : "POST",
        data : {
			nombre:$('#id_nombre').val().toUpperCase(),
			apaterno:$('#id_apaterno').val().toUpperCase(),
			amaterno:$('#id_amaterno').val().toUpperCase(),
		},
        success : function(data) {
			actualizarTabla(data);
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
			console.log(err);
        }
    });
};
function obtenerUno(parametro) {
	var id=0
	$.ajax(
		{
        url : "localhost:8000/entrega/persona/",
        type : "GET",
        data : {
			id:parametro
		},
        success : function(data) {
			$('#id_elemento').val(data[0]['id']);
			$('#id_nombre').val(data[0]['nombre']);
			$('#id_apaterno').val(data[0]['apaterno']);
			$('#id_amaterno').val(data[0]['amaterno']);
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
			console.log(err);
        }
    });
};

function obtenerTodos(parametro) {
	var id=0
	$.ajax(
		{
        url : "https://autentificaciontest.herokuapp.com/entrega/persona/",
        type : "GET",
        data : {
		},
        success : function(data) {
			actualizarTabla(data);
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
			console.log(err);
        }
    });
};