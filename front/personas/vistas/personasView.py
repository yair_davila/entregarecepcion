from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from datetime import date
from django.http import HttpResponse
import simplejson
import requests

class PersonasView():
	def nuevo(context):
		return render(context,'nuevo_persona.html')
	
	def obtener_personas(context):
		url = 'http://autentificaciontest.herokuapp.com/entrega/persona/'
		# url = 'https://jsonplaceholder.typicode.com/posts'
		args = {''}
		response = requests.get(url)
		if response.status_code == 200:
			response_json = response.json()
			data = response.json()
			data = simplejson.dumps(data)
		else:
			personasJson = []
			data = simplejson.dumps(personasJson)
		return HttpResponse(data, content_type="application/json")

	def obtener_personas_post(context):
		url = 'http://autentificaciontest.herokuapp.com/entrega/persona/'
		args = {''}
		if (context.method == 'GET'):
			print("id" in context.GET)
			if("id" in context.GET):
				args = {'id':context.GET.get("id")}
				response = requests.get(url,data=args)
				print(response)
				print(response.json())
		else:
			response = requests.get(url)
		if response.status_code == 200:
			response_json = response.json()
			data = response.json()
			data = simplejson.dumps(data)
		else:
			personasJson = []
			data = simplejson.dumps(personasJson)
		return HttpResponse(data, content_type="application/json")

	def crear_persona(context):
		url = 'http://autentificaciontest.herokuapp.com/entrega/persona/'
		args = {''}
		if("id" in context.GET):
			args = {
				'id':context.GET.get('id'),
				'nombre':context.GET.get('nombre'),
				'apaterno':context.GET.get('apaterno'),
				'amaterno':context.GET.get('amaterno')
			}		
		else:
			args = {
				'id':context.GET.get('id'),
				'nombre':context.GET.get('nombre'),
				'apaterno':context.GET.get('apaterno'),
				'amaterno':context.GET.get('amaterno')
			}
		response = requests.post(url,data=args)
		if response.status_code < 300 and response.status_code > 199:
			response_json = response.json()
			data = response.json()
			data = simplejson.dumps(data)
		else:
			print(response.json())
			personasJson = []
			data = simplejson.dumps(personasJson)
		return HttpResponse(data, content_type="application/json")