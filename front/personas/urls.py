from django.urls import path
from personas.vistas.personasView import PersonasView
urlpatterns = [
    #path('', ImagenAnunciosView.list_imagenAnuncios, name='list_imagenAnuncios'),
    path('', PersonasView.nuevo, name='personas_nuevo'),
    path('obtenerpersonas', PersonasView.obtener_personas, name='personas_obtener'),
    path('obtenerpersonaspost', PersonasView.obtener_personas_post, name='personas_post_obtener'),
    path('crearpersona', PersonasView.crear_persona, name='personas_crear'),
]