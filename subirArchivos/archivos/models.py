from django.db import models

# Create your models here.
class Archivo (models.Model):
	documento = models.FileField(upload_to="documentos")

	class Meta:
		app_label = "archivos"