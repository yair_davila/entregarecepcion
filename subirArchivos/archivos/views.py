from django.shortcuts import render, redirect
from archivos.models import Archivo
from archivos.formArchivo import FormArchivo

# Create your views here.
def nuevo(context):
	if(context.method == 'POST'):
		form = FormArchivo(context.POST, context.FILES)
		if form.is_valid():
			archivo = form.save()
			archivo.save()
			return redirect ('archivo_nuevo')
		
	form = FormArchivo()
	archivos = Archivo.objects.all()
	return render(context,'nuevo_archivo.html',{'formulario': form,'archivos':archivos})
