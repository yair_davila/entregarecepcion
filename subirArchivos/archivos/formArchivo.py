from archivos.models import Archivo
from django import forms

class FormArchivo (forms.ModelForm):
	class Meta:
		model = Archivo
		fields = ('documento',)
