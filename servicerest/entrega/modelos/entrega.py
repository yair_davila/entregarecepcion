from django.db import models

class Entrega(models.Model):
    rfc_ci = models.CharField('Numero_Domicilio', max_length=13)
    id_unidad = models.SmallIntegerField('Estatus', default=1)
    espacio = models.CharField('Numero_Domicilio', max_length=250)
    directorio = models.CharField('Numero_Domicilio', max_length=250)
    tipo_unidad = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.rfc_ci.__str__()+" "+self.espacio

    class Meta:
        app_label = "entrega"