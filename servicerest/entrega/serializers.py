from entrega.modelos.entrega import Entrega
from rest_framework import serializers

# Serializers define the API representation.
class EntregaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entrega
        fields = '__all__'
"""
class EntregaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entrega
        fields = '__all__'
            #['url', 'rfc_ci', 'id_unidad', 'espacio','directorio','tipo_unidad']

from rest_framework import serializers
from entrega.modelos.entrega import Entrega

class EntregaSerializer(serializers.ModelSerializer):

    id= serializers.ReadOnlyField()
    rfc_ci = serializers.CharField()
    id_unidad = serializers.IntegerField()
    espacio = serializers.CharField()
    directorio = serializers.CharField()
    tipo_unidad = serializers.IntegerField()

    def create (self, validate_data):
        intance = Entrega()
        intance.rfc_ci = validate_data.get('rfc_ci')
        intance.id_unidad = validate_data.get('id_unidad')
        intance.espacio = validate_data.get('espacio')
        intance.directorio = validate_data.get('directorio')
        intance.tipo_unidad = validate_data.get('tipo_unidad')
        intance.save()

    class Meta:
        model = Entrega
        fields = '__all__'
"""