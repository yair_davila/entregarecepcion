
from django.urls import path
from rest_framework import routers
from entrega.viewsets import EntregaViewSet
from entrega.models import Entrega

"""
router = routers.SimpleRouter()
router.register('entrega',EntregaViewSet.as_view())
"""
router = routers.SimpleRouter()
router.register(r'entrega', EntregaViewSet)
urlpatterns = router.urls
