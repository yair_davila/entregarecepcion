from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from entrega.modelos.entregaModel import Entrega
from rest_framework import status
from entrega.serializadores.serializadorEntrega import EntregaSerializador

class EntregaView(APIView):
    
    def get(self, request):
        entregas = Entrega.objects.all()
        serializer = EntregaSerializador(entregas, many=True)
        # content = {'message': 'Hello, World!'}
        # return Response(content)
        return Response(serializer.data)
    
    def post(self, request):
        o = request.POST.get('id', None)
        serializado = EntregaSerializador(data=request.data)
        valido = serializado.is_valid()
        print(valido)
        if(o != None):
            if valido:
                print("actualizacion")
                #serializado = EntregaSerializador(Entrega.objects.get(pk=o), data=request.data)
            else:
                print("obteniendo")
                entregas = Entrega.objects.get(pk=o)
                serializado = EntregaSerializador(entregas, data=request.data)
                return Response(serializado.data)
        else:
            if valido:
                print("creacion")
                serializado.save()
                
                return Response(serializado.data, status=status.HTTP_201_CREATED)

        print(str(serializado.errors))
        return Response(serializado.errors, status=status.HTTP_400_BAD_REQUEST)