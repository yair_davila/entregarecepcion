from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from entrega.modelos.personaModel import Persona
from rest_framework import status
from entrega.serializadores.serializadorPersona import PersonaSerializador
from datetime import date

class PersonaView(APIView):
    
    def get(self, request):
        """
        print(request.data['id'])
        print('id' in request.data)
        print('other' in request.data)
        

        identificador = request.GET.get('id', None)
        print(identificador)
        """
        entregas = []
        
        if('id' in request.data):
            entregas = Persona.objects.filter(id = request.data['id'])
        else:
            entregas = Persona.objects.filter(estatus = 1)
        serializer = PersonaSerializador(entregas, many=True)
        # content = {'message': 'Hello, World!'}
        # return Response(content)
        return Response(serializer.data)
    
    def post(self, request):
        serializado = PersonaSerializador(data=request.data)
        valido = serializado.is_valid()
        if(valido):
            if('id' in request.data):
                entrega = Persona.objects.get(id = request.data['id'])
                entrega.fecha_actualizacion = date.today()
                entrega.nombre = request.data['nombre']
                entrega.apaterno = request.data['apaterno']
                entrega.amaterno = request.data['amaterno']
                entrega.save()
            else:
                
                entrega = serializado.save()
                entrega.fecha_creacion = date.today()
                entrega.save()
            entregas = Persona.objects.filter(estatus = 1)
            serializer = PersonaSerializador(entregas, many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        print(str(serializado.errors))
        return Response(serializado.errors, status=status.HTTP_400_BAD_REQUEST)