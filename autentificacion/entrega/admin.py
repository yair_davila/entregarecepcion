from django.contrib import admin
from entrega.modelos.personaModel import Persona
from entrega.modelos.entregaModel import Entrega

admin.site.register(Persona)
admin.site.register(Entrega)
# Register your models here.
