from rest_framework import serializers
from entrega.modelos.personaModel import Persona
from django.db import models

class PersonaSerializador(
    serializers.ModelSerializer
    #serializers.Serializer
    ):

    class Meta:
        model = Persona
        fields = [
            'id',
            'nombre',
            'apaterno',
            'amaterno'
            ]

