from rest_framework import serializers
from entrega.modelos.entregaModel import Entrega
from django.db import models

class EntregaSerializador(
    #serializers.ModelSerializer
    serializers.Serializer
    ):
    
    
    
    rfc_ci = models.CharField(max_length=100)
    id_unidad = models.IntegerField()
    espacio = models.CharField(max_length=250,)
    directorio = models.CharField(max_length=250,)
    tipo_unidad = models.IntegerField()
    
    def create(self, validated_data):
        instance = Entrega()
        instance.rfc_ci = validated_data.get('rfc_ci', None)
        instance.id_unidad = validated_data.get('id_unidad', None)
        instance.espacio = validated_data.get('espacio', None)
        instance.directorio = validated_data.get('directorio', None)
        instance.tipo_unidad = validated_data.get('tipo_unidad', None)
        
        #print(instance.clean_fields())
        return instance

    def update(self, instance, validated_data):
        instance.rfc_ci = validated_data.get('rfc_ci', instance.rfc_ci)
        instance.id_unidad = validated_data.get('id_unidad', instance.id_unidad)
        instance.espacio = validated_data.get('espacio', instance.espacio)
        instance.directorio = validated_data.get('directorio', instance.directorio)
        instance.tipo_unidad = validated_data.get('tipo_unidad', instance.tipo_unidad)
        #instance.save()
        print(instance.clean_fields())
        return instance
    """
    class Meta:
        model = Entrega
        fields = [
            'id',
            'rfc_ci',
            'id_unidad',
            'espacio',
            'directorio',
            'tipo_unidad'
            ]
    """
