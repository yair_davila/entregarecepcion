from django.urls import path
from entrega.vistas.entragaView import EntregaView
from entrega.vistas.personaView import PersonaView

urlpatterns = [
    path('', EntregaView.as_view(), name='entrega'),
    path('persona/', PersonaView.as_view(), name='persona')
]