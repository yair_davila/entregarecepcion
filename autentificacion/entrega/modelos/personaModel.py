from django.db import models

class Persona(models.Model):
    nombre = models.CharField(max_length=250)
    apaterno = models.CharField(max_length=250)  # Field name made lowercase.
    amaterno = models.CharField(max_length=250)  # Field name made lowercase.
    estatus = models.IntegerField(default=1)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_creacion = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.nombre) + " " + str(self.apaterno ) + " " + str(self.amaterno ) + " " + str(self.estatus) + " " + str(self.fecha_creacion) + " " + str(self.fecha_actualizacion)

    class Meta:
        app_label = "entrega"