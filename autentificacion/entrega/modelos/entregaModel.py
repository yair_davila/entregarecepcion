from django.db import models

class Entrega(models.Model):
    rfc_ci = models.CharField(max_length=250)
    # models.ForeignKey('RespCi', models.DO_NOTHING, db_column='RFC_CI')  # Field name made lowercase.
    id_unidad = models.IntegerField()  # Field name made lowercase.
    # id_unidad = models.ForeignKey(CatUnidades, models.DO_NOTHING, db_column='ID_UNIDAD')  # Field name made lowercase.
    espacio = models.CharField(max_length=250)  # Field name made lowercase.
    directorio = models.CharField(max_length=250)  # Field name made lowercase.
    tipo_unidad = models.IntegerField()  # Field name made lowercase.

    class Meta:
        app_label = "entrega"