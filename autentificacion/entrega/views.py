from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from entrega.modelos.entregaModel import Entrega
from entrega.serializadores.serializadorEntrega import EntregaSerializador

class HelloView(APIView):

	def get(self, request):
		books = Entrega.objects.all()
        serializer = EntregaSerializador(books, many=True)
		#content = {'message': 'Hello, World!'}
		#return Response(content)
        return Response(serializer.data)

	
	def post(self, request):
		serializado = EntregaSerializador(data=request.data)
		#content = {'message': 'Hello, World!'}
		#return Response(content)
        if serializado.is_valid():
            serializado.save()
            return Response(serializado.data, status=status.HTTP_201_CREATED)
        return Response(serializado.errors, status=status.HTTP_400_BAD_REQUEST)