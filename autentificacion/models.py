# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Anexo1(models.Model):
    folioa1 = models.AutoField(db_column='FOLIOA1', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    objetivo = models.TextField(db_column='OBJETIVO')  # Field name made lowercase.
    creacion = models.DateField(db_column='CREACION')  # Field name made lowercase.
    legal = models.TextField(db_column='LEGAL')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO1'


class Anexo10(models.Model):
    folioa10 = models.AutoField(db_column='FOLIOA10', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    fecha_informe = models.DateField(db_column='FECHA_INFORME')  # Field name made lowercase.
    monto_autorizado = models.CharField(db_column='MONTO_AUTORIZADO', max_length=250)  # Field name made lowercase.
    monto_disponible = models.CharField(db_column='MONTO_DISPONIBLE', max_length=250)  # Field name made lowercase.
    gastos_pendientes = models.CharField(db_column='GASTOS_PENDIENTES', max_length=250)  # Field name made lowercase.
    documentos_pagados = models.CharField(db_column='DOCUMENTOS_PAGADOS', max_length=250)  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO10'


class Anexo11(models.Model):
    folioa11 = models.AutoField(db_column='FOLIOA11', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    no_doc = models.CharField(db_column='NO_DOC', max_length=250)  # Field name made lowercase.
    desc_doc = models.TextField(db_column='DESC_DOC')  # Field name made lowercase.
    importe = models.CharField(db_column='IMPORTE', max_length=250)  # Field name made lowercase.
    f_inicio = models.DateField(db_column='F_INICIO')  # Field name made lowercase.
    f_termino = models.DateField(db_column='F_TERMINO')  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO11'


class Anexo12(models.Model):
    folioa12 = models.AutoField(db_column='FOLIOA12', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    f_apertura = models.DateField(db_column='F_APERTURA')  # Field name made lowercase.
    f_fallo = models.DateField(db_column='F_FALLO')  # Field name made lowercase.
    proyecto = models.TextField(db_column='PROYECTO')  # Field name made lowercase.
    unidad = models.TextField(db_column='UNIDAD')  # Field name made lowercase.
    tipo_concurso = models.CharField(db_column='TIPO_CONCURSO', max_length=250)  # Field name made lowercase.
    origen_recurso = models.CharField(db_column='ORIGEN_RECURSO', max_length=250)  # Field name made lowercase.
    empresa_ganadora = models.CharField(db_column='EMPRESA_GANADORA', max_length=250)  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO12'


class Anexo13(models.Model):
    folioa13 = models.AutoField(db_column='FOLIOA13', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    desc_obra = models.TextField(db_column='DESC_OBRA')  # Field name made lowercase.
    ubicacion = models.TextField(db_column='UBICACION')  # Field name made lowercase.
    monto_autoriza = models.CharField(db_column='MONTO_AUTORIZA', max_length=250)  # Field name made lowercase.
    monto_ejercido = models.CharField(db_column='MONTO_EJERCIDO', max_length=250)  # Field name made lowercase.
    monto_ejercer = models.CharField(db_column='MONTO_EJERCER', max_length=250)  # Field name made lowercase.
    avance_fisico = models.CharField(db_column='AVANCE_FISICO', max_length=250)  # Field name made lowercase.
    avance_financiero = models.CharField(db_column='AVANCE_FINANCIERO', max_length=250)  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO13'


class Anexo2(models.Model):
    folioa2 = models.AutoField(db_column='FOLIOA2', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    asunto = models.TextField(db_column='ASUNTO')  # Field name made lowercase.
    unidad_tramite = models.TextField(db_column='UNIDAD_TRAMITE')  # Field name made lowercase.
    avance = models.TextField(db_column='AVANCE')  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO2'


class Anexo3(models.Model):
    folioa3 = models.AutoField(db_column='FOLIOA3', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    nomina = models.TextField(db_column='NOMINA')  # Field name made lowercase.
    corte = models.DateField(db_column='CORTE')  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO3'


class Anexo4(models.Model):
    folioa4 = models.AutoField(db_column='FOLIOA4', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    inventario = models.TextField(db_column='INVENTARIO')  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA')  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO4'


class Anexo5(models.Model):
    folioa5 = models.AutoField(db_column='FOLIOA5', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    cantidad = models.TextField(db_column='CANTIDAD')  # Field name made lowercase.
    descripcion = models.TextField(db_column='DESCRIPCION')  # Field name made lowercase.
    ubicacion = models.TextField(db_column='UBICACION')  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO5'


class Anexo6(models.Model):
    folioa6 = models.AutoField(db_column='FOLIOA6', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA', blank=True, null=True)  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO', blank=True, null=True)  # Field name made lowercase.
    area_resguarda = models.TextField(db_column='AREA_RESGUARDA', blank=True, null=True)  # Field name made lowercase.
    tipo_doc = models.TextField(db_column='TIPO_DOC', blank=True, null=True)  # Field name made lowercase.
    tipo_archivo = models.TextField(db_column='TIPO_ARCHIVO', blank=True, null=True)  # Field name made lowercase.
    contenido = models.TextField(db_column='CONTENIDO', blank=True, null=True)  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE', blank=True, null=True)  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO6'


class Anexo7(models.Model):
    folioa7 = models.AutoField(db_column='FOLIOA7', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    tipo_informe = models.TextField(db_column='TIPO_INFORME')  # Field name made lowercase.
    reporte_financiero = models.CharField(db_column='REPORTE_FINANCIERO', max_length=3)  # Field name made lowercase.
    fecha_informe = models.DateField(db_column='FECHA_INFORME')  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO7'


class Anexo8(models.Model):
    folioa8 = models.AutoField(db_column='FOLIOA8', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    t_cuenta = models.TextField(db_column='T_CUENTA')  # Field name made lowercase.
    no_cuenta = models.TextField(db_column='NO_CUENTA')  # Field name made lowercase.
    institucion = models.TextField(db_column='INSTITUCION')  # Field name made lowercase.
    saldo = models.TextField(db_column='SALDO')  # Field name made lowercase.
    conciliacion = models.CharField(db_column='CONCILIACION', max_length=5)  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO8'


class Anexo9(models.Model):
    folioa9 = models.AutoField(db_column='FOLIOA9', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey('Entrega', models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    folio_periodo = models.ForeignKey('Periodo', models.DO_NOTHING, db_column='FOLIO_PERIODO')  # Field name made lowercase.
    tipo_documento = models.TextField(db_column='TIPO_DOCUMENTO')  # Field name made lowercase.
    numero_cuenta = models.TextField(db_column='NUMERO_CUENTA')  # Field name made lowercase.
    institucion = models.TextField(db_column='INSTITUCION')  # Field name made lowercase.
    n_final = models.CharField(db_column='N_FINAL', max_length=250)  # Field name made lowercase.
    n_inicial = models.CharField(db_column='N_INICIAL', max_length=250)  # Field name made lowercase.
    observaciones = models.TextField(db_column='OBSERVACIONES')  # Field name made lowercase.
    enlace = models.TextField(db_column='ENLACE')  # Field name made lowercase.
    f_captura = models.DateField(db_column='F_CAPTURA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANEXO9'


class Bitacora(models.Model):
    folio = models.AutoField(db_column='FOLIO', primary_key=True)  # Field name made lowercase.
    anemox_m = models.CharField(db_column='ANEMOX_M', max_length=50)  # Field name made lowercase.
    anexo = models.CharField(db_column='ANEXO', max_length=50)  # Field name made lowercase.
    evento = models.CharField(db_column='EVENTO', max_length=50)  # Field name made lowercase.
    usuario = models.CharField(db_column='USUARIO', max_length=50)  # Field name made lowercase.
    f_evento = models.DateField(db_column='F_EVENTO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BITACORA'


class CatUnidades(models.Model):
    id_unidad = models.AutoField(db_column='ID_UNIDAD', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=250)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CAT_UNIDADES'


class Entrega(models.Model):
    id_entrega = models.AutoField(db_column='ID_ENTREGA', primary_key=True)  # Field name made lowercase.
    rfc_ci = models.ForeignKey('RespCi', models.DO_NOTHING, db_column='RFC_CI')  # Field name made lowercase.
    id_unidad = models.ForeignKey(CatUnidades, models.DO_NOTHING, db_column='ID_UNIDAD')  # Field name made lowercase.
    espacio = models.CharField(db_column='ESPACIO', max_length=250)  # Field name made lowercase.
    directorio = models.CharField(db_column='DIRECTORIO', max_length=250)  # Field name made lowercase.
    tipo_unidad = models.IntegerField(db_column='TIPO_UNIDAD')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ENTREGA'


class EntregaUsuarios(models.Model):
    folio_eu = models.AutoField(db_column='FOLIO_EU', primary_key=True)  # Field name made lowercase.
    id_usuario = models.ForeignKey('Usuarios', models.DO_NOTHING, db_column='ID_USUARIO')  # Field name made lowercase.
    id_entrega = models.ForeignKey(Entrega, models.DO_NOTHING, db_column='ID_ENTREGA', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ENTREGA_USUARIOS'


class Periodo(models.Model):
    folio_periodo = models.AutoField(db_column='FOLIO_PERIODO', primary_key=True)  # Field name made lowercase.
    id_entrega = models.ForeignKey(Entrega, models.DO_NOTHING, db_column='ID_ENTREGA')  # Field name made lowercase.
    resp_entrega = models.CharField(db_column='RESP_ENTREGA', max_length=250)  # Field name made lowercase.
    resp_recibe = models.CharField(db_column='RESP_RECIBE', max_length=250)  # Field name made lowercase.
    finicio = models.DateField(db_column='FINICIO')  # Field name made lowercase.
    ffin = models.DateField(db_column='FFIN')  # Field name made lowercase.
    anexos = models.CharField(db_column='ANEXOS', max_length=250)  # Field name made lowercase.
    tipo_periodo = models.IntegerField(db_column='TIPO_PERIODO')  # Field name made lowercase.
    fecha = models.DateField(db_column='FECHA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PERIODO'


class RespCi(models.Model):
    rfc_ci = models.CharField(db_column='RFC_CI', primary_key=True, max_length=250)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=250)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RESP_CI'


class Usuarios(models.Model):
    id_usuario = models.AutoField(db_column='ID_USUARIO', primary_key=True)  # Field name made lowercase.
    usuario = models.CharField(db_column='USUARIO', max_length=50)  # Field name made lowercase.
    pass_field = models.CharField(db_column='PASS', max_length=50)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    nombre = models.CharField(db_column='NOMBRE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    tipo_usuario = models.IntegerField(db_column='TIPO_USUARIO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'USUARIOS'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AuthtokenToken(models.Model):
    key = models.CharField(primary_key=True, max_length=40)
    created = models.DateTimeField()
    user = models.OneToOneField(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'authtoken_token'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
